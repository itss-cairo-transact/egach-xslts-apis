<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.002.001.02" exclude-result-prefixes="xs ns0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template name="firstCharacter">
		<xsl:param name="value" select="/.."/>
		<xsl:param name="default" select="/.."/>
		<xsl:choose>
			<xsl:when test="(string-length($value) = 0)">
				<xsl:value-of select="$default"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($value, 1, 1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="/">
		<T24GenericInputFile>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>20210428053955678</ReceivedDate>
				<BulkIndex>1</BulkIndex>
				<TransactionIndex>1</TransactionIndex>
				<UniqueReference>a1a704b6de0c4739a124d4f9f3e959d7</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>File received</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId">
					<FileDeliveryHeader>
						<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId"/>
					</FileDeliveryHeader>
				</xsl:if>
				<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId">
					<FileHeaderSendingInstitution>
						<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId"/>
					</FileHeaderSendingInstitution>
				</xsl:if>
				<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:InstdAgt">
					<FileHeaderReceivingInstitution>
						<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId"/>
					</FileHeaderReceivingInstitution>
				</xsl:if>
				<FileHeaderFileReference>SBD-BNK21090BHBJHLBG</FileHeaderFileReference>
				<FileMessageFormat>pacs.002.001.02</FileMessageFormat>
				<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm">
					<FileHeaderCreationDateTime>
						<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm), '18', '2')), '000')"/>
					</FileHeaderCreationDateTime>
				</xsl:if>
				<FileTypeIndicator>R</FileTypeIndicator>
			</FileHeader>
			<FileBulks>
				<PaymentStatusReport>
					<TotalNumberOfCreditTransfersBulks>1</TotalNumberOfCreditTransfersBulks>
					<Bulk>
						<BulkHeader>
							<ContraIndicator>NA</ContraIndicator>
							<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId">
								<BulkSendersReference>
									<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId"/>
								</BulkSendersReference>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm">
								<BulkCreationDateTime>
									<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm"/>
								</BulkCreationDateTime>
							</xsl:if>
							<BulkFormat>pacs.002</BulkFormat>
							<BulkSchemeIndicator>C</BulkSchemeIndicator>
							<BulkClearingStatusReportGroupStatus>PART</BulkClearingStatusReportGroupStatus>
							<SkipBulkUpdate>Y</SkipBulkUpdate>
							<BulkAmountCurrency>EGP</BulkAmountCurrency>
							<BulkNumberOfTransactions>1</BulkNumberOfTransactions>
						</BulkHeader>
						<Transaction>
							<BulkClearingStatusReportOriginalBulkReference>SBD-BNK21090BHBJHLBG</BulkClearingStatusReportOriginalBulkReference>
							<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgNmId">
								<BulkClearingStatusReportOriginalBulkFormat>
									<xsl:value-of select="substring(ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgNmId,1,8)"/>
								</BulkClearingStatusReportOriginalBulkFormat>
							</xsl:if>
							<OriginalTransactionIdentification>BNK21090BHBJHLBG</OriginalTransactionIdentification>
							<OriginalUETR>00000000-0000-4000-8000-000000000000</OriginalUETR>
							<TransactionStatus>RJCT</TransactionStatus>
							<StatusReasonInformationReason>
								<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:StsRsn/ns0:Prtry !=''">
									<Proprietary>
										<xsl:value-of select="substring(ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:StsRsn/ns0:Prtry,3,4)"/>
									</Proprietary>
								</xsl:if>
							</StatusReasonInformationReason>
							<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:AddtlStsRsnInf">
								<StatusReasonInformationAdditionalInformation>
									<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:AddtlStsRsnInf"/>
								</StatusReasonInformationAdditionalInformation>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm">
								<AcceptanceDateTime>
									<xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:CreDtTm"/>
								</AcceptanceDateTime>
							</xsl:if>
							<ClearingSystemReference>RTGS unique payment reference</ClearingSystemReference>
						</Transaction>
					</Bulk>
				</PaymentStatusReport>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>