<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.006.001.01" exclude-result-prefixes="xs ns0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template name="firstCharacter">
		<xsl:param name="value" select="/.."/>
		<xsl:param name="default" select="/.."/>
		<xsl:choose>
			<xsl:when test="(string-length($value) = 0)">
				<xsl:value-of select="$default"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($value, 1, 1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="/">
		<T24GenericInputFile>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>.</ReceivedDate>
				<BulkIndex>.</BulkIndex>
				<TransactionIndex>.</TransactionIndex>
				<UniqueReference>.</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>.</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<FileMessageFormat>pacs.006.001.01</FileMessageFormat>
				<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:MsgId">
					<FileHeaderFileReference>
						<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:MsgId"/>
					</FileHeaderFileReference>
				</xsl:if>
			</FileHeader>
			<FileBulks>
				<PaymentCancellationRequest>
					<TotalCancellationRequestsBulks>
						<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
					</TotalCancellationRequestsBulks>
					<Bulk>
						<BulkHeader>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:MsgId">
								<BulkSendersReference>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:MsgId"/>
								</BulkSendersReference>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<BulkCreationDateTime>
									<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(./ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '18', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '21', '3'))"/>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:CreDtTm"/>
								</BulkCreationDateTime>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:NbOfTxs">
								<BulkNumberOfTransactions>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
								</BulkNumberOfTransactions>
							</xsl:if>
							<BulkAmountCurrency>EGP</BulkAmountCurrency>
							<BulkSchemeIndicator>C</BulkSchemeIndicator>
							<BulkFormat>pacs.006</BulkFormat>
						</BulkHeader>
						<Transaction>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:CxlId">
								<CancellationIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:CxlId"/>
								</CancellationIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgId">
								<OriginalMessageIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgId"/>
								</OriginalMessageIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId ">
								<OriginalMessageNameIdentification>
									<xsl:value-of select="substring(string(ns0:Document/ns0:pacs.006.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId), '1', '8')"/>
								</OriginalMessageNameIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:OrgnlTxId">
								<OriginalTransactionIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:OrgnlTxId"/>
								</OriginalTransactionIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:CxlRsnInf/ns0:CxlRsn/ns0:Prtry">
								<CancellationReasonInformationReason>
									<Proprietary>
										<xsl:value-of select="substring(ns0:Document/ns0:pacs.006.001.01/ns0:TxInf/ns0:CxlRsnInf/ns0:CxlRsn/ns0:Prtry,3,4)"/>
									</Proprietary>
								</CancellationReasonInformationReason>
							</xsl:if>
						</Transaction>
					</Bulk>
				</PaymentCancellationRequest>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>
