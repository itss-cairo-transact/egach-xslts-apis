* @ValidationCode : MjotMTg2ODIzMjgxMDpDcDEyNTI6MTYzODM2OTEyODUxMDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 01 Dec 2021 16:32:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.determineOutMsgFormat(iTransDetails, oMessageFormat, oMessageFormatResponse)
*------------------------------------------------------------------------------
    $USING PP.STEP2Service
    GOSUB initialise
    GOSUB process

RETURN
*------------------------------------------------------------------------------
process:
*
    BEGIN CASE
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'CT'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.008'
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'RT'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.004'
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'CR'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.006'
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'DD'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.003'
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'RV'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.007'
        CASE iTransDetails<PP.STEP2Service.TransDetails.clearingTransactionType> EQ 'RJ'
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.002'


        CASE 1
            oMessageFormat<PP.STEP2Service.MsgFormat.messageFormat> = 'pacs.008'
    END CASE
*
RETURN
*------------------------------------------------------------------------------
initialise:
*
    oMessageFormat = ''
    oMessageFormatResponse = ''
*
RETURN
*------------------------------------------------------------------------------
END
