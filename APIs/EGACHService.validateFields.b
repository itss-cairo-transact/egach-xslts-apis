* @ValidationCode : MjotMTA2NDg4OTY5ODpDcDEyNTI6MTYzNTUyMDg0NDU0Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Oct 2021 17:20:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*-----------------------------------------------------------------------------
* <Rating>-140</Rating>
*-----------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.validateFields(iTransDets, iPrtyDbtDets, iCreditPartyDets, iOriginatingSource, iDebitAuthDets, iInformationDets, oValidChannelFlag, oValidateResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iTransDets - TransDets (Single), IN
* iPrtyDbtDets - PartyDebitDets (List), IN
* iCreditPartyDets - CreditPartyDets (List), IN
* iOriginatingSource - String (Single), IN
* iDebitAuthDets - DebitAuthDets (List), IN
* iInformationDets - InformationDets (List), IN
* oValidChannelFlag - ValidChannelFlag (Single), OUT*
* oValidateResponse - PaymentResponse (Single), OUT
*-----------------------------------------------------------------------------
    $INSERT I_CountryIBANStructureService_PotentialIBAN
    $USING PP.BankCodeService
    $USING PP.PaymentFrameworkService
    $USING PP.PaymentWorkflowDASService
    $USING PP.CountryIBANStructureService
    $USING PP.DebitPartyDeterminationService
    $USING PP.STEP2Service
    $USING PP.EGACHService
*------------------------------------------------------------------------------
*
    CALL TPSLogging("Input parameter","STEP2Service.validateFields","iTransDets : <":iTransDets:"> iPrtyDbtDets : <":iPrtyDbtDets:"> iCreditPartyDets : <":iCreditPartyDets:"> iOriginatingSource : <":iOriginatingSource:"> iDebitAuthDets : <":iDebitAuthDets:"> iInformationDets : <":iInformationDets:">","")
*
    GOSUB initialise
    GOSUB process
*
    CALL TPSLogging("Output Parameters","STEP2Service.validateFields","oValidChannelFlag : <":oValidChannelFlag:">, oValidateResponse : <":oValidateResponse:">","")
*
RETURN
*------------------------------------------------------------------------------
initialise:
    oValidChannelFlag = ''
    oValidateResponse = ''
    iNCCContext=''
    clgTxnType = iTransDets<PP.STEP2Service.TransDets.clearingTransactionType>
    oValidChannelFlag<PP.STEP2Service.ValidChannelFlag.validChannelFlag> = "N"
    
RETURN
*------------------------------------------------------------------------------
process:
*
    GOSUB validateEndToEndReference
    GOSUB validateFTNumber
    GOSUB validateTransactionAmount
    GOSUB validateTransactionCurrencyCode
    IF iOriginatingSource EQ "OE" THEN
        GOSUB validateDebitPartyAccountLineForOE
    END ELSE
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> NE "" THEN
            GOSUB validateDebitPartyFreeLine1
            GOSUB validateDebitPartyAccountLine
            GOSUB validateDebitPartyIdentifierCode
        END
    END
    GOSUB validateCreditPartyIdentifierCode
    GOSUB validateCreditPartyFreeLine1
    GOSUB validateCreditPartyAccountLine
*   Address check to be performed for payments direction O/R.
    IF iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'O' OR iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'R' THEN
        GOSUB validateAddress
    END
    oValidChannelFlag<PP.STEP2Service.ValidChannelFlag.validChannelFlag> = "Y"
*
RETURN
*------------------------------------------------------------------------------
validateEndToEndReference:
RETURN
*------------------------------------------------------------------------------
validateFTNumber:
    IF iTransDets<PP.STEP2Service.TransDets.ftNumber> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "FTNumber NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateTransactionAmount:
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount NOT FOUND"
        GOSUB finalise
    END

* Transaction Amount must be 0.01 or more and 999999999.99 or les and the fractional part has a maximum of two digits.
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> <= 0 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount OUT OF RANGE Value"
        GOSUB finalise
    END
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> >= 1000000000 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount OUT OF RANGE Value"
        GOSUB finalise
    END

    Decimal = FIELD(iTransDets<PP.STEP2Service.TransDets.transactionAmount>, ".", 2)
    IF LEN(Decimal) > 2 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount TOO MANY DECIMALS Value"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateTransactionCurrencyCode:
    IF iTransDets<PP.STEP2Service.TransDets.transactionCurrencyCode> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionCurrencyCode NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateDebitPartyFreeLine1:
    FMp = ''; VMp = '';Pos=''
    
    FIND "ORDPTY" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyFreeLine1,VMp> EQ "" AND iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyName,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "debitPartyFreeLine1 NOT FOUND"
            GOSUB finalise
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ORDPTY in debitPartyRole NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateDebitPartyAccountLineForOE:
*    IF iPrtyDbtDets<PartyDebitDets.debitPartyAccountLine> EQ "" THEN
*        oValidateResponse<PaymentResponse.responseMessages,1,ResponseMessage.messageInfo> = "DebitPartyAccountLine is missing"
*        GOSUB finalise
*    END
    IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine> NE "" THEN
        iPotentialIBAN = ""
        iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
        iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine>,"/","")
*
        oIBANDetail = ""
        oDetIBANResponse = ""
        CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
        IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyAccountLine is invalid IBAN"
            GOSUB finalise
        END
    END
RETURN
*------------------------------------------------------------------------------
validateDebitPartyAccountLine:
      
    FIND "ORDPTY" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyAccountLine NOT FOUND"
            GOSUB finalise
        END
*
        IF clgTxnType NE 'RT' THEN
            iPotentialIBAN = ""
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine,VMp>,"/","")
*
            oIBANDetail = ""
            oDetIBANResponse = ""
            CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
            IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
                oDetIBANResponse = ""
                iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iTransDets<PP.STEP2Service.TransDets.relatedIBAN>,"/","")
                CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
                IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyAccountLine NOT IBAN"
                    GOSUB finalise
                END
            END
        END
    END
*
RETURN
*------------------------------------------------------------------------------
validateDebitPartyIdentifierCode:
    IF iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'R' THEN
        FIND "ORDINS" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
            IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,VMp> EQ "" THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyIdentifierCode NOT FOUND"
                GOSUB finalise
            END
        END ELSE
            FIND "SENDER" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
                IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,VMp> EQ "" THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyIdentifierCode NOT FOUND"
                    GOSUB finalise
                END
            END ELSE
                IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,VMp> EQ "" THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyIdentifierCode NOT FOUND"
                    GOSUB finalise
                END
            END
        END
    END
RETURN
*------------------------------------------------------------------------------
validateCreditPartyIdentifierCode:
    FIND "ACWINS" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp> EQ "" THEN
*to determine BIC from NCC for step2 payments as STEP2 clearing accepts only BIC
            iNCCContext<PP.BankCodeService.NCCContext.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
            iNCCContext<PP.BankCodeService.NCCContext.nationalID> = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp>
            CALL PP.BankCodeService.determineBICFromNCC(iNCCContext,oBICDetails,oBICFromNCCError)
            IF oBICFromNCCError NE '' THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyIdentifierCode NOT FOUND"
                GOSUB finalise
            END
        END
    END ELSE
        FIND "RECVER" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
            IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp> EQ "" THEN
*to determine BIC from NCC for step2 payments as STEP2 clearing accepts only BIC
                iNCCContext<PP.BankCodeService.NCCContext.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
                iNCCContext<PP.BankCodeService.NCCContext.nationalID> = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp>
                CALL PP.BankCodeService.determineBICFromNCC(iNCCContext,oBICDetails,oBICFromNCCError)
                IF oBICFromNCCError NE '' THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyIdentifierCode NOT FOUND"
                    GOSUB finalise
                END
            END
        END ELSE
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo>= "ACWINS/RECVER in crPartyRole NOT FOUND"
            GOSUB finalise
        END
    END
RETURN
*------------------------------------------------------------------------------
validateCreditPartyFreeLine1:
* Checking if the crPartyName is null along with partyfreeline for BENFCY
    FIND "BENFCY" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyFreeLine1,VMp> EQ "" AND iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyName,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyFreeLine1 NOT FOUND"
            GOSUB finalise
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "BENFCY in crPartyRole NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateCreditPartyAccountLine:
    FIND "BENFCY" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyAccountLine NOT FOUND"
            GOSUB finalise
        END
* No validation happens for an RT payment specially IBAN validation.
        IF clgTxnType NE 'RT' THEN
            iPotentialIBAN = ""
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp>,"/","")
            oIBANDetail = ""
            oDetIBANResponse = ""
*
            CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
*
            IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyAccountLine NOT IBAN"
                GOSUB finalise
            END
        END
    END
RETURN
*------------------------------------------------------------------------------
finalise:
*
    oValidateResponse<PP.STEP2Service.PaymentResponse.returnCode> = 'FAILURE'
    oValidateResponse<PP.STEP2Service.PaymentResponse.serviceName> = 'STEP2Service'
    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageCode> = 'STB10003'
    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageType> = 'NON_FATAL_ERROR'
*
    CALL TPSLogging("Output Parameter - END","STEP2Service.validateFieldsDirectDebit","oValidChannelFlag : <":oValidChannelFlag:">, oValidateResponse : <":oValidateResponse:">","")
    GOSUB exit
*
RETURN
*------------------------------------------------------------------------------
exit:
RETURN TO exit
*------------------------------------------------------------------------------
validateAddress:
*   Here we peform address validation which is similar to FATF validation.
    FMp = ''
    VMp = ''
    iPaymentDirection = ''
    iPaymentDirection = iTransDets<PP.STEP2Service.TransDets.pmtDirection>
    debtorAddress = ''
    customerAddress = ''
    GOSUB getCompanyProperties
    GOSUB getAccInfoDetails

*   Below condition is added for CT transactions
    IF (iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'CT') THEN
        IF iPaymentDirection EQ 'O' THEN
            debtorAddress = customerAddress
        END ELSE
*           Below mapping is for redirect payments.
            FIND "ORDPTY" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
                debtorAddress = iPrtyDbtDets<PP.DebitPartyDeterminationService.PORPartyDebit.debitPartyAddressLine1,VMp>
            END
        END

        IF debtorAddress EQ '' THEN
            GOSUB FindFATFOriginatingBankCountry
            IF (origninatingCG EQ '') OR (origninatingCG NE 'EU' AND origninatingCG NE 'EEA') THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "Originating Bank not in EU/EEA region"
                GOSUB finalise
            END
            GOSUB FindFATFBeneficiaryBankCountry
            IF (BeneficiaryCG EQ '') OR (BeneficiaryCG NE 'EU' AND BeneficiaryCG NE 'EEA') THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "Beneficiary Bank not in EU/EEA region"
                GOSUB finalise
            END
        END
    END
        
RETURN
*--------------------------------------------------------------------------------
getAccInfoDetails:
*   To get payment record details
    iTransAccDetails = ''
    oAccInfoDetails = ''
    oGetAccError = ''
    iTransAccDetails<PP.DebitPartyDeterminationService.InputTransactionAccDetails.mainOrChargeAccType> = 'D'
    iTransAccDetails<PP.DebitPartyDeterminationService.InputTransactionAccDetails.ftNumber> = iTransDets<PP.STEP2Service.TransDets.ftNumber>
    
    PP.DebitPartyDeterminationService.getAccInfoDetails(iTransAccDetails, oAccInfoDetails, oGetAccError)
    customerAddress = oAccInfoDetails<PP.DebitPartyDeterminationService.AccInfoDetails.customerAddress>
    
RETURN
*--------------------------------------------------------------------------------
getCompanyProperties:
*   Determine Company Properties
    iCompanyBIC = ''
    iCompanyPropKey = ''
    oCompanyProperties = ''
    oGetCompPropsError = ''
    iCompanyPropKey = iTransDets<PP.STEP2Service.TransDets.companyID>
    PP.PaymentFrameworkService.getCompanyProperties(iCompanyPropKey, oCompanyProperties, oGetCompPropsError)
    iCompanyBIC =  oCompanyProperties<PP.PaymentFrameworkService.CompanyProperties.companyBIC>
    
RETURN
*------------------------------------------------------------------------------
FindFATFOriginatingBankCountry:
*   when the FATF flag is E then check for the origination Bank country to be in EU EEA region.
*   This block is to determine origination Country Group for Redirect and outgoing payments.
    BankBIC = ''
    origninatingCG = ''
    ordInsPos = ''
    ordPtyPos = ''
    senderPos = ''
    ibanAcctNo=''
    FMp = ''
    ordInsPos = ''
*   For outgoing payment the companny Bic is itself the origination country
    IF iPaymentDirection EQ 'O' THEN
        BankBIC = iCompanyBIC
        GOSUB determineCountryGroup
        origninatingCG = ReturnedCG
    END ELSE
        FIND "ORDINS" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,ordInsPos THEN
            BankBIC = iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,ordInsPos>
            IF BankBIC NE '' THEN
                GOSUB determineCountryGroup
                origninatingCG = ReturnedCG
            END ELSE
                ordInsPos = ''
            END
        END ELSE
            FIND 'SENDER' IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,ordInsPos THEN
                BankBIC = iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,ordInsPos>
                IF BankBIC NE '' THEN
                    GOSUB determineCountryGroup
                    origninatingCG = ReturnedCG
                END ELSE
                    ordInsPos = ''
                END
            END ELSE
                ordInsPos = ''
            END
        END
        IF ordInsPos EQ '' THEN
            FIND 'ORDPTY' IN  iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,ordInsPos THEN
                ibanAcctNo = iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine,ordInsPos>
                GOSUB findBICNCCfromIBAN
                origninatingCG = ReturnedCG
            END ELSE
                origninatingCG = ''
            END
        END
    END
    
RETURN
*------------------------------------------------------------------------------
determineCountryGroup:
*   This block is to determine the Country Group
*   Initialise ReturnedCG is 'N'
    ReturnedCG = 'N'
    iCountryCode = ''
    oCountryGroupList = ''
    oGetCountryGrpError = ''
*   If BankBIC
    IF BankBIC NE '' THEN
        GOSUB determineCtyFromBIC
    END
*   To find the Country Group
    IF iCountryCode NE '' THEN
        oCountryGroupList = ''
        oGetCountryGrpError = ''
        PP.PaymentFrameworkService.getCountryGroupList(iCountryCode, oCountryGroupList, oGetCountryGrpError)
        IF oGetCountryGrpError EQ '' THEN
            CONVERT @VM TO @FM IN oCountryGroupList
            LOCATE 'EEA' IN oCountryGroupList SETTING EeaPos THEN
                ReturnedCG = oCountryGroupList<EeaPos>
                BankBIC=''
                RETURN
            END
            LOCATE "EU" IN oCountryGroupList SETTING EuPos THEN ;*Check if the returned list of CountryGroup contains EEA/EU in it
                ReturnedCG = oCountryGroupList<EuPos>
                BankBIC=''
                RETURN
            END ELSE
                LOC.POS = ''
            END
        END
    END
    BankBIC=''
    
RETURN
*------------------------------------------------------------------------------
determineCtyFromBIC:
*   This block is to determine the countryCode based on the provided BIC
    iCountryCode = ''
    iCountryCode = BankBIC[5,2]
    
RETURN
*------------------------------------------------------------------------------
findBICNCCfromIBAN:
*   Determine IBAN details from PPT.COUNTRYIBANTABLE
    oIBANDetail = ''
    oDetIBANResponse = ''
    iPotentialIBAN =''
    ReturnedCG=''
    actibanAcctNo=''
    lenOfAcctNo=''
    acctLinePosValue=''

    lenOfAcctNo = LEN(ibanAcctNo)
    acctLinePosValue = ibanAcctNo[1,1]
    IF  acctLinePosValue EQ '/' THEN
        actibanAcctNo =  ibanAcctNo[2,lenOfAcctNo-1]
    END ELSE
        actibanAcctNo =  ibanAcctNo
    END

    iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
    iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = actibanAcctNo
    PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)

*   If the account number is Not an IBAN, return to the calling Method
    IF oDetIBANResponse NE '' THEN
        ReturnedCG = 'E'
        RETURN
    END

    iIBANContext=''
    oBICNCCDetails=''
    oBICNCCError=''
    iIBANContext<PP.BankCodeService.IBANContext.companyID> =  iTransDets<PP.STEP2Service.TransDets.companyID>
    iIBANContext<PP.BankCodeService.IBANContext.ibanCountryCode> = oIBANDetail<PP.CountryIBANStructureService.IBANDetail.ibanCountryCode>
    iIBANContext<PP.BankCodeService.IBANContext.ibanNationalID> =  oIBANDetail<PP.CountryIBANStructureService.IBANDetail.ibanNationalID>

    PP.BankCodeService.determineBICNCCFromIBAN(iIBANContext,oBICNCCDetails,oBICNCCError)

    IF oBICNCCError NE '' THEN
        ReturnedCG = 'E'
    END ELSE
        BankBIC = oBICNCCDetails<PP.BankCodeService.BICNCCDetail.bicCode>
        GOSUB determineCountryGroup
    END
    
RETURN
*------------------------------------------------------------------------------
FindFATFBeneficiaryBankCountry:
    
    FIND "ACWINS" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,VMp> EQ 'G' THEN
            iACWINSBicG = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp>
            GOSUB AssignBICAndNCC
            BeneficiaryCG = ReturnedCG
        END ELSE
            IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,VMp> EQ 'R' THEN
                iACWINSBicG = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp>
            END
            ReturnedCG ='E'
        END
    END
    
    IF ReturnedCG EQ 'E' THEN
        IF iACWINSBicG NE '' THEN
            GOSUB AssignBICAndNCC
            RETURN
        END
        FIND "RECVER" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
            IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,VMp> EQ 'G' THEN
                iRecvrG = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp>
                IF iRecvrG NE '' THEN
                    GOSUB AssignBICAndNCC
                    BeneficiaryCG = ReturnedCG
                END
            END
        END ELSE
            IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,VMp> EQ 'D' THEN
                iRecvrD = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp>
                IF iRecvrD NE '' THEN
                    GOSUB AssignBICAndNCC
                    BeneficiaryCG = ReturnedCG
                END
            END
        END
    END
    
RETURN
*------------------------------------------------------------------------------
AssignBICAndNCC:
*   This block is to assign BankBIC for creditParty
    BankBIC = iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp>
    GOSUB determineCountryGroup
    BeneficiaryCG = ReturnedCG
    
RETURN
*------------------------------------------------------------------------------
END
