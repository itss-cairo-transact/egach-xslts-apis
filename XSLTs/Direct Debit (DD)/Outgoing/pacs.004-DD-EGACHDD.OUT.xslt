<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vmf="http://www.altova.com/MapForce/UDF/vmf"
                xmlns:agt="http://www.altova.com/Mapforce/agt" 
                xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.004.001.01"
                xmlns:ns1="http://www.temenos.com/T24/LocalClearingService/PorAdditionalInf"
                xmlns:ns2="http://www.temenos.com/T24/LocalClearingService/PorInformation"
                xmlns:ns3="http://www.temenos.com/T24/LocalClearingService/PorPartyCredit"
                xmlns:ns4="http://www.temenos.com/T24/LocalClearingService/PorPartyDebit"
                xmlns:ns5="http://www.temenos.com/T24/LocalClearingService/PorRemittanceInfoPart1"
                xmlns:ns6="http://www.temenos.com/T24/LocalClearingService/PorTransaction"
                xmlns:ns7="http://www.temenos.com/T24/LocalClearingService/PpCanReq"
                xmlns:ns8="http://www.temenos.com/T24/event/OutwardMappingIF/doOutwardMappingFlow"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="vmf agt ns0 ns1 ns2 ns3 ns4 ns5 ns6 ns7 ns8 xs">
    <xsl:template name="vmf:vmf1_inputtoresult">
        <xsl:param name="input" select="/.." />
        <xsl:choose>
            <xsl:when test="$input='EGACH'">
                <xsl:value-of select="'VST2'" />
            </xsl:when>
            <xsl:when test="$input='RPSSCL'">
                <xsl:value-of select="'VSCL'" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="/">
        <xsl:variable name="var1_initial" select="." />
        <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pacs.004.001.01">
            <!--xsl:attribute name="xsi:schemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">urn:iso:std:iso:20022:tech:xsd:pacs.004.001.01 pacs.004.001.01.xsd</xsl:attribute-->
            <pacs.004.001.01>
                <GrpHdr>
                    <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:bulkReference">
                        <xsl:variable name="var13_cur" select="." />
                        <MsgId>
                            <xsl:value-of select="." />
                        </MsgId>
						</xsl:for-each>
                    <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:createdDateTime">
                        <xsl:variable name="var14_cur" select="." />
                        <CreDtTm>
                            <xsl:value-of
                                select="concat(substring(., 1, 4), '-', substring(., 5, 2), '-', substring(., 7, 2), 'T', substring(., 9, 2), ':', substring(., 11, 2), ':', substring(., 13, 2), '.0Z')" />
                        </CreDtTm>
                    </xsl:for-each>
					<xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:transactionBulkCount">
                        <xsl:variable name="var15_cur" select="." />
                        <NbOfTxs>
                            <xsl:value-of select="." />
                        </NbOfTxs>
                    </xsl:for-each>
                    <GrpRtr>false</GrpRtr>
                    <SttlmInf>
                        <SttlmMtd>CLRG</SttlmMtd>
                    </SttlmInf>
                    <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:companyBIC">
                        <xsl:variable name="var23_cur" select="." />
                        <InstgAgt>
                            <FinInstnId>
                                <BIC>
                                    <xsl:value-of select="." />
                                </BIC>
                            </FinInstnId>
							 </InstgAgt>
                    </xsl:for-each>
					   </GrpHdr>
					   <!--OrgnlGrpInf-->
					    <OrgnlMsgId>
                        <xsl:choose>
                            <xsl:when test="((ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:paymentDirection = 'R') and (not(ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:originalOrReturnId)))">
                                <xsl:value-of select = "'UNMATCHED'" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:bulkSendersReference">
                                    <xsl:variable name="var25_cur" select="." />
                                    <xsl:value-of select="." />
                                </xsl:for-each>
                            </xsl:otherwise>
                        </xsl:choose>
                    </OrgnlMsgId>
					<TxInf>
                    <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:fTNumber">
                        <xsl:variable name="var24_cur" select="." />
                        <RtrId>
                            <xsl:value-of select="." />
                        </RtrId>
                    </xsl:for-each>
					<xsl:variable name="var26_cur" select="." />
                        <OrgnlInstrId>
                            <xsl:value-of select="." />
                        </OrgnlInstrId>
                    <!--/xsl:for-each-->
					 <xsl:choose>
                        <xsl:when test="boolean(translate(normalize-space($var27_nested), ' 0', ''))">
                            <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:customerSpecifiedReference">
                                <xsl:variable name="var29_cur" select="." />
                                <OrgnlEndToEndId>
                                    <xsl:value-of select="." />
                                </OrgnlEndToEndId>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <OrgnlEndToEndId>
                                <xsl:value-of select="'NOTPROVIDED'" />
                            </OrgnlEndToEndId>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:sendersReferenceIncoming">
                        <xsl:variable name="var30_cur" select="." />
                        <OrgnlTxId>
                            <xsl:value-of select="." />
                        </OrgnlTxId>
                    </xsl:for-each>
					<xsl:for-each select="ns8:doOutwardMappingFlow/ns8:iportransaction">
                        <xsl:variable name="var34_cur" select="." />
                        <xsl:for-each select="(./ns6:outTransactionAmount)[(string(.) != string(0))]">
                            <xsl:variable name="var35_filter" select="." />
                            <RtrdIntrBkSttlmAmt>
                                <xsl:for-each select="$var34_cur/ns6:outTransactionCurrencyCode">
                                    <xsl:variable name="var36_cur" select="." />
                                    <xsl:attribute name="Ccy" namespace="">
                                        <xsl:value-of select="." />
                                    </xsl:attribute>
                                </xsl:for-each>
                                <xsl:value-of select="number(.)" />
                            </RtrdIntrBkSttlmAmt>
                        </xsl:for-each>
                    </xsl:for-each>	
					<StsRsnInf>
						<RtrRsn>
						<xsl:if test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode">
						<xsl:choose>
                                <xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'AC01'">
								<Prtry>000100</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'AC04'">
								<Prtry>000101</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'AC06'">
								<Prtry>000102</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'AG01'">
								<Prtry>000103</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'AM03'">
								<Prtry>000104</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'BE04'">
								<Prtry>000105</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'BE06'">
								<Prtry>000106</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'BE07'">
								<Prtry>000107</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'MD07'">
								<Prtry>000108</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'ED05'">
								<Prtry>000109</Prtry>
								</xsl:when>
								<xsl:when test="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:clgReturnCode = 'RR04'">
								<Prtry>000110</Prtry>
								</xsl:when>
                                <xsl:otherwise><Prtry>Error</Prtry></xsl:otherwise>
					</xsl:choose>
					</xsl:if>		
								</RtrRsn>
                            <AddtlRtrRsnInf>
                                <xsl:value-of select="ns8:doOutwardMappingFlow/ns8:iportransaction/ns6:reasonDescription" />
                            </AddtlRtrRsnInf>
                    </StsRsnInf>
					       </TxInf>
            </pacs.004.001.01>
        </Document>
    </xsl:template>
</xsl:stylesheet>