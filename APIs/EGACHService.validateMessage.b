* @ValidationCode : MjoxODgyMjc3NjUzOkNwMTI1MjoxNjM1NTI1Mjk2MzMyOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Oct 2021 18:34:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.validateMessage(iChannelDetails, iRSCreditDets, oChannelResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iChannelDetails - ChannelDetails (Single), IN
* iRSCreditDets - RSCreditDets (List), IN
* oChannelResponse - PaymentResponse (Single), OUT
*-----------------------------------------------------------------------------

    $USING PP.STEP2Service
    $USING PP.PaymentWorkflowDASService
    $USING PP.DebitAuthorityService
    $USING PP.DebitPartyDeterminationService
    $USING PP.CreditPartyDeterminationService
    $USING PP.PaymentFrameworkService
    $USING PP.PaymentWorkflowGUI
    $USING PP.InboundCodeWordService
    $USING PP.DuplicateCheckService
    $USING PP.InwardCreditTransferInitiationService
    $USING PP.EGACHService
    
*------------------------------------------------------------------------------
*
    CALL TPSLogging("Input Parameters","EGACHService.validateMessage 3330627 10 Sep 19","iChannelDetails : <":iChannelDetails:">, iRSCreditDets : <":iRSCreditDets:">","")
*
    GOSUB initialise
    GOSUB process
*
    CALL TPSLogging("Output Parameters - END","EGACHService.validateMessage","oChannelResponse:<":oChannelResponse:">","")
*
RETURN
*------------------------------------------------------------------------------
initialise:
*
    oChannelResponse = '' ; scenarioCode = '' ; iCreditPartyDet = '' ; settlementDate = '' ; reqCollectionDate = ''
    relatedIBAN = '' ; mainOrChargeAccType = ''
*
RETURN
*------------------------------------------------------------------------------
process:
* retrieve the payment details
    GOSUB getPorTransInfo
* retrieve the debit party details
    GOSUB debitPartyInfo
* retrieve the credit party details
    GOSUB creditPartyInfo
* retrieve the mandate details and the information details of a Direct Debit payment
    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> MATCHES 'RF':@VM:'RD':@VM:'RV' THEN
        GOSUB getPorDebitAuthInfo
        GOSUB getPorInformation
    END
    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> EQ 'DD' THEN
        GOSUB getPorDebitAuthInfo
    END
* validate the remittance info, length should not be more than 140
    GOSUB additionalInfo
* validate the retrieved information
    GOSUB validateFields
    GOSUB validateRemittanceInformation ; *Paragraph to check whether the Remittance Information is with in the length as part Sepa 2019 RB
*
RETURN
*------------------------------------------------------------------------------
getPorTransInfo:
* retrieve the payment details
    iPaymentID                          = ""
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber>      = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID>     = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    oPaymentRecord                      = ""
    oAdditionalPaymentRecord            = ""
    oReadErr                            = ""
    PP.PaymentWorkflowDASService.getPaymentRecord(iPaymentID,oPaymentRecord,oAdditionalPaymentRecord,oReadErr)
    IF oReadErr<PP.CreditPartyDeterminationService.DASError.error> NE "" THEN
        scenarioCode = 1
        GOSUB updateResponseAndExit
    END

* 33B
    GOSUB getDebitInstructedAmt
    instructedCcyValue = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.instructedCurrencyCode>
*If DebitInstructedAmount is present and InstructedCurrency is present and if it is not Euro Currency, then
    IF debitInstructAmtPresent AND instructedCcyValue AND instructedCcyValue NE "EUR" THEN
        IF iChannelDetails<PP.STEP2Service.ChannelDetails.rsOutputChannelImposed> EQ 'Y' THEN  ;* If the Output channel 'RPSSCL' is imposed on OE Screen,
            LogEventType = 'ERR'
            LogEventDescription = ''
            LogErrorCode = 'STB10007'
            LogAdditionalInfo = 'STEP2'
            GOSUB updateHistoryLog
        END
        scenarioCode = 8
        GOSUB updateResponseAndExit
    END
* 33B
*
RETURN
*------------------------------------------------------------------------------
debitPartyInfo:
*   Calling Debit Party Determination Service Component.
    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.originatingSource> NE "OE" OR (oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.originatingSource> EQ "OE" AND (oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> EQ "DD" OR oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> EQ "RV")) THEN
        iDebitPartyRole                             = ""
        iDebitPartyRole<PP.DebitPartyDeterminationService.DebitPartyRole.companyID>   = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
        iDebitPartyRole<PP.DebitPartyDeterminationService.DebitPartyRole.ftNumber>    = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
        oPrtyDbtDetails                             = ""
        oGetPrtyDbtError                            = ""
        PP.DebitPartyDeterminationService.getPartyDebitDetails(iDebitPartyRole,oPrtyDbtDetails,oGetPrtyDbtError)
        IF oGetPrtyDbtError<PP.CreditPartyDeterminationService.DASError.error> NE "" THEN
            scenarioCode = 2
            GOSUB updateResponseAndExit
        END
        IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> EQ 'DD' THEN
            mainOrChargeAccType = "C"
        END ELSE
            mainOrChargeAccType = "D"
        END
        GOSUB getAccInfoDetails
        relatedIBAN = oAccInfoDetails<PP.DebitPartyDeterminationService.AccInfoDetails.relatedIBAN>
    END ELSE
;*debit side mapping for OE
        mainOrChargeAccType = "D"
        GOSUB getAccInfoDetails
        oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyAccountLine> = oAccInfoDetails<PP.DebitPartyDeterminationService.AccInfoDetails.relatedIBAN>
        oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyName> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.debitPartyLine1>
        oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyAddressLine1> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.debitPartyLine2>
        oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyAddressLine2> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.debitPartyLine3>
        oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyCountry> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.orderingPartyResidency>
    END
*
RETURN
*------------------------------------------------------------------------------
creditPartyInfo:
*   Calling Debit Party Determination Service Component.
    iCreditPartyRole = ""
    iCreditPartyRole<PP.CreditPartyDeterminationService.CreditPartyKey.companyID>  = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iCreditPartyRole<PP.CreditPartyDeterminationService.CreditPartyKey.ftNumber>   = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    oCreditPartyDet = ""
    oGetCreditError = ""
    PP.CreditPartyDeterminationService.getPartyCreditDetails(iCreditPartyRole,oCreditPartyDet,oGetCreditError)
    IF oGetCreditError<PP.CreditPartyDeterminationService.DASError.error> NE "" THEN
        scenarioCode = 3
        GOSUB updateResponseAndExit
    END ELSE
* Fetching the output values
        crtPtyPos = 1
        totalcreditPartyRoles = DCOUNT(oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyRole>, @VM)
        LOOP
        WHILE (crtPtyPos LE totalcreditPartyRoles)
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRole,crtPtyPos>           = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyRole,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,crtPtyPos>      = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyRoleIndic,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,crtPtyPos>    = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyIdentifCode,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,crtPtyPos>    = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyAccountLine,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine1,crtPtyPos>      = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyFreeLine1,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine2,crtPtyPos>      = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyFreeLine2,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine3,crtPtyPos>      = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyFreeLine3,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine4,crtPtyPos>      = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyFreeLine4,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyInformationTag,crtPtyPos> = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crPartyInformationTag,crtPtyPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyName,crtPtyPos>           = oCreditPartyDet<PP.CreditPartyDeterminationService.CreditPartyDetails.crName,crtPtyPos>
            crtPtyPos = crtPtyPos + 1
        REPEAT
    END
    IF NOT (oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> MATCHES 'RF':@VM:'RD':@VM:'DD':@VM:'RV') THEN
        IF iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRole> NE '' THEN
            GOSUB checkPartyCreditRole
        END
    END
*
RETURN
*------------------------------------------------------------------------------
checkPartyCreditRole:
*--------------------
* This para checks crPartyRole is present in the RSCreditDets and map the output valiables accordingly.
* Assign the output values iRSCreditDets to iCreditPartyDet
    totalcreditPartyRoles = DCOUNT (iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRole>, @VM)
    rsCrParDetPos = 1
    LOOP
    WHILE (rsCrParDetPos LE totalcreditPartyRoles)
        crPartyRole = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRole, rsCrParDetPos>
        LOCATE crPartyRole IN iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING crtPtyDetPos THEN
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRole,crtPtyDetPos>        = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRole,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,crtPtyDetPos>   = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRoleIndic,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,crtPtyDetPos> = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyIdentifCode,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,crtPtyDetPos> = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyAccountLine,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine1,crtPtyDetPos>   = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine1,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine2,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine2,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine3,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine3,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine4,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine4,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyInformationTag,crtPtyPos> = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyInformationTag,rsCrParDetPos>
        END ELSE
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRole,crtPtyPos>           = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRole,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyRoleIndic,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyRoleIndic,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,crtPtyPos>    = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyIdentifCode,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,crtPtyPos>    = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyAccountLine,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine1,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine1,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine2,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine2,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine3,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine3,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyFreeLine4,crtPtyPos>      = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyFreeLine4,rsCrParDetPos>
            iCreditPartyDet<PP.STEP2Service.CreditPartyDets.crPartyInformationTag,crtPtyPos> = iRSCreditDets<PP.STEP2Service.RSCreditDets.crPartyInformationTag,rsCrParDetPos>
            crtPtyPos = crtPtyPos + 1
        END
        rsCrParDetPos++
    REPEAT
*
RETURN
*-----------------------------------------------------------------------------
getPorDebitAuthInfo:
* retrieve the mandate details
    iInputDebitAuthority = ''; oDebAuthDetails = ''; oGetDAInfoError = ''
*
    iInputDebitAuthority<PP.DebitAuthorityService.InputDADetails.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iInputDebitAuthority<PP.DebitAuthorityService.InputDADetails.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
*
    PP.DebitAuthorityService.getDebitAuthInfo(iInputDebitAuthority,oDebAuthDetails,oGetDAInfoError)
    IF oGetDAInfoError NE "" THEN
        scenarioCode = 6
        GOSUB updateResponseAndExit
    END
*
RETURN
*-----------------------------------------------------------------------------
getPorInformation:
* retrieve the payment information details
    iPaymentID = ''; oPaymentInformation = ''; oPaymentInfoError = ''
*
    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.informationCode> = "INSBNK"
    PP.InboundCodeWordService.getPaymentOrderInformation(iPaymentID, oPaymentInformation, oPaymentInfoError)
    IF oPaymentInfoError NE "" THEN
        scenarioCode = 7
        GOSUB updateResponseAndExit
    END
* retrieve the settlement date of original transaction
    orgstdtFMPOS = ''; orgstdtVMPOS = ''
    FIND 'ORGSTDT' IN oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.instructionCode> SETTING orgstdtFMPOS,orgstdtVMPOS THEN
        settlementDate = oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.informationLine,orgstdtVMPOS>
    END
* retrieve the requested collection date of original transaction
    orgrcldtFMPOS = ''; orgrcldtVMPOS = ''
    FIND 'ORGRCLDT' IN oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.instructionCode> SETTING orgrcldtFMPOS,orgrcldtVMPOS THEN
        reqCollectionDate = oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.informationLine,orgrcldtVMPOS>
    END
*
RETURN
*------------------------------------------------------------------------------
validateFields:
*
    iTransDetails = "" ; iPrtyDbtDetails = "" ; iOriginatingSource = "" ; iDebitAuthDets = "" ; iInformationDets = ""
    oValidChannelFlag = "" ; oValidateResponse = ""
* filling the payment details
    iTransDetails<PP.STEP2Service.TransDets.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
    iTransDetails<PP.STEP2Service.TransDets.ftNumber> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
    iTransDetails<PP.STEP2Service.TransDets.transactionAmount> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.transactionAmount>
    iTransDetails<PP.STEP2Service.TransDets.transactionCurrencyCode> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.transactionCurrencyCode>
    iTransDetails<PP.STEP2Service.TransDets.pmtDirection> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.paymentDirection>
    iTransDetails<PP.STEP2Service.TransDets.clearingTransactionType> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType>
    iTransDetails<PP.STEP2Service.TransDets.relatedIBAN> = relatedIBAN
* filling the debit party details
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyRole> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyRole>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyRoleIndicator> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyRoleIndicator>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyInformationTag> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyInformationTag>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyNationalID> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyNationalID>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyIdentifierCode>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyAccountLine>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyFreeLine1> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyFreeLine1>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyFreeLine2> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyFreeLine2>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyFreeLine3> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyFreeLine3>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyFreeLine4> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyFreeLine4>
    iPrtyDbtDetails<PP.STEP2Service.PartyDebitDets.debitPartyName> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyName>
    iPrtyDbtDetails<PP.DebitPartyDeterminationService.PORPartyDebit.debitPartyAddressLine1> = oPrtyDbtDetails<PP.DebitPartyDeterminationService.PartyDebitDetails.debitPartyAddressLine1>
* filling the originatingSource
    iOriginatingSource = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.originatingSource>
*
    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> MATCHES 'RF':@VM:'RD':@VM:'DD':@VM:'RV' THEN
*      filling some additional payment details
        iTransDetails<PP.STEP2Service.TransDets.bulkSendersReference> = oAdditionalPaymentRecord<PP.PaymentWorkflowDASService.AdditionalPaymentRecord.bulkSendersReference>
        iTransDetails<PP.STEP2Service.TransDets.customerSpecifiedReference> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.customerSpecifiedReference>
        iTransDetails<PP.STEP2Service.TransDets.sendersReferenceIncoming> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.sendersReferenceIncoming>
        iTransDetails<PP.STEP2Service.TransDets.clearingReturnCode> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingReturnCode>
        iTransDetails<PP.STEP2Service.TransDets.requestedCollectionDate> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.requestedCollectionDate>
        iTransDetails<PP.STEP2Service.TransDets.clearingNatureCode> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingNatureCode>
        iTransDetails<PP.STEP2Service.TransDets.bankOperationCode> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.bankOperationCode>
        iTransDetails<PP.STEP2Service.TransDets.detailsOfCharges> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.detailsOfCharges>
        iTransDetails<PP.STEP2Service.TransDets.batchIndicator> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.batchIndicator>
        iTransDetails<PP.STEP2Service.TransDets.relatedIBAN> = relatedIBAN
*      filling the mandate details
        iDebitAuthDets<PP.STEP2Service.DebitAuthDets.creditorID> = oDebAuthDetails<PP.DebitAuthorityService.DebAuthDetails.creditorID>
        iDebitAuthDets<PP.STEP2Service.DebitAuthDets.mandateReference> = oDebAuthDetails<PP.DebitAuthorityService.DebAuthDetails.mandateReference>
        iDebitAuthDets<PP.STEP2Service.DebitAuthDets.signatureDate> = oDebAuthDetails<PP.DebitAuthorityService.DebAuthDetails.signatureDate>
*      filling the information details
        iInformationDets<PP.STEP2Service.InformationDets.informationLine> = settlementDate:@VM:reqCollectionDate
*      calling the direct debit validation method
        PP.EGACHService.fieldValidationDD(iTransDetails, iPrtyDbtDetails, iCreditPartyDet, iOriginatingSource, iDebitAuthDets, iInformationDets, oValidChannelFlag, oValidateResponse)
        IF oValidateResponse NE '' AND oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.returnCode> EQ 'FAILURE' THEN
            LogEventType = 'ERR'
            LogEventDescription = ''
            LogErrorCode = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>
            LogAdditionalInfo = iChannelDetails<PP.STEP2Service.ChannelDetails.outputChannel>:' Reason: ':oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>
            GOSUB updateHistoryLog
        END
    END ELSE
*      calling the credit transfer validation method
        PP.EGACHService.fieldValidation(iTransDetails, iPrtyDbtDetails, iCreditPartyDet, iOriginatingSource, iDebitAuthDets, iInformationDets, oValidChannelFlag, oValidateResponse)
        IF oValidateResponse NE '' AND oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.returnCode> EQ 'FAILURE' THEN
            LogEventType = 'ERR'
            LogEventDescription = ''
            LogErrorCode = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>
            LogAdditionalInfo = iChannelDetails<PP.STEP2Service.ChannelDetails.outputChannel>:' Reason: ':oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>
            GOSUB updateHistoryLog
        END
    END
* checking the output returned
    IF oValidChannelFlag<PP.STEP2Service.ValidChannelFlag.validChannelFlag> NE "Y" THEN
        scenarioCode = 4
        GOSUB updateResponseAndExit
    END
*
RETURN
*-----------------------------------------------------------------------------
updateResponseAndExit:
    oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.returnCode>                                    = 'FAILURE'
    oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.serviceName>                                   = 'STEP2Service'
    oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageText>    = ''
    BEGIN CASE
        CASE scenarioCode = 1
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>    = 'STB00001'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>    = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>    = 'FATAL_ERROR'
        CASE scenarioCode = 2
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>    = 'STB10001'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>    = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>    = 'NON_FATAL_ERROR'
        CASE scenarioCode = 3
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>    = 'STB10002'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>    = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>    = 'NON_FATAL_ERROR'
        CASE scenarioCode = 4
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.returnCode>                                        = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.returnCode>
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.serviceName>                                       = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.serviceName>
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>    = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>    = iChannelDetails<PP.STEP2Service.ChannelDetails.outputChannel>:' Reason: ':oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>    = oValidateResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>
        CASE scenarioCode = 5
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10004'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
        CASE scenarioCode = 6
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10005'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
        CASE scenarioCode = 7
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10006'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
        CASE scenarioCode = 8
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10007'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
        CASE scenarioCode = 9
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10008'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageText>  = "FF01" ;* When this is the last channel defined in PP.CONTRACT, then this error will also get update into Payment response object
        CASE scenarioCode = 10
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10009'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageText>  = 'Additional Info length cannot be more than 140'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
        CASE scenarioCode = 11
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageCode>  = 'STB10010'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageText>  = 'Structured cannot be expanded more than 999 times'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageType>  = 'NON_FATAL_ERROR'
            oChannelResponse<PP.CreditPartyDeterminationService.PaymentResponse.responseMessages,1,PP.CreditPartyDeterminationService.ResponseMessage.messageInfo>  = ''
    END CASE
*
    CALL TPSLogging("Output Parameters - END","STEP2Service.validateMessage","oChannelResponse:<":oChannelResponse:">","")
    GOSUB exit
*
RETURN
*------------------------------------------------------------------------------
getAccInfoDetails:
** Assign input and output for the method getAccInfoDetails in order to get relatedIBAN value
    iTransAccDetails = '' ; oAccInfoDetails = '' ; oGetAccError = ''
    iTransAccDetails<PP.DebitPartyDeterminationService.InputTransactionAccDetails.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iTransAccDetails<PP.DebitPartyDeterminationService.InputTransactionAccDetails.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iTransAccDetails<PP.DebitPartyDeterminationService.InputTransactionAccDetails.mainOrChargeAccType> = mainOrChargeAccType
*
    PP.DebitPartyDeterminationService.getAccInfoDetails(iTransAccDetails, oAccInfoDetails, oGetAccError)
*
*    IF oGetAccError<DASError.error> NE "" THEN
*        scenarioCode = 5
*        GOSUB updateResponseAndExit
*    END
*
RETURN
*-----------------------------------------------------------------------------
* The below paragraph should get the DebitInstructAmount from POR.PAYMENTFLOWDETAILS, If value present, then
* Instructed Currency value to be get from POR.TRANSACTION, the value should not contains the currency EUR.
* If it is EUR, then this channel is considered to be invalid - 33B
getDebitInstructedAmt:

    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>

    PP.PaymentFrameworkService.getPORPaymentFlowDetails(iPORPmtFlowDetailsReq, oPORPmtFlowDetailsList, oPORPmtFlowDetailsGetError)  ;* To read POR.PAYMENTFLOWDETAILS table

    IF oPORPmtFlowDetailsGetError EQ '' THEN  ;* If record present, then
        debitInstructAmtPresent = oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.debitInstructAmount>  ;*Assign the DebitInstructAmount value
    END

* 33 B
RETURN
*------------------------------------------------------------------------------
updateHistoryLog:
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventType> = LogEventType
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventDescription> = LogEventDescription
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.errorCode> = LogErrorCode
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.additionalInfo> = LogAdditionalInfo

    PP.PaymentFrameworkService.insertPORHistoryLog(iPORHistoryLog, oPORHistoryLogError)  ;* To update POR.HISTORYLOG table

RETURN
*------------------------------------------------------------------------------
exit:
RETURN TO exit
*-----------------------------------------------------------------------------
*** <region name= validateRemittanceInformation>
validateRemittanceInformation:
*** <desc> Paragraph to check whether the Remittance Information is with in the length as part Sepa 2019 RB </desc>
    IF oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.paymentMethod> MATCHES "INST":@VM:"NRINST" THEN
        RETURN      ;* The below validation is not applicable for Instant or Near real Instant payments
    END
    
    Vpos = ''
    remLengthCount = DCOUNT(oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName>, @VM)
    FOR count = 1 TO remLengthCount
        IF  oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName,count> EQ 'RemittanceStrdLength' THEN
            remittanceStrdln<-1> = oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldValue,count>
        END
    NEXT count
    Vpos = ''
    LOCATE 'RemittanceUstrdLength' IN oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName,1> SETTING Vpos THEN
        remittanceUstrdln = oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldValue,Vpos>
    END
    
* get the POR.RemittanceInfStructured details.
    oGetRemittanceInfStructuredError = ''
    iRemittanceInfStructuredKey = ''
    oGetRemittanceInfStructured = ''
    lclInstNotPERI = ''
    remitNonCrdRefInfFields = ''
   
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.ftNumber> =  iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    PP.InwardCreditTransferInitiationService.getPORRemittanceInfStructuredDetails(iRemittanceInfStructuredKey, oGetRemittanceInfStructured, oGetRemittanceInfStructuredError)
*   All Remittance fields other than crdRefInfTpCdOrPropCd, crdRefInfTpIssuer and crdRefInfRef
    remitInfoFields = PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocInfTpCdOrPropCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocInfTpCdOrPropProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocInfTpIssuer:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocInfNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocInfRelatedDt:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDuePayableAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDuePayableAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDiscApplAmTpCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDiscApplAmTpProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDiscApplAmAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmDiscApplAmAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmCrNoteAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmCrNoteAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmTaxAmTpCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmTaxAmTpProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmTaxAmAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmTaxAmAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmAdjAmRsnAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmAdjAmRsnAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmAdjAmRsnCrDbInd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmAdjAmRsnRsn:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmAdjAmRsnAdInf:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmRemittedAm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.refDocAmRemittedAmCcy:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfTpCdOrPropProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddTp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddDep:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddSubDep:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddStreetNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddBuildingNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddPostCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddTownNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddCtrySubDiv:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddCtry:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine1:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine2:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine3:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine4:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine5:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine6:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrPostAddLine7:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdOrgIdAnyBIC:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdOrgIdOthId:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdOrgIdOthSchNmCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdOrgIdOthSchNmProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdOrgIdOthIssuer:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdDtPlOfBrBrDt:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdDtPlOfBrProvOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdDtPlOfBrCityOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdDtPlOfBrCtryOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdOthId:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdOthSchNmCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdOthSchNmProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrIdPrvIdOthIssuer:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrCtryOfResidence:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetNmPrefix:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetPhoneNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetMobileNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetFaxNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetEmailAdd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invrContactDetOth:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddTp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddDep:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddSubDep:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddStreetNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddBuildingNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddPostCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddTownNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddCtrySubDiv:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddCtry:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine1:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine2:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine3:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine4:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine5:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine6:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.invePostAddLine7:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdOrgIdAnyBIC:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdOrgIdOthId:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdOrgIdOthSchNmCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdOrgIdOthSchNmProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdOrgIdOthIssuer:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdDtPlOfBrBrDt:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdDtPlOfBrProvOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdDtPlOfBrCityOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdDtPlOfBrCtryOfBr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdOthId:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdOthSchNmCd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdOthSchNmProp:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveIdPrvIdOthIssuer:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveCtryOfResidence:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetNmPrefix:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetNm:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetPhoneNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetMobileNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetFaxNr:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetEmailAdd:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.inveContactDetOth:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.adRemittanceInf1:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.adRemittanceInf2:@VM:PP.InwardCreditTransferInitiationService.RemittanceInfStructured.adRemittanceInf3
    remitFieldCount = DCOUNT(remitInfoFields,@VM)
    FOR remCnt =1 TO remitFieldCount
*       When any filed contains value other than crdRefInfTpCdOrPropCd, crdRefInfTpIssuer and crdRefInfRef
        IF oGetRemittanceInfStructured<remitInfoFields<1,remCnt>> NE '' THEN
            remitNonCrdRefInfFields = 'Yes'
        END
    NEXT remCnt

*   when only crdRefInfTpCdOrPropCd, crdRefInfTpIssuer and crdRefInfRef is available
    IF remitNonCrdRefInfFields EQ '' AND (oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfTpCdOrPropCd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfTpIssuer> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfRef>) THEN
        lclInstNotPERI = 'YES'
    END
   
    Vpos = ''
    aosList = ''
    LOCATE 'BenBankAOSList' IN oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName,1> SETTING Vpos THEN
        aosList =  oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldValue,Vpos>
    END

    IF aosList EQ '' THEN       ;* When this aosList is null, then skip further processing in this paragraph
*       When aosList is not equal to ERI then localinstrumentation code should be empty
        IF lclInstNotPERI EQ 'YES' THEN
            clInsCodeValue = ''
            GOSUB updatePorInformation
        END
        RETURN                  ;* go out of this paragraph
    END
**
    recPORInformation = ''
    errPORInformation = ''

    PP.PaymentWorkflowGUI.getSupplementaryInfo('POR.INFORMATION', iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>, '', recPORInformation, errPORInformation)

*   when no Structured remittance and only unstructured is available
    IF remitNonCrdRefInfFields EQ '' AND (oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfTpCdOrPropCd> EQ '' AND oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfTpIssuer> EQ '' AND oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.RemittanceInfStructured.crdRefInfRef>EQ '') THEN
        lclInstNotPERI = 'YES'
    END

    BEGIN CASE
        CASE lclInstNotPERI  ;*When Not Extended Remittance or only unstructured
            
            clInsCodeValue = ''
            GOSUB updatePorInformation

        CASE recPORInformation      ;* If record is present, then
            infoCode = recPORInformation<PP.PaymentWorkflowGUI.PorInformation.Informationcode>
            instructCode = recPORInformation<PP.PaymentWorkflowGUI.PorInformation.Instructioncode>
            
        
            LOCATE "LCLINSCD" IN instructCode<1,1> SETTING lclPos THEN
                clInsCodeValue = recPORInformation<PP.PaymentWorkflowGUI.PorInformation.Informationline,lclPos>
            END
            IF clInsCodeValue NE "PERI" AND aosList EQ "ERI" THEN
                clInsCodeValue = "PERI"
                GOSUB updatePorInformation      ; *Paragraph to update POR Information to update PERI in Information line
            END
            remStrdCount = DCOUNT(remittanceStrdln, @FM)
            FOR remStrd =1 TO remStrdCount
                IF clInsCodeValue EQ "PERI" AND aosList EQ "ERI" AND remittanceStrdln<remStrd> AND remittanceStrdln<remStrd> GT "280" THEN
                    GOSUB setErrorScenario9
                END
            NEXT remStrd
        
        CASE aosList EQ "ERI"   ;* When the record is not present, the but the receiving bic is supports ERI
            clInsCodeValue = "PERI"
            
            GOSUB updatePorInformation ; *Paragraph to update POR Information to update PERI in Information line
            remStrdCount = DCOUNT(remittanceStrdln, @FM)
            FOR remStrd =1 TO remStrdCount
                IF clInsCodeValue EQ "PERI" AND aosList EQ "ERI" AND remittanceStrdln<remStrd> AND remittanceStrdln<remStrd> GT "280" THEN
                    GOSUB setErrorScenario9
                END
            NEXT remStrd
    END CASE

* get the POR.RemittanceInfStructured details.
    oGetRemittanceInfStructuredError = ''
    iRemittanceInfStructuredKey = ''
    oGetRemittanceInfStructured = ''
   
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.ftNumber> =  iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    PP.InwardCreditTransferInitiationService.getPORRemittanceInfStructuredDetails(iRemittanceInfStructuredKey, oGetRemittanceInfStructured, oGetRemittanceInfStructuredError)

    IF oGetRemittanceInfStructured NE '' THEN
        numberRepeat = ''
        docInfStr = ''
* we comment this the below line accourding to field Structure is not exist - ITSS 28/10/2021
*  oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.Structured> = ''
        noOfStrdElements = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd>,@VM)
        IF noOfStrdElements EQ 0 THEN
            noOfStrdElements = 1
        END
        FOR strd=1 TO  noOfStrdElements
            noOfDocInf = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd>,@SM)
            IF noOfDocInf EQ 0 THEN
                noOfDocInf = 1
            END
            FOR docInf=1 TO  noOfDocInf
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropProp,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpIssuer,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf> THEN
                    docInfStr = '<RfrdDocInf>'
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropProp,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpIssuer,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf> THEN
                        docInfStr:= '<Tp>'
                        IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd,docInf> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropProp,strd,docInf> THEN
                            docInfStr:= '<CdOrPrtry>'
                            
                            IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd,docInf> THEN
                                docInfStr:= '<Cd>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd,strd,docInf>:'</Cd>'
                            END
                            IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropProp,strd,docInf> THEN
                                docInfStr:= '<Prtry>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropProp,strd,docInf>:'</Prtry>'
                            END
                            docInfStr:= '</CdOrPrtry>'
                        END
                        IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpIssuer,strd,docInf> THEN
                            docInfStr:= '<Issr>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpIssuer,strd,docInf>:'</Issr>'
                        END
                    
                        docInfStr:= '</Tp>'
                        IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd,docInf> THEN
                            docInfStr:=  '<Nb>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd,docInf>:'</Nb>'
                            numberRepeat = oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd,docInf>
                        END
                        
                        IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf> THEN
                            docInfStr:= '<RltdDt>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf>[1,4]:'-':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf>[5,2]:'-':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfRelatedDt,strd,docInf>[7,2]:'</RltdDt>'
                        END
                    END
                    docInfStr:= '</RfrdDocInf>'
                END
            NEXT docInf
            docInfStrDocInf = docInfStr
            IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDuePayableAm,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDiscApplAmAm,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmCrNoteAm,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmTaxAmAm,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAm,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnCrDbInd,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnRsn,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAdInf,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmRemittedAm,strd> THEN
                docInfStr:= '<RfrdDocAmt>'
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDuePayableAm,strd> THEN
                    docInfStr:= '<DuePyblAmt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDuePayableAmCcy,strd>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDuePayableAm,strd>:'</DuePyblAmt>'
                END
                noOfDiscAmts = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDiscApplAmAm,strd>,@SM)
                FOR disAmt = 1 TO noOfDiscAmts
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDiscApplAmAm,strd,disAmt> THEN
                        docInfStr:= '<DscntApldAmt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDiscApplAmAmCcy,strd,disAmt>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDiscApplAmAm,strd,disAmt>:'</DscntApldAmt>'
                    END
                NEXT disAmt
            
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmCrNoteAm,strd> THEN
                    docInfStr:= '<CdtNoteAmt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmCrNoteAmCcy,strd>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmCrNoteAm,strd>:'</CdtNoteAmt>'
                END
                noOfTaxAmts = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmTaxAmAm,strd>,@SM)
                FOR taxAmt = 1 TO noOfTaxAmts
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmTaxAmAm,strd,taxAmt> THEN
                        docInfStr:= '<TaxAmt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmTaxAmAmCcy,strd,taxAmt>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmTaxAmAm,strd,taxAmt>:'</TaxAmt>'
                    END
                NEXT taxAmt
            
                noOfRsns = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnRsn,strd>,@SM)
                IF noOfRsns EQ 0 THEN
                    noOfRsns = 1
                END
                FOR rsn=1 TO noOfRsns
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAm,strd,rsn> THEN
                        docInfStr:= '<Amt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAmCcy,strd,rsn>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAm,strd,rsn>:'</Amt>'
                    END
            
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnCrDbInd,strd,rsn> THEN
                        docInfStr:= '<CdtDbtInd>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnCrDbInd,strd,rsn>:'</CdtDbtInd>'
                    END
            
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnRsn,strd,rsn> THEN
                        docInfStr:= '<Rsn>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnRsn,strd,rsn>:'</Rsn>'
                    END
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAdInf,strd,rsn> THEN
                        docInfStr:=  '<AddtlInf>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmAdjAmRsnAdInf,strd,rsn>:'</AddtlInf>'
                    END
                NEXT rsn
            
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmRemittedAm,strd> THEN
                    docInfStr:= '<RmtdAmt Ccy=':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmRemittedAmCcy,strd>:'>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmRemittedAm,strd>:'</RmtdAmt>'
                END
                docInfStr:= '</RfrdDocAmt>'
            END
            docInfStrDocAmt = docInfStr
        
            IF LEN(docInfStr) GT 280 THEN
                GOSUB addNewOccurance
                FOR fld = PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmDuePayableAm TO PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocAmRemittedAmCcy
                    oGetRemittanceInfStructured<fld,strd+1> = oGetRemittanceInfStructured<fld,strd>
                    oGetRemittanceInfStructured<fld,strd> = ''
                NEXT fld
                oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd+1> = numberRepeat
                docInfStr = docInfStrDocInf
            END
    
            IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropCd,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropProp,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpIssuer,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfRef,strd> THEN
                docInfStr:= '<CdtrRefInf>'
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropCd,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropProp,strd> OR oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpIssuer,strd> THEN
                    docInfStr:= '<Tp>'
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropCd,strd> THEN
                        docInfStr:=  '<CdOrPrtry><cd>SCOR</cd></CdOrPrtry>'
                    END
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropProp,strd> THEN
                        docInfStr := '<CdOrPrtry><Prtry>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropProp,strd>:'</Prtry></CdOrPrtry>'
                    END
                    IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpIssuer,strd> THEN
                        docInfStr:= '<Issr>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpIssuer,strd>:'</Issr>'
                    END
                    docInfStr:= '</Tp>'
                END
                IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfRef,strd> THEN
                    docInfStr:= '<Ref>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfRef,strd>:'</Ref>'
                END
                docInfStr:= '</CdtrRefInf>'
            END
            docInfStrCdtRef = docInfStr
            IF LEN(docInfStr) GT 280 THEN
                GOSUB addNewOccurance
                FOR fld = PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfTpCdOrPropCd TO PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.crdRefInfRef
                    oGetRemittanceInfStructured<fld,strd+1> = oGetRemittanceInfStructured<fld,strd>
                    oGetRemittanceInfStructured<fld,strd> = ''
                NEXT fld
                oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd+1> = numberRepeat
                docInfStr = docInfStrDocAmt
            END
        
            IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf1,strd> THEN
                docInfStr:= '<AddtlRmtInf>':oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf1,strd>:'</AddtlRmtInf>'
            END
        
            IF LEN(docInfStr) GT 280 THEN
                GOSUB addNewOccurance
                oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf1,strd+1> = oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf1,strd>
                oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf1,strd> = ''
                oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfNr,strd+1> = numberRepeat
            END
            
            docInfStr = ''
            numberRepeat = ''
            
        NEXT strd
* we comment this the below line accourding to field Structure is not exist - ITSS 28/10/2021
*IF oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.Structured> NE '' THEN
* iout = ''
            
* PP.InwardCreditTransferInitiationService.insertPORRemittanceInfStructured(oGetRemittanceInfStructured, iout)
*END
        GOSUB validateRemittanceExpansion ;* validate struct remittance fields expansion upto 999 times
    END
RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= updatePorInformation>
updatePorInformation:
*** <desc>Paragraph to update POR Information to update PERI in Information line </desc>
    iPaymentID = ''
    oPaymentInformation = ''
    oPaymentInfoError = ''
    iPaymentOrderInfo = ''
    oInsertInfoErr = ''
    periInfoLinePos = ''

    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iPaymentID<PP.InboundCodeWordService.PaymentInfoKeys.informationCode> = "INSBNK"
    
    PP.InboundCodeWordService.getPaymentOrderInformation(iPaymentID, oPaymentInformation, oPaymentInfoError)
    
    IF oPaymentInformation NE '' THEN
        countPorInfTypelineSeq = DCOUNT(oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.informationTypeLineSequence>,@SM)
        countPorInfTypelineSeq = countPorInfTypelineSeq + 1
    END ELSE
        countPorInfTypelineSeq = 1
    END
    IF lclInstNotPERI EQ 'YES' THEN
        LOCATE 'PERI' IN oPaymentInformation<PP.InboundCodeWordService.PaymentInformation.informationLine,1,1> SETTING periInfoLinePos THEN
            countPorInfTypelineSeq = periInfoLinePos
        END
    END
        
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.ftNumber> = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.informationCode> = "INSBNK"
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.instructionCode> = "LCLINSCD"
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.informationTypeLineSequence> = countPorInfTypelineSeq
    iPaymentOrderInfo<PP.InboundCodeWordService.PaymentOrderInfo.informationLine> = clInsCodeValue  ;* Ex: PERI
    PP.InboundCodeWordService.insertPORInformation(iPaymentOrderInfo, oInsertInfoErr) ;* To update POR.INFORMATION in POR.SUPPLEMETARY.INFO
RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= setErrorScenario9>
setErrorScenario9:
*** <desc> </desc>
    scenarioCode = 9
    IF iChannelDetails<PP.STEP2Service.ChannelDetails.rsOutputChannelImposed> EQ 'Y' THEN  ;* If the Output channel 'RPSSCL' is imposed on OE Screen,
        LogEventType = 'ERR'
        LogEventDescription = 'clInsCodeValue / aosList ':clInsCodeValue:' / ':aosList
        LogErrorCode = 'STB10008'
        LogAdditionalInfo = 'STEP2'
        GOSUB updateHistoryLog
    END
    GOSUB updateResponseAndExit
RETURN
*** </region>
*-----------------------------------------------------------------------------
additionalInfo:
* read the additional info
    iAdditionalInf<PP.DuplicateCheckService.PORAdditionalInf.companyID>     = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iAdditionalInf<PP.DuplicateCheckService.PORAdditionalInf.ftNumber>      = iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    iAdditionalInf<PP.DuplicateCheckService.PORAdditionalInf.additionalInformationCode>  = "RMTINF"
    PP.DuplicateCheckService.getPORAdditionalInf(iAdditionalInf, oAdditionalInf, oAdditionalInfError)
* retrieve the additional info and check the length
    additionalInfLine = oAdditionalInf<PP.DuplicateCheckService.AdditionalInfDetails.additionalInfLine>
    addinfline =''
    CHANGE @FM TO @VM IN additionalInfLine
    LOOP
        REMOVE additionalInfLine.VAL FROM additionalInfLine SETTING ADDINF.POS
    WHILE additionalInfLine.VAL:ADDINF.POS
        IF addinfline EQ '' THEN
            addinfline = addinfline:additionalInfLine.VAL
        END ELSE
            addinfline = addinfline:' ':additionalInfLine.VAL ;* adding space inbetween, each info line
        END
    REPEAT
    addinflen = LEN(addinfline)
* if length is more then 140 throw an error to the user
    IF addinflen GT '140' THEN
        scenarioCode = 10
        GOSUB updateResponseAndExit
    END
    
RETURN
*------------------------------------------------------------------------------
*** <region name= addNewOccurance>
*------------------------------------------------------------------------------
addNewOccurance:
* we comment this the below line accourding to field Structure is not exist - ITSS 28/10/2021
* oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.Structured> = 'YES'
    FOR field = PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd TO PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.adRemittanceInf3
        INS '' BEFORE oGetRemittanceInfStructured<field, strd+1>
    NEXT field
    noOfStrdElements = noOfStrdElements + 1
RETURN
*** </region>
*-----------------------------------------------------------------------------
*** <region name= validateRemittanceExpansion>
validateRemittanceExpansion:
* check remittance structured can be expanded only upto 999  times
    oGetRemittanceInfStructured = ''
    oGetRemittanceInfStructuredError = ''
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.companyID> = iChannelDetails<PP.STEP2Service.ChannelDetails.companyID>
    iRemittanceInfStructuredKey<PP.InwardCreditTransferInitiationService.RemittanceInfStructuredKey.ftNumber> =  iChannelDetails<PP.STEP2Service.ChannelDetails.ftNumber>
    PP.InwardCreditTransferInitiationService.getPORRemittanceInfStructuredDetails(iRemittanceInfStructuredKey, oGetRemittanceInfStructured, oGetRemittanceInfStructuredError)

    noOfStructElements = DCOUNT(oGetRemittanceInfStructured<PP.InwardCreditTransferInitiationService.PORRemittanceInfStructured.refDocInfTpCdOrPropCd>,@VM)
    IF noOfStructElements GT 999 THEN
        scenarioCode = 11
        GOSUB updateResponseAndExit
    END

RETURN
*** </region>
*-----------------------------------------------------------------------------
END