* @ValidationCode : MjotMTUwMDc4MzM0MTpDcDEyNTI6MTYzNTIzOTcxMDUxNDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 26 Oct 2021 11:15:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*------------------------------------------------------------------------------
*
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.validateGUI
*------------------------------------------------------------------------------
* In/out parameters:
*------------------------------------------------------------------------------
    $USING EB.SystemTables
    $USING EB.DataAccess
    $USING EB.ErrorProcessing
    $USING PP.ClearingFrameworkService
    $USING PP.BankCodeService
    $USING PP.STEP2Service
*------------------------------------------------------------------------------
* Logging
*    CALL TPSLogging("Start","STEP2.validateGUI","","")
*    CALL TPSLogging("Version","STEP2.validateGUI","Task - 1560943, Date - 12-APR-2016","")

    GOSUB initialise
    GOSUB process

*Logging
*    CALL TPSLogging("End","STEP2.validateGUI","","")

RETURN
*------------------------------------------------------------------------------
process:
* Main process para
    GOSUB mandatoryCheck
    GOSUB validateBIC
    GOSUB validateReceiverCode

*
RETURN
*------------------------------------------------------------------------------
initialise:
*-------------------
    currencyCode = ''
    participationtype = ''
    receiver = ''
    iGetBICContext = ''
    oBICTableDetails = ''
    oGetBICDetsError = ''
    CompId = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldCompanyid)
*
RETURN

*------------------------------------------------------------------------------
validateReceiverCode:
*-------------------
    participationtype = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldParticipationtype)
    receiver = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldReceiver)
    IF participationtype EQ "IP" THEN
        IF receiver EQ "" THEN
            EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldReceiver)
            EB.SystemTables.setEtext("PP-MAND.FIELD")
            EB.ErrorProcessing.StoreEndError()
        END ELSE
            bicFieldName = "Receiver"
            bicCode = receiver
            GOSUB checkBICCode
        END
    END
*
RETURN
*------------------------------------------------------------------------------
mandatoryCheck:
* Mandatory field check for BicCode, Receiver and the AccountHolder

    bicCode = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldBiccode)
    IF bicCode EQ "" THEN
        EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldBiccode)
        EB.SystemTables.setEtext("PP-MAND.FIELD")
        EB.ErrorProcessing.StoreEndError()
    END

    accountHolder = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldAccountholder)
    IF accountHolder EQ "" THEN
        EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldAccountholder)
        EB.SystemTables.setEtext("PP-MAND.FIELD")
        EB.ErrorProcessing.StoreEndError()
    END
*
RETURN
*-----------------------------------------------------------------------------
validateBIC:
*-------------------
* BICCode, Receiver and AccountHolder fields must be valid bicCodes in PPT.BICTABLE table.
    IF EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldBiccode) NE "" THEN
        bicCode = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldBiccode)
        bicFieldName = "BICCode"
        GOSUB checkBICCode
    END

    bicCode = ''
    bicCode = EB.SystemTables.getRNew(PP.ClearingFrameworkService.ClearingDirectory.CldAccountholder)
    IF bicCode NE '' THEN
        bicFieldName = "AccountHolder"
        GOSUB checkBICCode
    END

RETURN
*------------------------------------------------------------------------------
checkBICCode:
*-------------------
* The BIC has ONLY two forms: one 11 chars, the other 8 chars. The length of 6,4,2 are only taken into considerations
* when no records are found for 8 and 11.
* Should be valid BICCode from the table PPT.BICTABLE of the selected company.
* BICCode lengtH more than 1 and start with "*" is not allowed.
*
    lenBIC = LEN(bicCode)
    
    IF ((lenBIC EQ '11') OR (lenBIC EQ '8')) THEN
        iGetBICContext<PP.BankCodeService.BICContext.companyID> = CompId   ;* company Id
        iGetBICContext<PP.BankCodeService.BICContext.bicCode> = bicCode  ;* BIC code to be validated
        GOSUB getBICCodeList
    END ELSE
        GOSUB checkBICFieldName
        EB.SystemTables.setEtext("PP-INVALID-BICCODE")
        EB.ErrorProcessing.StoreEndError()
    END
 
RETURN
*------------------------------------------------------------------------------
getBICCodeList:
*-------------------
;* direct READ and SELECT on PPT.BICTABLE is converted to API call to support future enhancements.
 
    PP.BankCodeService.getBICDetails(iGetBICContext, oBICTableDetails, oGetBICDetsError)
    IF oBICTableDetails EQ '' THEN  ;* when the given BIC is not in the BIC table, raise error
        GOSUB checkBICFieldName
        EB.SystemTables.setEtext("PP-INVALID-BICCODE")
        EB.ErrorProcessing.StoreEndError()
    END

RETURN
*------------------------------------------------------------------------------
checkBICFieldName:
*-------------------
    BEGIN CASE
        CASE bicFieldName EQ "BICCode"
            EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldBiccode)
        CASE bicFieldName EQ "Receiver"
            EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldReceiver)
        CASE bicFieldName EQ "AccountHolder"
            EB.SystemTables.setAf(PP.ClearingFrameworkService.ClearingDirectory.CldAccountholder)
    END CASE

RETURN
*------------------------------------------------------------------------------
END
