<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:camt.056.001.01"
    exclude-result-prefixes="xs ns0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" /> 
    <xsl:strip-space elements="*" />
    <xsl:template match="/">
       <T24GenericInputFile>
           <xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">file:///C:/Users/mgodeanu/Desktop/StandardInputGenericXML.xsd</xsl:attribute>
           <FileInfo>
                <FileName>.</FileName>
                <QueueName>.</QueueName>
                <ReceivedDate>.</ReceivedDate>
                <BulkIndex>.</BulkIndex>
                <TransactionIndex>.</TransactionIndex>
                <UniqueReference>.</UniqueReference>
                <ProcessingStatus>RECEIVED</ProcessingStatus>
                <ProcessingStatusDescription>.</ProcessingStatusDescription>
                <Content>.</Content>
            </FileInfo>
			<FileHeader>
                <xsl:if test="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC">
                    <FileHeaderSendingInstitution>
                        <xsl:value-of select="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC"/>
                    </FileHeaderSendingInstitution>
                </xsl:if>
                <xsl:if test="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Assgne/ns0:Agt/ns0:FinInstnId/ns0:BIC">
                    <FileHeaderReceivingInstitution>
                        <xsl:value-of select="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Assgne/ns0:Agt/ns0:FinInstnId/ns0:BIC"/>   
                    </FileHeaderReceivingInstitution>
                </xsl:if>
                <FileMessageFormat>camt.056.001.01</FileMessageFormat>
                <xsl:if test="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Id">
                    <FileHeaderFileReference>
                        <xsl:value-of select="ns0:Document/ns0:FIToFIPmtCxlReq/ns0:Assgnmt/ns0:Id"/>    
                    </FileHeaderFileReference>
                </xsl:if>
            </FileHeader>
			<FileBulks>
                <CancellationRequests>
                    <TotalPaymentCancellationRequestBulks>
                        <xsl:value-of select="string(count(ns0:Document/ns0:FIToFIPmtCxlReq))" />
                    </TotalPaymentCancellationRequestBulks>
                    <xsl:for-each select="//ns0:FIToFIPmtCxlReq">
                        <Bulk>
                            <BulkHeader>
                                <xsl:if test="./ns0:Assgnmt/ns0:Id">
                                    <BulkSendersReference>
                                        <xsl:value-of select="./ns0:Assgnmt/ns0:Id"/>     
                                    </BulkSendersReference>
                                </xsl:if>   
			                    <xsl:if test="./ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC">
                                    <BulkInstructingAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC"/>   
                                    </BulkInstructingAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if>
                                <xsl:if test="./ns0:Assgnmt/ns0:Assgne/ns0:Agt/ns0:FinInstnId/ns0:BIC">
                                    <BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:Assgnmt/ns0:Assgne/ns0:Agt/ns0:FinInstnId/ns0:BIC"/>   
                                    </BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if>
                                <xsl:if test="./ns0:Assgnmt/ns0:CreDtTm">
                                    <BulkCreationDateTime>
                                        <xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(./ns0:Assgnmt/ns0:CreDtTm), '1', '4'), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '6', '2')), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '9', '2')), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '12', '2')), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '15', '2')), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '18', '2')), substring(string(./ns0:Assgnmt/ns0:CreDtTm), '21', '3'))" />        
                                    </BulkCreationDateTime>
                                </xsl:if>
                                <xsl:if test="./ns0:CtrlData/ns0:NbOfTxs">
                                    <BulkNumberOfTransactions>
                                        <xsl:value-of select="./ns0:CtrlData/ns0:NbOfTxs"/>    
                                    </BulkNumberOfTransactions>
                                </xsl:if>
                                <BulkAmountCurrency>EUR</BulkAmountCurrency>  
                                <BulkSchemeIndicator>C</BulkSchemeIndicator>
                                <BulkFormat>camt.056</BulkFormat> 
                                <xsl:if test="./ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC">
                                    <AssignerFinancialInstitutionIdentification>
                                        <BICFI>
                                            <xsl:value-of select="./ns0:Assgnmt/ns0:Assgnr/ns0:Agt/ns0:FinInstnId/ns0:BIC"/>
                                        </BICFI>   
                                    </AssignerFinancialInstitutionIdentification>
                                </xsl:if>
                            </BulkHeader>
							<xsl:for-each select="//ns0:Undrlyg/ns0:TxInf">
                                <Transaction>
                                    <xsl:if test="./ns0:CxlId">
                                        <CancellationIdentification>
                                            <xsl:value-of select="./ns0:CxlId"/>    
                                        </CancellationIdentification>
                                    </xsl:if>   
									<xsl:if test="./ns0:OrgnlGrpInf/ns0:OrgnlMsgId">
                                        <OriginalMessageIdentification>
                                            <xsl:value-of select="./ns0:OrgnlGrpInf/ns0:OrgnlMsgId"/>
                                        </OriginalMessageIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId ">
                                        <OriginalMessageNameIdentification>
                                            <xsl:value-of select="./ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId"/>    
                                        </OriginalMessageNameIdentification>
                                    </xsl:if>							
                                    <xsl:if test="./ns0:OrgnlEndToEndId">
                                        <OriginalEndToEndIdentification>
                                            <xsl:value-of select="./ns0:OrgnlEndToEndId"/>
                                        </OriginalEndToEndIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxId">
                                        <OriginalTransactionIdentification>
                                            <xsl:value-of select="./ns0:OrgnlTxId"/>   
                                        </OriginalTransactionIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlIntrBkSttlmAmt">
                                        <OriginalInterbankSettlementAmount>
                                            <xsl:value-of select="./ns0:OrgnlIntrBkSttlmAmt"/>   
                                        </OriginalInterbankSettlementAmount>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlIntrBkSttlmAmt/@Ccy">
                                        <OriginalInterbankSettlementAmountCurrency>
                                            <xsl:value-of select="./ns0:OrgnlIntrBkSttlmAmt/@Ccy"/>   
                                        </OriginalInterbankSettlementAmountCurrency>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlIntrBkSttlmDt">
                                        <OriginalInterbankSettlementDate>
                                            <xsl:value-of select="concat(concat(substring(string(./ns0:OrgnlIntrBkSttlmDt), '1', '4'), substring(string(./ns0:OrgnlIntrBkSttlmDt), '6', '2')), substring(string(./ns0:OrgnlIntrBkSttlmDt), '9', '2'))" />        
                                        </OriginalInterbankSettlementDate>
                                    </xsl:if>
									<xsl:if test="(./ns0:CxlRsnInf/ns0:Orgtr/ns0:Nm) or 
                                                  (./ns0:CxlRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI)">
                                        <CancellationReasonInformationOriginator>
                                            <xsl:if test="./ns0:CxlRsnInf/ns0:Orgtr/ns0:Nm">
                                                <Name>
                                                    <xsl:value-of select="./ns0:CxlRsnInf/ns0:Orgtr/ns0:Nm"/>
                                                </Name>
                                            </xsl:if>
                                            <xsl:if test="./ns0:CxlRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
                                                <AnyBIC>
                                                    <xsl:value-of select="./ns0:CxlRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>    
                                                </AnyBIC>
                                            </xsl:if>
                                        </CancellationReasonInformationOriginator>
                                    </xsl:if>
									<xsl:if test="(./ns0:CxlRsnInf/ns0:Rsn/ns0:Cd) or
                                                  (./ns0:CxlRsnInf/ns0:Rsn/ns0:Prtry)">
                                        <CancellationReasonInformationReason>
                                            <xsl:if test="./ns0:CxlRsnInf/ns0:Rsn/ns0:Cd">
                                                <Code>
                                                    <xsl:value-of select="./ns0:CxlRsnInf/ns0:Rsn/ns0:Cd"/>    
                                                </Code>
                                            </xsl:if>
                                            <xsl:if test="./ns0:CxlRsnInf/ns0:Rsn/ns0:Prtry">
                                                <Proprietary>
                                                    <xsl:value-of select="./ns0:CxlRsnInf/ns0:Rsn/ns0:Prtry"/>    
                                                </Proprietary>
                                            </xsl:if>
                                        </CancellationReasonInformationReason>
                                    </xsl:if>
									<xsl:if test="./ns0:CxlRsnInf/ns0:AddtlInf">
                                        <CancellationReasonInformationAdditionalInformation>
                                            <xsl:value-of select="./ns0:CxlRsnInf/ns0:AddtlInf"/>    
                                        </CancellationReasonInformationAdditionalInformation>
                                    </xsl:if>							
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAcct/ns0:Id/ns0:IBAN">
                                        <OriginalDebtorAccountIdentification>
                                            <IBAN>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAcct/ns0:Id/ns0:IBAN"/>    
                                            </IBAN>
                                        </OriginalDebtorAccountIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalDebtorAgentFinancialInstitutionIdentification>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC"/>   
                                            </BICFI>
                                        </OriginalDebtorAgentFinancialInstitutionIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalCreditorAgentFinancialInstitutionIdentification>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
                                            </BICFI>
                                        </OriginalCreditorAgentFinancialInstitutionIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAcct/ns0:Id/ns0:IBAN">
                                        <OriginalCreditorAccountIdentification>
                                            <IBAN>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAcct/ns0:Id/ns0:IBAN"/>   
                                            </IBAN>
                                        </OriginalCreditorAccountIdentification>
                                    </xsl:if>
							    </Transaction>
                            </xsl:for-each>
                        </Bulk>                        
                    </xsl:for-each>
                </CancellationRequests>
            </FileBulks>           
       </T24GenericInputFile>   
    </xsl:template>
</xsl:stylesheet>