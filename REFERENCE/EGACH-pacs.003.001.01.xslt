<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.003.001.01" 
    exclude-result-prefixes="xs ns0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*" />
    <xsl:template name="firstCharacter">
        <xsl:param name="value" select="/.." />
        <xsl:param name="default" select="/.." />
        <xsl:choose>
            <xsl:when test="(string-length($value) = 0)">
                <xsl:value-of select="$default" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring($value, 1, 1)" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>	
    <xsl:template match="/">
        <T24GenericInputFile>
            <FileInfo>
                <FileName>.</FileName>
                <QueueName>.</QueueName>
                <ReceivedDate>.</ReceivedDate>
                <BulkIndex>.</BulkIndex>
                <TransactionIndex>.</TransactionIndex>
                <UniqueReference>.</UniqueReference>
                <ProcessingStatus>RECEIVED</ProcessingStatus>
                <ProcessingStatusDescription>.</ProcessingStatusDescription>
                <Content>.</Content>
            </FileInfo>
            <FileHeader>
                    <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId">
                        <BulkSendersReference>
                             <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId" />
                        </BulkSendersReference>
                    </xsl:if>
                     <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm">
                        <BulkCreationDateTime>
                            <xsl:value-of
                                select="concat(concat(concat(concat(concat(concat(substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '18', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '21', '3'))" />
                        </BulkCreationDateTime>
                     </xsl:if>		
                       <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:NbOfTxs">
                                    <BulkNumberOfTransactions>
                                        <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:NbOfTxs" />
                                    </BulkNumberOfTransactions>
                        </xsl:if>
                                    <BulkTotalAmount>
                                        <xsl:variable name="tempValue" select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt" />
                                            <xsl:variable name="format1">
                                                <xsl:call-template name="firstCharacter">
                                                    <xsl:with-param name="value" select="'.'" />
                                                    <xsl:with-param name="default" select="'.'" />
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:variable name="format2">
                                                <xsl:call-template name="firstCharacter">
                                                    <xsl:with-param name="value" select="','" />
                                                    <xsl:with-param name="default" select="','" />
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:variable name="formatValue">
                                                <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                            </xsl:variable>
                                            <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                    </BulkTotalAmount>
									
									

					
					
					<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt">
                                    <BulkSettlementDate>
                                        <xsl:value-of
                                            select="concat(concat(substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '1', '4'), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '6', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '9', '2'))" />
                                    </BulkSettlementDate>
                    </xsl:if>		
					
					
					
					<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd">
                                    <SettlementMethod>
                                        <xsl:value-of
                                            select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd" />
											
									</SettlementMethod>>
                    </xsl:if>
					
					
					
					
					<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:ClrSys/ns0:ClrSysId">
                                    <BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemCode>
                                        <xsl:value-of
                                            select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:ClrSys/ns0:ClrSysId" />
											
									</BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemCode>
                    </xsl:if>
					
					
					
					 <xsl:choose>
                        <xsl:when test="ns0:Document/ns0:pacs.003.001.01//ns0:GrpHdr/ns0:PmtTpInf/ns0:InstrPrty">
                                <PaymentTypeInformationInstructionPriority>U</PaymentTypeInformationInstructionPriority>
                        </xsl:when>
                        <xsl:otherwise>
                               <PaymentTypeInformationInstructionPriority>N</PaymentTypeInformationInstructionPriority>
                        </xsl:otherwise>
                      </xsl:choose>
					  
					  
					  
					  <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp">
                                    <PaymentTypeInformationCategoryPurpose>
                                        <xsl:value-of
                                            select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp" />
											
									</PaymentTypeInformationCategoryPurpose>
                    </xsl:if>
					
					
					
					<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
                                    <FileHeaderSendingInstitution>
                                        <xsl:value-of
                                            select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC" />
											
									</FileHeaderSendingInstitution>
                    </xsl:if>
					
					
					<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" >
						<FileHeaderReceivingInstitution>
							<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" />
						</FileHeaderReceivingInstitution>
					</xsl:if>	
					
					



				
            </FileHeader>
         <FileBulks>
                <DirectDebits>
				
				 <TotalDirectDebitsBulks>
                        <xsl:value-of select="string(count(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf))" />
                 </TotalDirectDebitsBulks>
				 


				
				
				
				<xsl:for-each select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf">
				<Transaction>
				
				    <xsl:if test="ns0:PmtId/ns0:InstrId">
                                        <PaymentIdentificationInstructionIdentification>
                                            <xsl:value-of select="ns0:PmtId/ns0:InstrId" />
                                        </PaymentIdentificationInstructionIdentification>
                                    </xsl:if>
									
									
									
					<xsl:if test="ns0:PmtId/ns0:EndToEndId">
                                        <PaymentIdentificationEndToEndIdentification>
                                            <xsl:value-of select="ns0:PmtId/ns0:EndToEndId" />
                                        </PaymentIdentificationEndToEndIdentification>
                     </xsl:if>
					 
					 
					 
					 
					 
					 
					 									
                  <xsl:if test="ns0:PmtId/ns0:TxId">
                                        <PaymentIdentificationTransactionIdentification>
                                            <xsl:value-of select="ns0:PmtId/ns0:TxId" />
                                        </PaymentIdentificationTransactionIdentification>
                    </xsl:if>
					
					
					<xsl:if test="ns0:IntrBkSttlmAmt/@Ccy">
	                                        <InterbankSettlementAmountCurrency>
	                                            <xsl:value-of select="ns0:IntrBkSttlmAmt/@Ccy" />
	                                        </InterbankSettlementAmountCurrency>
                   </xsl:if>
				   
				   
				   	<ChargeBearer>
                                            <xsl:choose>
                                                <xsl:when test="ns0:ChrgBr !=''">
                                                    <xsl:choose>
                                                        <xsl:when test="ns0:ChrgBr = 'SLEV'"><xsl:value-of select="'SHA'" /></xsl:when>
                                                        <xsl:when test="ns0:ChrgBr = 'SHAR'"><xsl:value-of select="'SHA'" /></xsl:when>
                                                        <xsl:when test="ns0:ChrgBr = 'DEBT'"><xsl:value-of select="'OUR'" /></xsl:when>
                                                        <xsl:when test="ns0:ChrgBr = 'CRED'"><xsl:value-of select="'BEN'" /></xsl:when>
														<xsl:otherwise><xsl:value-of select="'SHA'" /></xsl:otherwise>                                                        
                                                    </xsl:choose>
                                                </xsl:when>
                                            </xsl:choose>
                    </ChargeBearer>
					
					
														
					<xsl:if test="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId">
	                                        <MandateRelatedInformationMandateIdentification>
	                                            <xsl:value-of select="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId" />
	                                        </MandateRelatedInformationMandateIdentification>

                    </xsl:if>
					
					<xsl:if test="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:DtOfSgntr">
	                                        <MandateRelatedInformationDateOfSignature>
	                                            <xsl:value-of select="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:DtOfSgntr" />
	                                        </MandateRelatedInformationDateOfSignature>
                     </xsl:if>
									
									
					<xsl:if test="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:ElctrncSgntr">
	                                        <RelatedInformationElectronicSignature>
	                                            <xsl:value-of select="ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:ElctrncSgntr" />
	                  </RelatedInformationElectronicSignature>
                                    </xsl:if>
									
									
									
									
									
									
					<xsl:if test="ns0:DrctDbtTx/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:OthrId/ns0:IdTp">
	                                        <CreditorSchemePrivateOtherIdentification>
	                                            <xsl:value-of select="ns0:DrctDbtTx/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:OthrId/ns0:IdTp" />
	                                        </CreditorSchemePrivateOtherIdentification>

                    </xsl:if>
					
					
					<xsl:if test="ns0:Cdtr/ns0:Nm">
                                        <CreditorName>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Nm" />
                                        </CreditorName>
                     </xsl:if>
					 
					 
					 
					 <xsl:for-each select="ns0:Cdtr/ns0:PstlAdr/ns0:AdrLine">
                                        <xsl:if test="string((position() = '1')) != 'false'">
                                            <CreditorPostalAddressAddressLine1>
                                                <xsl:value-of select="string(.)" />
                                            </CreditorPostalAddressAddressLine1>
                                        </xsl:if>
                                        <xsl:if test="string((position() = '2')) != 'false'">
                                            <CreditorPostalAddressAddressLine2>
                                                <xsl:value-of select="string(.)" />
                                            </CreditorPostalAddressAddressLine2>
                                        </xsl:if>
                     </xsl:for-each>
					 
					 
					 
					 
					 <xsl:if test="ns0:Cdtr/ns0:PstlAdr/ns0:Ctry">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:PstlAdr/ns0:Ctry" />
                                        </CreditorIdentification>
                      </xsl:if>
					  
								 
						<xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb" />
                                        </CreditorIdentification>
                          </xsl:if>
					
					
							 <xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb" />
                                        </CreditorIdentification>
                           </xsl:if>
						   
						   
						   	<xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb" />
                                        </CreditorIdentification>
                             </xsl:if>
							 
							 
							 	<xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb" />
                                        </CreditorIdentification>
                                 </xsl:if>
								 
								 
								 <xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:PsptNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:PsptNb" />
                                        </CreditorIdentification>
                                 </xsl:if>
								 
								 
								 								 
								<xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb" />
                                        </CreditorIdentification>
                                 </xsl:if>
								 
								 
								  <xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb" />
                                        </CreditorIdentification>
                                 </xsl:if>
								 
								 
								 
								<xsl:if test="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb">
                                        <CreditorIdentification>
                                            <xsl:value-of select="ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb" />
                                        </CreditorIdentification>
                                 </xsl:if>
								 
								 
								<xsl:if test="ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                        <CreditorAccountIdentification>
                                            <xsl:value-of select="ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id" />
                                        </CreditorAccountIdentification>
                                 </xsl:if>
								 
								 								 
								<xsl:if test="ns0:CdtrAcct/ns0:FinInstnId/ns0:BIC">
                                        <DebtorAgentFinancialInstitutionIdentification>
                                            <xsl:value-of select="ns0:CdtrAcct/ns0:FinInstnId/ns0:BIC" />
                                        </DebtorAgentFinancialInstitutionIdentification>
                                 </xsl:if>
								 
								 <xsl:if test="ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
                                        <InstructingAgentFinancialInstitutionIdentificationBIC>
                                            <xsl:value-of select="ns0:InstgAgt/ns0:FinInstnId/ns0:BIC" />
                                        </InstructingAgentFinancialInstitutionIdentificationBIC>
                                 </xsl:if>
								 
								 
								<xsl:if test="ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
						              <FileHeaderReceivingInstitution>
							               <xsl:value-of select="ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" />
						           </FileHeaderReceivingInstitution>
					           </xsl:if>	
								 
								 							   
							    <xsl:if test="ns0:Dbtr/ns0:Nm">
                                        <DebtorName>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Nm" />
                                        </DebtorName>
                                    </xsl:if>
									
									
									<xsl:for-each select="ns0:Dbtr/ns0:PstlAdr/ns0:AdrLine">
                                        <xsl:if test="string((position() = '1')) != 'false'">
                                            <DebtorPostalAddressAddressLine1>
                                                <xsl:value-of select="string(.)" />
                                            </DebtorPostalAddressAddressLine1>
                                        </xsl:if>
                                        <xsl:if test="string((position() = '2')) != 'false'">
                                            <DebtorPostalAddressAddressLine2>
                                                <xsl:value-of select="string(.)" />
                                            </DebtorPostalAddressAddressLine2>
                                        </xsl:if>
                                  </xsl:for-each>
								  
								  
								  <xsl:if test="ns0:Dbtr/ns0:PstlAdr/ns0:Ctry">
                                        <DebtorPostalAddressCountry>
                                            <xsl:value-of select="ns0:Dbtr/ns0:PstlAdr/ns0:Ctry" />
                                        </DebtorPostalAddressCountry>
                                </xsl:if>
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
																
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:PsptNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:PsptNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								
								
								<xsl:if test="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb">
                                        <DebtorIdentification>
                                            <xsl:value-of select="ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb" />
                                        </DebtorIdentification>
                                </xsl:if>
								
								
								
								
								 
									
                    
					
					

									
						<xsl:if test="ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                        <DebtorAccountIdentification>
                                            <xsl:value-of select="ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id" />
                                        </DebtorAccountIdentification>
                                </xsl:if>
								
								
						<xsl:if test="ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <DebtorAgentFinancialInstitutionIdentification>
                                            <xsl:value-of select="ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC" />
                                        </DebtorAgentFinancialInstitutionIdentification>
                                 </xsl:if>
								 
								 
								 
								 								
						<xsl:if test="ns0:Purp/ns0:Cd">
                                        <PaymentTypeInformationCategoryPurpose>
                                            <xsl:value-of select="ns0:Purp/ns0:Cd" />
                                        </PaymentTypeInformationCategoryPurpose>
                                 </xsl:if>
								 
								 
								 
							<xsl:if test="ns0:RmtInf/ns0:Ustrd">
                                        <RemittanceInformation>
                                            <xsl:value-of select="ns0:RmtInf/ns0:Ustrd" />
                                        </RemittanceInformation>
                                 </xsl:if>
								 
								 
								 
								 
								 
								 
								  </Transaction>

								</xsl:for-each>	
                               
					

                </DirectDebits>
            </FileBulks>
        </T24GenericInputFile>
    </xsl:template>
</xsl:stylesheet>