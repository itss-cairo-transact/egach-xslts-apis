* @ValidationCode : MjotMjEyOTcwMDgzNzpDcDEyNTI6MTYwMDE2MzIzMjI0ODptYW5pbWVnYWxhaWs6MTowOjA6MTpmYWxzZTpOL0E6REVWXzIwMjAwOC4yMDIwMDczMS0xMTUxOjM2OjIy
* @ValidationInfo : Timestamp         : 15 Sep 2020 15:17:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : MOUNIR
* @ValidationInfo : Nb tests success  : 1
* @ValidationInfo : Nb tests failure  : 0
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : 22/36 (61.1%)
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : DEV_202008.20200731-1151


*-----------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE PPEGACH.ORIGINAL.PAYMENT.ID.API(ioPaymentObject,originalFTNumber,statusCode,errorCode)
*-----------------------------------------------------------------------------
    $USING PP.MessageMappingService
    $USING PP.PaymentWorkflowDASService
    $USING EB.DataAccess
    $USING PP.InwardMappingFramework
	$USING PPAUBP.ClearingFramework
    GOSUB initialise
    GOSUB process
    
RETURN

*-----------------------------------------------------------------------------

*** <region name= initialise>
initialise:
*** <desc> </desc>
    originalFTNumber = ''
    statusCode = ''
    errorCode = ''
    tempOriginalFTnumber = ''
    
    tempOriginalFTnumber= ioPaymentObject<PP.MessageMappingService.PaymentObject.transaction,1,PP.MessageMappingService.Transaction.transactionReferenceIncoming>
    
RETURN
*** </region>


*-----------------------------------------------------------------------------

*** <region name= process>
process:
*** <desc> </desc>
 
    
    FN.POR.TRANSACTION.CONCAT = 'F.POR.TRANSACTION.CONCAT'
    F.POR.TRANSACTION.CONCAT = ''
    R.TRANSACTION.CONCAT = ''
    ERR.CONCAT = ''
    iOriginatingSource=ioPaymentObject<PP.MessageMappingService.PaymentObject.transaction,1,PP.MessageMappingService.Transaction.originatingSource>
    IDVAL = tempOriginalFTnumber:'-':iOriginatingSource
    PP.InwardMappingFramework.getPORTransactionConcat(IDVAL, R.TRANSACTION.CONCAT, ERR.CONCAT)
    
    IF R.TRANSACTION.CONCAT EQ '' THEN
        ioPaymentObject<PP.MessageMappingService.PaymentObject.transaction,1,PP.MessageMappingService.Transaction.transactionCurrencyCode> = 'EGP'
        ioPaymentObject<PP.MessageMappingService.PaymentObject.creditParty,1,PP.MessageMappingService.PartyCredit.creditPartyRole>='BENFCY'
        ioPaymentObject<PP.MessageMappingService.PaymentObject.creditParty,1,PP.MessageMappingService.PartyCredit.creditPartyRoleIndicator>='R'
    END ELSE
        originalFTNumber = R.TRANSACTION.CONCAT<1>
        GOSUB getPaymentRecordForOrigFT
        IF oPaymentRecord THEN
            statusCode = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.statusCode>
        END
    END
    
                    
RETURN
*** </region>
 
getPaymentRecordForOrigFT:
***************************
    iPaymentID =''
    oPaymentRecord = ''
    oAdditionalPaymentRecord = ''
    oReadErr = ''

    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber> = originalFTNumber
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID> = originalFTNumber[1,3]

    PP.PaymentWorkflowDASService.getPaymentRecord(iPaymentID, oPaymentRecord, oAdditionalPaymentRecord, oReadErr) ;* To read POR.TRANSACTION

RETURN

*-----------------------------------------------------------------------------
END
