* @ValidationCode : MjotNzI0NDMyNDk2OkNwMTI1MjoxNjM4MTEyNTY3NzY2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Nov 2021 17:16:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*--------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.validateFieldsDD(iTransDets, iPrtyDbtDets, iCreditPartyDets, iOriginatingSource, iDebitAuthDets, iInformationDets, oValidChannelFlag, oValidateResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iTransDets - TransDets (Single), IN
* iPrtyDbtDets - PartyDebitDets (List), IN
* iCreditPartyDets - CreditPartyDets (List), IN
* iOriginatingSource - String (Single), IN
* iDebitAuthDets - DebitAuthDets (List), IN
* iInformationDets - InformationDets (List), IN
* oValidChannelFlag - ValidChannelFlag (Single), OUT*
* oValidateResponse - PaymentResponse (Single), OUT
*-----------------------------------------------------------------------------
* $INSERT I_EQUATE - Not Used anymore;
* $INSERT I_STEP2Service_TransDets - Not Used anymore;
* $INSERT I_STEP2Service_PartyDebitDets - Not Used anymore;
* $INSERT I_STEP2Service_CreditPartyDets - Not Used anymore;
* $INSERT I_STEP2Service_ValidChannelFlag - Not Used anymore;
* $INSERT I_STEP2Service_DebitAuthDets - Not Used anymore;
* $INSERT I_STEP2Service_InformationDets - Not Used anymore;
* $INSERT I_STEP2Service_ResponseMessage - Not Used anymore;
* $INSERT I_STEP2Service_PaymentResponse - Not Used anymore;
*
    $INSERT I_CountryIBANStructureService_PotentialIBAN
* $INSERT I_CountryIBANStructureService_IBANDetail - Not Used anymore;
    $USING PP.PaymentFrameworkService
    $USING PP.BankCodeService
    $USING PP.CountryIBANStructureService
    $USING PP.STEP2Service
    $USING PP.DebitPartyDeterminationService
    
*------------------------------------------------------------------------------
*
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDD","iTransDets : <":iTransDets:">","")
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDirectDebit","iPrtyDbtDets : <":iPrtyDbtDets:">","")
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDirectDebit","iCreditPartyDets : <":iCreditPartyDets:">","")
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDirectDebit","iOriginatingSource : <":iOriginatingSource:">","")
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDirectDebit","iDebitAuthDets : <":iDebitAuthDets:">","")
    CALL TPSLogging("Input parameter","EGACHService.validateFieldsDirectDebit","iInformationDets : <":iInformationDets:">","")
*
    GOSUB initialise
    GOSUB process
*
    CALL TPSLogging("Output Parameters","EGACHService.validateFieldsDirectDebit","oValidChannelFlag : <":oValidChannelFlag:">, oValidateResponse : <":oValidateResponse:">","")
*
RETURN
*------------------------------------------------------------------------------
initialise:
    oValidChannelFlag = '' ; oValidateResponse = '' ; oValidChannelFlag<PP.STEP2Service.ValidChannelFlag.validChannelFlag> = "N" ; validClrReturnCodes = '' ; pos = ''
*
RETURN
*-----------------------------------------------------------------------------
process:
*
    BEGIN CASE
        CASE iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> MATCHES 'RF':@VM:'RD'
            GOSUB returnRefundValidation
        CASE iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RV'                ;* Do reversal validation for ClearingTransactionType RV
            GOSUB reversalValidation
        CASE 1
            GOSUB ddiValidation
    END CASE
*
    oValidChannelFlag<PP.STEP2Service.ValidChannelFlag.validChannelFlag> = "Y"
*
RETURN
*-----------------------------------------------------------------------------
returnRefundValidation:
*
    GOSUB validateFTNumber
    GOSUB validateBulkSendersRef
    GOSUB validateCustomerSpecifiedRef
    GOSUB validateSendersRefIncoming
    GOSUB validateTransactionAmount
    GOSUB validateCurrencyEUR
    GOSUB validateClrReturnCode
    GOSUB validateClrNatureCode
    GOSUB validateCreditorID
    GOSUB validateMandateRef
    GOSUB validateDateOfSignature
    GOSUB validateInformationLine
    GOSUB validateCreditPartyDetails
    GOSUB validateDebitPartyDetails
*
RETURN
*------------------------------------------------------------------------------
reversalValidation:
*
    GOSUB validateFTNumber
    GOSUB validateBulkSendersRef
    GOSUB validateCustomerSpecifiedRef
    GOSUB validateTransactionAmount
    GOSUB validateCurrencyEUR
    GOSUB validateClrReturnCode
    GOSUB validateClrNatureCode
    GOSUB validateMandateRef
    GOSUB validateDateOfSignature
    GOSUB validateCreditPartyDetails
    GOSUB validateDebitPartyDetails
*
RETURN
*------------------------------------------------------------------------------
ddiValidation:
*
    GOSUB validateBankOptCode
    GOSUB validateClrNatureCode
    GOSUB validateTransactionAmount
    GOSUB validateCurrencyEUR
    GOSUB validateDetailsOfCharges
    GOSUB validateReqCollectionDate
    GOSUB validateMandateRef
    GOSUB validateDateOfSignature
    GOSUB validateCreditorID
    GOSUB validateCreditPartyDetailsDDI
    GOSUB validateDebitPartyDetails
    IF iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'O' OR iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'R' THEN
        GOSUB validateAddress
    END
*
RETURN
*------------------------------------------------------------------------------
validateFTNumber:
    IF iTransDets<PP.STEP2Service.TransDets.ftNumber> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "FTNumber NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateBulkSendersRef:
    IF iTransDets<PP.STEP2Service.TransDets.bulkSendersReference> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "BulkSendersReference NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateSendersRefIncoming:
    IF iTransDets<PP.STEP2Service.TransDets.sendersReferenceIncoming> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "SendersReferenceIncoming NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateCustomerSpecifiedRef:
    IF iTransDets<PP.STEP2Service.TransDets.customerSpecifiedReference> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CustomerSpecifiedReference NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateTransactionAmount:
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount NOT FOUND"
        GOSUB finalise
    END

* Transaction Amount must be 0.01 or more and 999999999.99 or les and the fractional part has a maximum of two digits.
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> <= 0 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount OUT OF RANGE Value"
        GOSUB finalise
    END
    IF iTransDets<PP.STEP2Service.TransDets.transactionAmount> >= 1000000000 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount OUT OF RANGE Value"
        GOSUB finalise
    END

    Decimal = FIELD(iTransDets<PP.STEP2Service.TransDets.transactionAmount>, ".", 2)
    IF LEN(Decimal) > 2 THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionAmount TOO MANY DECIMALS Value"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateCurrencyEUR:
    IF iTransDets<PP.STEP2Service.TransDets.transactionCurrencyCode> NE "EUR" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "TransactionCurrencyCode NOT EUR"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateClrReturnCode:
    BEGIN CASE
        CASE iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RF'
            validClrReturnCodes = "AC01":@FM:"AC04":@FM:"AC06":@FM:"AC13":@FM:"AG01":@FM:"AG02":@FM:"AM04":@FM:"AM05":@FM:"MD01":@FM:"MD07":@FM:"MS02":@FM:"MS03":@FM:"RC01":@FM:"RR01":@FM:"RR02":@FM:"RR03":@FM:"RR04":@FM:"SL01":@FM:"BE05":@FM:"FF05"
            LOCATE iTransDets<PP.STEP2Service.TransDets.clearingReturnCode> IN validClrReturnCodes SETTING Pos ELSE
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ClearingReturnCode NOT VALID for RF"
                GOSUB finalise
            END
        CASE iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RD'
            IF iTransDets<PP.STEP2Service.TransDets.clearingReturnCode> NE "MD06" THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ClearingReturnCode NOT VALID for RD"
                GOSUB finalise
            END
        CASE iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RV'
            IF NOT (iTransDets<PP.STEP2Service.TransDets.clearingReturnCode> MATCHES 'AM05':@VM:'MS02':@VM:'MS03') THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ClearingReturnCode NOT VALID for RV"
                GOSUB finalise
            END
    END CASE
*
RETURN
*-----------------------------------------------------------------------------
validateClrNatureCode:
    IF iTransDets<PP.STEP2Service.TransDets.clearingNatureCode> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ClearingNatureCode NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateBankOptCode:
    IF iTransDets<PP.STEP2Service.TransDets.bankOperationCode> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "BankOperationCode NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateDetailsOfCharges:
    IF iTransDets<PP.STEP2Service.TransDets.detailsOfCharges> NE "SHA" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DetailsOfCharges NOT SHA"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateReqCollectionDate:
    IF iTransDets<PP.STEP2Service.TransDets.requestedCollectionDate> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "RequestedCollectionDate NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateCreditorID:
    IF iDebitAuthDets<PP.STEP2Service.DebitAuthDets.creditorID> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditorID NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateMandateRef:
    IF iDebitAuthDets<PP.STEP2Service.DebitAuthDets.mandateReference> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "MandateReference NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateDateOfSignature:
    IF iDebitAuthDets<PP.STEP2Service.DebitAuthDets.signatureDate> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "SignatureDate NOT FOUND"
        GOSUB finalise
    END
RETURN
*-----------------------------------------------------------------------------
validateInformationLine:
* validate the settlement date of the original transaction
    IF iInformationDets<PP.STEP2Service.InformationDets.informationLine,1> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "InformationLine representing the settlementDate NOT FOUND"
        GOSUB finalise
    END
* validate the requested collection date of the original transaction
    IF iInformationDets<PP.STEP2Service.InformationDets.informationLine,2> EQ "" THEN
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "InformationLine representing the requestedCollectionDate NOT FOUND"
        GOSUB finalise
    END
RETURN
*------------------------------------------------------------------------------
validateCreditPartyDetails:
*
* determine the credit parties that should be interrogated based on clearing transaction type
    IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RF' OR iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RD' THEN
        crParty1 = "ORDPTY"
        crParty2 = "ORDINS"
    END
    IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RV' THEN
        crParty1 = "BENFCY"
        crParty2 = "ACWINS"
    END
*
    FMp = "" ; VMp = ""
    FIND crParty1 IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyName,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyName NOT FOUND"
            GOSUB finalise
        END
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyAccountLine NOT FOUND"
            GOSUB finalise
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = dbParty1 : " in creditPartyRole NOT FOUND"
        GOSUB finalise
    END
*
    FMp = ""; VMp = ""
    FIND crParty2 IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyIdentifierCode NOT FOUND"
            GOSUB finalise
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = dbParty2 : " in creditPartyRole NOT FOUND"
        GOSUB finalise
    END
*
RETURN
*-----------------------------------------------------------------------------
validateCreditPartyDetailsDDI:
*

    FMp = "" ; VMp = ""
    FIND "ORDPTY" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
        IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyName,VMp> EQ "" AND iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyFreeLine1,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyName and CreditPartyFreeLine1 NOT FOUND"
            GOSUB finalise
        END
        IF iOriginatingSource NE 'OE' THEN
    
*       check the related IBAN from POR.AccountInfo table
*            IF iTransDets<TransDets.batchIndicator> EQ "" AND iTransDets<TransDets.relatedIBAN> EQ "" THEN
*                oValidateResponse<PaymentResponse.responseMessages,1,ResponseMessage.messageInfo> = "RelatedIBAN NOT FOUND"
*                GOSUB finalise
*            END
*       check the CreditPartyAccountLine to be a valid IBAN
            IF iTransDets<PP.STEP2Service.TransDets.batchIndicator> EQ "C" THEN
                IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp> EQ "" THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyAccountLine NOT FOUND"
                    GOSUB finalise
                END
                iPotentialIBAN = "" ; oIBANDetail = "" ; oDetIBANResponse = ""
                iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
                iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyAccountLine,VMp>,"/","")
                CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
                IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
                    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyAccountLine is invalid IBAN"
                    GOSUB finalise
                END
            END
        END
    END ELSE
  
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ORDPTY in creditPartyRole NOT FOUND"
        GOSUB finalise
    END
*
    FMp = "" ; VMp = ""
    IF iOriginatingSource NE 'OE' AND iTransDets<PP.STEP2Service.TransDets.pmtDirection> EQ 'R' THEN
        FIND "ORDINS" IN iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyRole> SETTING FMp,VMp THEN
            IF iCreditPartyDets<PP.STEP2Service.CreditPartyDets.crPartyIdentifCode,VMp> EQ "" THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "CreditPartyIdentifierCode NOT FOUND"
                GOSUB finalise
            END
        END ELSE
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "ORDINS in creditPartyRole NOT FOUND"
            GOSUB finalise
        END
    END
*
RETURN
*-----------------------------------------------------------------------------
validateDebitPartyDetails:
* determine the debit parties that should be interrogated based on clearing transaction type
    IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> MATCHES 'RF':@VM:'RD':@VM:'DD' THEN
        dbParty1 = "DEBTOR"
        dbParty2 = "DBTAGT"
    END
    IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'RV' THEN
        dbParty1 = "ORDPTY"
        dbParty2 = "ORDINS"
    END
*
    FMp = "" ; VMp = ""
    FIND dbParty1 IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyName,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyName NOT FOUND"
            GOSUB finalise
        END
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyAccountLine NOT FOUND"
            GOSUB finalise
        END
        IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'DD' THEN
            iPotentialIBAN = "" ; oIBANDetail = "" ; oDetIBANResponse = ""
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
            iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = EREPLACE(iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyAccountLine,VMp>,"/","")
            CALL PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)
            IF oDetIBANResponse<PP.STEP2Service.PaymentResponse.returnCode> NE "" THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyAccountLine is invalid IBAN"
                GOSUB finalise
            END
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = dbParty1 : " in debitPartyRole NOT FOUND"
        GOSUB finalise
    END
*
    FMp = ""; VMp = ""
    FIND dbParty2 IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
        IF iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,VMp> EQ "" THEN
            oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "DebitPartyIdentifierCode NOT FOUND"
            GOSUB finalise
        END
    END ELSE
        oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = dbParty2 : " in debitPartyRole NOT FOUND"
        GOSUB finalise
    END
*
RETURN
*------------------------------------------------------------------------------
finalise:
*
    oValidateResponse<PP.STEP2Service.PaymentResponse.returnCode> = 'FAILURE'
    oValidateResponse<PP.STEP2Service.PaymentResponse.serviceName> = 'STEP2Service'
    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageCode> = 'STB10003'
    oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageType> = 'NON_FATAL_ERROR'
*
    CALL TPSLogging("Output Parameter - END","STEP2Service.validateFieldsDirectDebit","oValidChannelFlag : <":oValidChannelFlag:">, oValidateResponse : <":oValidateResponse:">","")
*
    GOSUB exit
*
RETURN
*------------------------------------------------------------------------------
exit:
RETURN TO exit
*------------------------------------------------------------------------------
validateAddress:
*   Below condition is added for DD transactions
*   Here we peform address validation which is similar to FATF validation.
    FMp = ''
    VMp = ''
    iPaymentDirection = ''
    iPaymentDirection = iTransDets<PP.STEP2Service.TransDets.pmtDirection>
    debtorAddress = ''
    GOSUB getCompanyProperties
    IF iTransDets<PP.STEP2Service.TransDets.clearingTransactionType> EQ 'DD' THEN
        FIND "DEBTOR" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,VMp THEN
            debtorAddress = iPrtyDbtDets<PP.DebitPartyDeterminationService.PORPartyDebit.debitPartyAddressLine1,VMp>
        END
        IF debtorAddress EQ '' THEN
            GOSUB FindFATFCreditorBankCountryDD
            IF (CreditorCG EQ '') OR (CreditorCG NE 'EU' AND CreditorCG NE 'EEA') THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "Creditor Bank not in EU/EEA region"
                GOSUB finalise
            END
            GOSUB FindFATFDebtorBankCountryDD
            IF (debtorCG EQ '') OR (debtorCG NE 'EU' AND debtorCG NE 'EEA') THEN
                oValidateResponse<PP.STEP2Service.PaymentResponse.responseMessages,1,PP.STEP2Service.ResponseMessage.messageInfo> = "Debtor Bank not in EU/EEA region"
                GOSUB finalise
            END
        END
    END
    
RETURN
*-------------------------------------------------------------------------------
FindFATFCreditorBankCountryDD:
    BankBIC = ''
    CreditorCG = ''
    BankBIC = iCompanyBIC
    GOSUB determineCountryGroup
    CreditorCG = ReturnedCG
    
RETURN
*------------------------------------------------------------------------------
FindFATFDebtorBankCountryDD:
    BankBIC = ''
    ordDbtAgtPos = ''
    debtorCG = ''
    ibanAcctNo = ''
    FIND "DBTAGT" IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,ordDbtAgtPos THEN
        BankBIC = iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,ordDbtAgtPos>
        IF BankBIC NE '' THEN
            GOSUB determineCountryGroup
            debtorCG = ReturnedCG
        END ELSE
            ordDbtAgtPos =''
        END
    END ELSE
        ordDbtAgtPos = ''
    END
    IF ordDbtAgtPos EQ '' THEN
        FIND 'DEBTOR' IN iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyRole> SETTING FMp,ordDbtAgtPos THEN
            ibanAcctNo = iPrtyDbtDets<PP.STEP2Service.PartyDebitDets.debitPartyIdentifierCode,ordDbtAgtPos>
            GOSUB findBICNCCfromIBAN
            debtorCG = ReturnedCG
        END ELSE
            debtorCG = ''
        END
    END
    
RETURN
*------------------------------------------------------------------------------
getCompanyProperties:
*   Determine Company Properties
    iCompanyBIC = ''
    iCompanyPropKey = ''
    oCompanyProperties = ''
    oGetCompPropsError = ''
    iCompanyPropKey = iTransDets<PP.STEP2Service.TransDets.companyID>
    PP.PaymentFrameworkService.getCompanyProperties(iCompanyPropKey, oCompanyProperties, oGetCompPropsError)
    iCompanyBIC =  oCompanyProperties<PP.PaymentFrameworkService.CompanyProperties.companyBIC>
    
RETURN
*------------------------------------------------------------------------------
determineCountryGroup:
*   This block is to determine the Country Group
*   Initialise ReturnedCG is 'N'
    ReturnedCG = 'N'
    iCountryCode = ''
    oCountryGroupList = ''
    oGetCountryGrpError = ''
*   If BankBIC
    IF BankBIC NE '' THEN
        GOSUB determineCtyFromBIC
    END
*   To find the Country Group
    IF iCountryCode NE '' THEN
        oCountryGroupList = ''
        oGetCountryGrpError = ''
        PP.PaymentFrameworkService.getCountryGroupList(iCountryCode, oCountryGroupList, oGetCountryGrpError)
        IF oGetCountryGrpError EQ '' THEN
            CONVERT @VM TO @FM IN oCountryGroupList
            LOCATE 'EEA' IN oCountryGroupList SETTING EeaPos THEN
                ReturnedCG = oCountryGroupList<EeaPos>
                BankBIC=''
                RETURN
            END
            LOCATE "EU" IN oCountryGroupList SETTING EuPos THEN ;*Check if the returned list of CountryGroup contains EEA/EU in it
                ReturnedCG = oCountryGroupList<EuPos>
                BankBIC=''
                RETURN
            END ELSE
                LOC.POS = ''
            END
        END
    END
    BankBIC=''
    
RETURN
*------------------------------------------------------------------------------
determineCtyFromBIC:
*   This block is to determine the countryCode based on the provided BIC
    iCountryCode = ''
    iCountryCode = BankBIC[5,2]
    
RETURN
*------------------------------------------------------------------------------
findBICNCCfromIBAN:
*   Determine IBAN details from PPT.COUNTRYIBANTABLE
    oIBANDetail = ''
    oDetIBANResponse = ''
    iPotentialIBAN =''
    ReturnedCG=''
    actibanAcctNo=''
    lenOfAcctNo=''
    acctLinePosValue=''

    lenOfAcctNo = LEN(ibanAcctNo)
    acctLinePosValue = ibanAcctNo[1,1]
    IF  acctLinePosValue EQ '/' THEN
        actibanAcctNo =  ibanAcctNo[2,lenOfAcctNo-1]
    END ELSE
        actibanAcctNo =  ibanAcctNo
    END

    iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.companyID> = iTransDets<PP.STEP2Service.TransDets.companyID>
    iPotentialIBAN<PP.CountryIBANStructureService.PotentialIBAN.ibanAccountNumber> = actibanAcctNo
    PP.CountryIBANStructureService.determineIBAN(iPotentialIBAN, oIBANDetail, oDetIBANResponse)

*   If the account number is Not an IBAN, return to the calling Method
    IF oDetIBANResponse NE '' THEN
        ReturnedCG = 'E'
        RETURN
    END

    iIBANContext=''
    oBICNCCDetails=''
    oBICNCCError=''
    iIBANContext<PP.BankCodeService.IBANContext.companyID> =  iTransDets<PP.STEP2Service.TransDets.companyID>
    iIBANContext<PP.BankCodeService.IBANContext.ibanCountryCode> = oIBANDetail<PP.CountryIBANStructureService.IBANDetail.ibanCountryCode>
    iIBANContext<PP.BankCodeService.IBANContext.ibanNationalID> =  oIBANDetail<PP.CountryIBANStructureService.IBANDetail.ibanNationalID>

    PP.BankCodeService.determineBICNCCFromIBAN(iIBANContext,oBICNCCDetails,oBICNCCError)

    IF oBICNCCError NE '' THEN
        ReturnedCG = 'E'
    END ELSE
        BankBIC = oBICNCCDetails<PP.BankCodeService.BICNCCDetail.bicCode>
        GOSUB determineCountryGroup
    END
    
RETURN
*------------------------------------------------------------------------------
END

