<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:vmf="http://www.altova.com/MapForce/UDF/vmf"
                xmlns:ns0="http://www.temenos.com/T24/event/Common/EventCommon"
                xmlns:ns1="http://www.temenos.com/T24/OutwardInterfaceService/PaymentHeader"
                xmlns:ns2="http://www.temenos.com/T24/OutwardInterfaceService/PorTransactionRTGS"
                xmlns:ns3="http://www.temenos.com/T24/OutwardInterfaceService/PorPartyCredit"
                xmlns:ns4="http://www.temenos.com/T24/OutwardInterfaceService/PorPartyDebit"
                xmlns:ns5="http://www.temenos.com/T24/OutwardInterfaceService/PorInformation"
                xmlns:ns6="http://www.temenos.com/T24/OutwardInterfaceService/PorAdditionalInf"
                xmlns:ns7="http://www.temenos.com/T24/OutwardInterfaceService/PorAccountInfo"
                xmlns:ns8="http://www.temenos.com/T24/OutwardInterfaceService/PorRemittanceInfoPart1"
                xmlns:ns9="http://www.temenos.com/T24/OutwardInterfaceService/PorRemittanceInfoPart2"
                xmlns:ns10="http://www.temenos.com/T24/OutwardInterfaceService/PpCanReq"
                xmlns:ns11="http://www.temenos.com/T24/OutwardInterfaceService/PorCoverInfo"
                xmlns:ns12="http://www.temenos.com/T24/OutwardInterfaceService/PaymentFlowDets"
                xmlns:ns13="http://www.temenos.com/T24/event/OutwardMappingIF/doOutwardMappingFlow"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0"
                exclude-result-prefixes="vmf ns0 ns1 ns2 ns3 ns4 ns5 ns6 ns7 ns8 ns9 ns10 ns11 ns12 ns13 xs">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="/">
        <xsl:variable name="var1_initial" select="."/>
        <Document xmlns="urn:iso:std:iso:20022:tech:xsd:pacs.002.001.02">
            <xsl:attribute name="xsi:schemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">urn:iso:std:iso:20022:tech:xsd:pacs.002.001.02 pacs.002.001.02.xsd</xsl:attribute>
            <xsl:variable name="localInstrmCd">
                <xsl:for-each
                    select="ns13:doOutwardMappingFlow/ns13:iporinformation[ns5:informationCode = 'INSBNK' and ns5:instructionCode = 'LCLINSCD' and ns5:informationLine]">
                    <xsl:value-of select="ns5:informationLine" />
                </xsl:for-each>
            </xsl:variable>
            <pacs.002.001.02>
                <GrpHdr>
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:ipaymentdetailsa/ns1:bulkReference">
                        <MsgId>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipaymentdetailsa/ns1:bulkReference"/>
                        </MsgId>
                    </xsl:if>
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime">
                        <CreDtTm>
                            <xsl:value-of select="concat(substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '1', '4'), '-', substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '5', '2'), '-', substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '7', '2'), 'T', substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '9', '2'), ':', substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '11', '2'), ':', substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '13', '2'), substring(string(ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:createdDateTime), '18', '6'))"/>
                        </CreDtTm>
                    </xsl:if>
                    <InstgAgt>
                        <FinInstnId>
                            <xsl:if test="ns13:doOutwardMappingFlow/ns13:ipptcompanyptoperties/ns2:companyBIC">
                                <BIC>
                                    <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipptcompanyptoperties/ns2:companyBIC"/>
                                </BIC>
                                
                            </xsl:if>
                        </FinInstnId>
                        <!--                        <BrnchId>
                            <Id>
                            </Id>
                            <Nm>
                            </Nm>
                        </BrnchId>-->
                    </InstgAgt>
                    <InstdAgt>
                        <FinInstnId>
						
                            <xsl:if test="ns13:doOutwardMappingFlow/ns13:ipartydebit[ns4:dbPtyRole = 'ORDINS' and ns4:dbPtyRoleIndicator = 'R' and ns4:dbPtyIdentifierCode]">
                                <BIC>
                                    <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipartydebit/ns4:dbPtyIdentifierCode"/>             	
                                </BIC>
                                
                            </xsl:if>
                        </FinInstnId>
                        <!--                        <BrnchId>
                            <Id>
                            </Id>
                            <Nm>
                            </Nm>
                        </BrnchId>-->
				
                    </InstdAgt>
				
				
                </GrpHdr>

                <OrgnlGrpInfAndSts>
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:ipaymentdetailsa/ns1:bulkReference">
                        <OrgnlMsgId>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipaymentdetailsa/ns1:bulkReference"/>   
                        </OrgnlMsgId>
                    </xsl:if>
                    <OrgnlMsgNmId>pacs.008.001.01</OrgnlMsgNmId>
                    <!--                    <OrgnlCreDtTm></OrgnlCreDtTm>
                    <OrgnlNbOfTxs></OrgnlNbOfTxs>-->
                    <!--<xsl:choose>
               <xsl:when test="(ORIGINAL AMOUNT=REJECTED AMOUNT)">
               <GrpSts>RJCT</GrpSts>
               </xsl:when>
               <xsl:otherwise>
                <GrpSts>PART</GrpSts>
               </xsl:otherwise>
                    </xsl:choose>-->
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:ipaymentflowdets/ns12:instConfStatus">
                        <GrpSts>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipaymentflowdets/ns12:instConfStatus"/>   
                        </GrpSts>
                    </xsl:if>
<!--                    <StsRsnInf>
                        <StsRsn>
                            <Prtry>
                            </Prtry>
                        </StsRsn>
                        <AddtlStsRsnInf>
                        </AddtlStsRsnInf>					
                    </StsRsnInf>-->
				
                </OrgnlGrpInfAndSts>
                
                <TxInfAndSts>
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:fTNumber">
                        <StsId>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:fTNumber"/>
                        </StsId>
                    </xsl:if>
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:transactionrefrenceincoming">
                        <OrgnlInstrId>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:transactionrefrenceincoming"/>     
                        </OrgnlInstrId>
                    </xsl:if>
            
                    <OrgnlEndToEndId>
                        <xsl:choose>
                            <xsl:when test="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:customerSpecifiedReference">
                                <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:customerSpecifiedReference"/>   
                            </xsl:when>
                            <xsl:otherwise>NOTPROVIDED</xsl:otherwise>
                        </xsl:choose>
                    </OrgnlEndToEndId>
	                  
                    <xsl:if test="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:sendersReferenceIncoming">
                        <OrgnlTxId>
                            <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:iportransaction/ns2:sendersReferenceIncoming"/>   
                        </OrgnlTxId>
                    </xsl:if>
					   
                    <TxSts>
                        <xsl:choose>
                            <xsl:when test="ns13:doOutwardMappingFlow/ns13:ipaymentflowdets/ns12:instConfStatus = 'RJCT'">
                                <xsl:value-of select="ns13:doOutwardMappingFlow/ns13:ipaymentflowdets/ns12:instConfStatus"/>       
                            </xsl:when>
                            <xsl:otherwise>RJCT</xsl:otherwise>
                        </xsl:choose>
                    </TxSts>
					
                   <StsRsnInf>
                        <StsRsn>
                            <xsl:if test="ns13:doOutwardInstMapping/ns13:iportransaction/ns2:clgReturnCode">
                                <Prtry>
                                    <xsl:value-of select="ns13:doOutwardInstMapping/ns13:iportransaction/ns2:clgReturnCode"/>
                                </Prtry>
							   <AddtlStsRsnInf>
                                     <xsl:value-of select="s13:doOutwardInstMapping/ns13:iportransaction/ns2:reasonDescription"/>
                               </AddtlStsRsnInf>
                            </xsl:if>					
                        </StsRsn>
                     </StsRsnInf>
<!--                    <InstgAgt>
                        <FinInstnId>
                            <BIC>
                            </BIC>
                        </FinInstnId>
                        <BrnchId>
                            <Id>
                                <Nm>
                                </Nm>
                            </Id>
                        </BrnchId>
                    </InstgAgt>
				
                    <InstdAgt>
                        <FinInstnId>
                            <BIC>
                            </BIC>
                        </FinInstnId>
                        <BrnchId>
                            <Id>
                            </Id>
                            <Nm>
                            </Nm>
                        </BrnchId>
                    </InstdAgt>-->

                </TxInfAndSts>
				
				
				
				
            </pacs.002.001.02>
        </Document>
    </xsl:template>
</xsl:stylesheet>