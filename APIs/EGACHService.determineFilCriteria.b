* @ValidationCode : MjotMTU5NzIwODI2NjpDcDEyNTI6MTYzNTIzOTY4Njk0Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 26 Oct 2021 11:14:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.determineFilCriteria(iBulkPrint, oMessageFormat, oFilingCriteriaResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iBulkPrint - String, IN
* oMessageFormat - String, OUT
* oFilingCriteriaResponse - PaymentResponse, OUT
*------------------------------------------------------------------------------
	$USING PP.STEP2Service
	$INSERT I_STEP2Service_PaymentResponse

*------------------------------------------------------------------------------
	GOSUB setInputLog
    GOSUB initialise
    GOSUB process
	GOSUB setOutputLog

RETURN

*------------------------------------------------------------------------------
process:
*
	clearingNatureCode = FIELD(iBulkPrint, '*', 2)
	
	IF clearingNatureCode EQ '' THEN
		clearingNatureCode = 'SCT'
	END
	
	IF clearingNatureCode EQ 'SCT' THEN
		oMessageFormat = 'ICF'
	END ELSE
		IF (clearingNatureCode EQ 'SDDCOR') OR (clearingNatureCode EQ 'SDDCOR1') OR (clearingNatureCode EQ 'SDDB2B') THEN
			oMessageFormat = 'IDF'
		END
	END
*
RETURN
    
*------------------------------------------------------------------------------
initialise:
*
    clearingNatureCode = ''
	oMessageFormat = ''
	oFilingCriteriaResponse = ''
*
RETURN

*------------------------------------------------------------------------------
setInputLog:
* Logging to see input
    CALL TPSLogging("Start", "STEP2Service.determineFilingCriteria", "", "")
	CALL TPSLogging("Version", "STEP2Service.determineFilingCriteria", "Task - 1610097, Date - 21/03/16", "")
    CALL TPSLogging("Input Parameter", "STEP2Service.determineFilingCriteria", "iBulkPrint : <":iBulkPrint:">", "")

*
RETURN

*------------------------------------------------------------------------------
setOutputLog:
* Logging to see output
	CALL TPSLogging("Output Parameter", "STEP2Service.determineFilingCriteria", "oMessageFormat : <":oMessageFormat:">", "")
	CALL TPSLogging("Output Parameter", "STEP2Service.determineFilingCriteria", "oFilingCriteriaResponse : <":oFilingCriteriaResponse:">", "")
    CALL TPSLogging("End", "STEP2Service.determineFilingCriteria", "", "")
    
*
RETURN

*------------------------------------------------------------------------------

END
