<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs ns0" version="1.0" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.004.001.01" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="yes" method="xml"/>
	<xsl:strip-space elements="*"/>
	<xsl:template name="firstCharacter">
		<xsl:param name="value" select="/.."/>
		<xsl:param name="default" select="/.."/>
		<xsl:choose>
			<xsl:when test="(string-length($value) = 0)">
				<xsl:value-of select="$default"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($value, 1, 1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="/">
		<T24GenericInputFile>
			<xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">C:/Users/mgodeanu/Desktop/StandardInputGenericXML.xsd</xsl:attribute>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>.</ReceivedDate>
				<BulkIndex>.</BulkIndex>
				<TransactionIndex>.</TransactionIndex>
				<UniqueReference>.</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>.</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd">
					<FileHeaderSendingInstitution>
						<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd"/>
					</FileHeaderSendingInstitution>
				</xsl:if>
			</FileHeader>
			<FileBulks>
				<DirectCreditReturn>
					<TotalDirectCreditReturnsBulks>1</TotalDirectCreditReturnsBulks>
					<Bulk>
						<BulkHeader>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId">
								<BulkSendersReference>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId"/>
								</BulkSendersReference>
							</xsl:if>
							<BulkSchemeIndicator>D</BulkSchemeIndicator>
							<ContraIndicator>NA</ContraIndicator>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy">
								<BulkAmountCurrency>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy"/>
								</BulkAmountCurrency>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<BulkSettlementDate>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm"/>
								</BulkSettlementDate>
							</xsl:if>
							<BulkFormat>pacs.004DD</BulkFormat>
							<BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemMemberIdentificationMemberIdentification>19975</BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemMemberIdentificationMemberIdentification>
							<xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs">
								<BulkNumberOfTransactions>
									<xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
								</BulkNumberOfTransactions>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs">
								<BulkCalculatedNumberOfTransactions>
									<xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
								</BulkCalculatedNumberOfTransactions>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt">
								<BulkTotalAmount>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt"/>
								</BulkTotalAmount>
							</xsl:if>
							<BulkCalculatedTotalAmount>
								<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt"/>
							</BulkCalculatedTotalAmount>
						</BulkHeader>
						<Transaction>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy">
								<InterbankSettlementAmountCurrency>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy"/>
								</InterbankSettlementAmountCurrency>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry">
								<ReturnReason>
									<Code>
										<xsl:value-of select="substring(ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry,3,4)"/>
									</Code>
								</ReturnReason>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:OrgnlTxId">
								<OriginalTransactionIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:OrgnlTxId"/>
								</OriginalTransactionIdentification>
							</xsl:if>
							<OriginalCreditorAccountIdentification>
								<OtherIdentification>105094930</OtherIdentification>
							</OriginalCreditorAccountIdentification>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt">
								<ReturnedInterbankSettlementAmount>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt"/>
								</ReturnedInterbankSettlementAmount>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy">
								<ReturnedInterbankSettlementAmountCurrency>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:TxInf/ns0:RtrdIntrBkSttlmAmt/@Ccy"/>
								</ReturnedInterbankSettlementAmountCurrency>
							</xsl:if>
							<OriginalDebtorAccountIdentification>
								<OtherIdentification>11231</OtherIdentification>
							</OriginalDebtorAccountIdentification>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<OriginalInterbankSettlementDate>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm"/>
								</OriginalInterbankSettlementDate>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<InterbankSettlementDate>
									<xsl:value-of select="ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm"/>
								</InterbankSettlementDate>
							</xsl:if>
							<TPTransactionNatureCode>128</TPTransactionNatureCode>
						</Transaction>
					</Bulk>
				</DirectCreditReturn>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>