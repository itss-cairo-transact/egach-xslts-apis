<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="xs ns0" version="1.0" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.004.001.01" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"></xsl:output>
    <xsl:strip-space elements="*"></xsl:strip-space>
    <xsl:template name="firstCharacter">
        <xsl:param name="value" select="/.."></xsl:param>
        <xsl:param name="default" select="/.."></xsl:param>
        <xsl:choose>
            <xsl:when test="(string-length($value) = 0)">
                <xsl:value-of select="$default"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring($value, 1, 1)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="/">
        <T24GenericInputFile>
            <xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">C:/Users/mgodeanu/Desktop/StandardInputGenericXML.xsd</xsl:attribute>
            <FileInfo>
                <FileName>.</FileName>
                <QueueName>.</QueueName>
                <ReceivedDate>.</ReceivedDate>
                <BulkIndex>.</BulkIndex>
                <TransactionIndex>.</TransactionIndex>
                <UniqueReference>.</UniqueReference>
                <ProcessingStatus>RECEIVED</ProcessingStatus>
                <ProcessingStatusDescription>.</ProcessingStatusDescription>
                <Content>.</Content>
            </FileInfo>
            <FileHeader>
                <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId">
                    <BulkSendersReference>
                        <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId"/>
                    </BulkSendersReference>
                </xsl:if>
                <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm">
                    <BulkCreationDateTime>
                        <!--<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '18', '2')), '000')"/>-->
                        <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm"/>    
                    </BulkCreationDateTime>
                </xsl:if>
                <FileMessageFormat>pacs.004.001.01</FileMessageFormat>
                <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId">
                    <FileHeaderFileReference>
                        <xsl:value-of select="concat(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId,'-RTGS')"/>
                    </FileHeaderFileReference>
                </xsl:if>
                <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs">
                    <BulkNumberOfTransactions>
                        <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>     
                    </BulkNumberOfTransactions>
                </xsl:if>
            </FileHeader>
            <xsl:variable name = "OrgMsgNm" select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId"/>
            <FileBulks>
                <Returns>
                    <TotalNumberOfReturnsBulks>1</TotalNumberOfReturnsBulks>
           <!-- Add BIC tags to Bulk Tag-->       
				  <Bulk>
					 <xsl:variable name="blkOrgnlNm">
                                <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Orgtr/ns0:Nm" />
                            </xsl:variable>
                            <xsl:variable name="blkOrgnlBICorBEI">
                                <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI" />
                            </xsl:variable>
                            <xsl:variable name="blkStsRsnInf">
                                <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Rsn/ns0:Cd" />
                            </xsl:variable>
							
                        <BulkHeader>
                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId">
                                <BulkSendersReference>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:MsgId"/>
                                </BulkSendersReference>
                            </xsl:if>
                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm">
                                <BulkCreationDateTime>
                                    <!--<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm), '18', '2')), '000')"/>-->
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:CreDtTm"/> 
                                </BulkCreationDateTime>
                            </xsl:if>
                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs">
                                <BulkNumberOfTransactions>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
                                </BulkNumberOfTransactions>
                            </xsl:if>

                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:TtlRtrdIntrBkSttlmAmt">
                                <BulkTotalAmount>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:TtlRtrdIntrBkSttlmAmt" />
                                </BulkTotalAmount>
                            </xsl:if>    
                            <!--<xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:TtlRtrdIntrBkSttlmAmt/@Ccy">
                                <BulkAmountCurrency>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:TtlRtrdIntrBkSttlmAmt/@Ccy" />
                                </BulkAmountCurrency>
                            </xsl:if> -->
<BulkAmountCurrency>EGP</BulkAmountCurrency>							
                         
                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt">
                                <BulkSettlementDate>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt" />
                                </BulkSettlementDate>
                            </xsl:if>
                            <xsl:if test="./ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
                                <BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                                    <xsl:value-of select="./ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" />
                                </BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                            </xsl:if>

                         
                            <!--xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:GrpRtr">
                                <GrpRtr>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:GrpRtr"/>
                                </GrpRtr>
                            </xsl:if-->
								
                            <!--xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd:">
                                <SttlmMtd>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd"/>
                                </SttlmMtd>
                            </xsl:if-->
								
								
                            <!--                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
                                <FileHeaderReceivingInstitution>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC"/>
                                </FileHeaderReceivingInstitution>
                            </xsl:if>-->
								
                            <!--xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BrnchId/ns0:Id">
                                <Id>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BrnchId/ns0:Id"/>
                                </Id>
                            </xsl:if-->
								
                            <!--xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BrnchId/ns0:Nm">
                                <Nm>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BrnchId/ns0:Nm"/>
                                </Nm>
                            </xsl:if-->
								
                             <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgId">
                                <OriginalMessageIdentification>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgId"/>
                                </OriginalMessageIdentification>
                            </xsl:if>
                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId">
                                <OriginalMessageNameIdentification>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId"/>
                                </OriginalMessageNameIdentification>
                            </xsl:if>
								
                            <!--xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlCreDtTm"/>
                                <OrgnlCreDtTm>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:OrgnlCreDtTm"/>
                                </OrgnlCreDtTm>
                            </xsl:if-->
								
                            <!--                            <xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry">
                                <ReturnReason>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry"/>
                                </ReturnReason>
                            </xsl:if>-->
								
                            <!--<xsl:if test="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:AddtlStsRsnInf">
                                <PaymentDetails>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.004.001.01/ns0:OrgnlGrpInf/ns0:RtrRsnInf/ns0:RtrRsn/ns0:AddtlStsRsnInf"/>
                                </PaymentDetails>
                            </xsl:if>-->
								
                            <!--<BulkFormat>pacs.004.001.01</BulkFormat>-->
                            <xsl:if test= "$OrgMsgNm = 'pacs.008.001.01' or $OrgMsgNm = 'pacs.008'">
                                <BulkFormat>pacs.004</BulkFormat>
                                <BulkSchemeIndicator>C</BulkSchemeIndicator>
                            </xsl:if>
                            <xsl:if test= "$OrgMsgNm = 'pacs.003.001.01' or $OrgMsgNm = 'pacs.003'">
                                <BulkFormat>pacs.004DD</BulkFormat>
                                <BulkSchemeIndicator>D</BulkSchemeIndicator>
                            </xsl:if>

                        </BulkHeader>
					<!--Add BIC tags to TXN-->		
                        <xsl:for-each select="//ns0:Document/ns0:pacs.004.001.01/ns0:TxInf">
                            <Transaction>
							   <xsl:variable name="txnOrgnlNm">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Orgtr/ns0:Nm" />
                                    </xsl:variable>
                                    <xsl:variable name="txnOrgnlBICorBEI">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI" />
                                    </xsl:variable>
                                    <xsl:variable name="txnStsRsnInf">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Rsn/ns0:Cd" />
                                    </xsl:variable>
                                    <xsl:variable name="txnStsRsnAddInf">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:AddtlInf" />
                                    </xsl:variable>
                                <xsl:if test="./ns0:RtrId">
                                    <ReturnIdentification>
                                        <xsl:value-of select="./ns0:RtrId" />
                                    </ReturnIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlInstrId">
                                    <OriginalInstructionIdentification>
                                        <xsl:value-of select="./ns0:OrgnlInstrId" />
                                    </OriginalInstructionIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlEndToEndId">
                                    <OriginalEndtoEndIdentification>
                                        <xsl:value-of select="./ns0:OrgnlEndToEndId" />
                                    </OriginalEndtoEndIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlInstrId">
                                    <OriginalInstructionIdentification>
                                        <xsl:value-of select="./ns0:OrgnlInstrId" />
                                    </OriginalInstructionIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlEndToEndId">
                                    <OriginalEndtoEndIdentification>
                                        <xsl:value-of select="./ns0:OrgnlEndToEndId" />
                                    </OriginalEndtoEndIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlTxId">
                                    <OriginalTransactionIdentification>
                                        <xsl:value-of select="./ns0:OrgnlTxId" />
                                    </OriginalTransactionIdentification>
                                </xsl:if>
                                <xsl:if test="./ns0:RtrdIntrBkSttlmAmt">
                                    <ReturnedInterbankSettlementAmount>
                                        <xsl:value-of select="./ns0:RtrdIntrBkSttlmAmt" />
                                    </ReturnedInterbankSettlementAmount>
                                </xsl:if>
                                <xsl:if test="./ns0:RtrdIntrBkSttlmAmt/@Ccy">
                                    <ReturnedInterbankSettlementAmountCurrency>
                                        <xsl:value-of select="./ns0:RtrdIntrBkSttlmAmt/@Ccy" />
                                    </ReturnedInterbankSettlementAmountCurrency>
                                </xsl:if>
                                <xsl:if test="./ns0:InstgAgt/ns0:FinInstnId">
                                    <FileHeaderSendingInstitution>
                                        <xsl:value-of select="./ns0:InstgAgt/ns0:FinInstnId" />
                                    </FileHeaderSendingInstitution>
                                </xsl:if>
                                <!--xsl:if test="./ns0:InstgAgt/ns0:BrnchId">
                                        <BrnchId>
                                            <xsl:value-of select="./ns0:InstgAgt/ns0:BrnchId" />
                                        </BrnchId>
                                </xsl:if-->
								
                                <xsl:if test="./ns0:InstdAgt/ns0:FinInstnId">
                                    <FileHeaderReceivingInstitution>
                                        <xsl:value-of select="./ns0:InstdAgt/ns0:FinInstnId" />
                                    </FileHeaderReceivingInstitution>
                                </xsl:if>
								
                                <!--xsl:if test="./ns0:InstdAgt/ns0:BrnchId">
                                    <BrnchId>
                                        <xsl:value-of select="./ns0:InstdAgt/ns0:BrnchId" />
                                    </BrnchId>
                                </xsl:if-->
								
								
                                <xsl:if test="./ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry">
                                    <ReturnReason>
                                        <xsl:value-of select="./ns0:RtrRsnInf/ns0:RtrRsn/ns0:Prtry" />
                                    </ReturnReason>
                                </xsl:if>
								
                                <xsl:if test="./ns0:RtrRsnInf/ns0:AddtlStsRsnInf">
                                    <PaymentDetails>
                                        <xsl:value-of select="./ns0:RtrRsnInf/ns0:AddtlStsRsnInf" />
                                    </PaymentDetails>
                                </xsl:if>
								
								<!-- Charges related fields are mapped -->
                                    <xsl:if test="ns0:ChrgsInf">
                                        <xsl:if test="ns0:ChrgsInf/ns0:Amt">
                                            <ChargesInformationAmount>
                                                <xsl:variable name="tempValue" select="ns0:ChrgsInf/ns0:Amt" />
                                                <xsl:variable name="format1">
                                                    <xsl:call-template name="firstCharacter">
                                                        <xsl:with-param name="value" select="'.'" />
                                                        <xsl:with-param name="default" select="'.'" />
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:variable name="format2">
                                                    <xsl:call-template name="firstCharacter">
                                                        <xsl:with-param name="value" select="','" />
                                                        <xsl:with-param name="default" select="','" />
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:variable name="formatValue">
                                                    <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                                </xsl:variable>
                                                <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                            </ChargesInformationAmount>
                                            <xsl:if test="ns0:ChrgsInf/ns0:Amt/@Ccy">
                                                <ChargesInformationAmountCurrency>
                                                   <xsl:value-of select="ns0:ChrgsInf/ns0:Amt/@Ccy" />
                                                </ChargesInformationAmountCurrency>
                                            </xsl:if>
                                        </xsl:if>
                                        
                                        <ChargesInformationAgentFinancialInstitutionIdentificationBICFI>
                                            <xsl:value-of select="ns0:ChrgsInf/ns0:Pty/ns0:FinInstnId/ns0:BIC" />
                                        </ChargesInformationAgentFinancialInstitutionIdentificationBICFI>
                                    </xsl:if>
                                    
<!-- BIC Added to TXN-->

 <xsl:if test="($txnOrgnlNm != '') or ($blkOrgnlNm != '') or ($txnOrgnlBICorBEI != '') or ($blkOrgnlBICorBEI != '')">
                                        <StatusReasonInformationOriginator>
                                            <xsl:if test="($txnOrgnlNm != '') or ($blkOrgnlNm != '')">
                                                <Name>
                                                    <xsl:choose>
                                                        <xsl:when test="$txnOrgnlNm != ''">
                                                            <xsl:value-of select="$txnOrgnlNm" />
                                                        </xsl:when>
                                                        <xsl:when test="$blkOrgnlNm != ''">
                                                            <xsl:value-of select="$blkOrgnlNm" />
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </Name>
                                            </xsl:if>
                                            <xsl:if test="($txnOrgnlBICorBEI != '') or ($blkOrgnlBICorBEI != '')">
                                                <BICOrBEI>
                                                    <xsl:choose>
                                                        <xsl:when test="$txnOrgnlBICorBEI != ''">
                                                            <xsl:value-of select="$txnOrgnlBICorBEI" />
                                                        </xsl:when>
                                                        <xsl:when test="$blkOrgnlBICorBEI != ''">
                                                            <xsl:value-of select="$blkOrgnlBICorBEI" />
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </BICOrBEI>
                                            </xsl:if>
                                        </StatusReasonInformationOriginator>
                                    </xsl:if>									
                <xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalDebtorAgent>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC" />
                                            </BICFI>
                                        </OriginalDebtorAgent>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalCreditorAgent>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC" />
                                            </BICFI>
                                        </OriginalCreditorAgent>
                                    </xsl:if>
								
								<xsl:if test= "$OrgMsgNm = 'pacs.008.001.01' or $OrgMsgNm = 'pacs.008'">
                                    <TPTransactionNatureCode>CT</TPTransactionNatureCode>
                                </xsl:if>
                                <TPTransactionTransferType>C</TPTransactionTransferType>
                                <ChargeWaiver>D</ChargeWaiver>
                            </Transaction>  
                        </xsl:for-each>
                    </Bulk>
                  
                </Returns>
            </FileBulks>
        </T24GenericInputFile>
    </xsl:template>
</xsl:stylesheet>