* @ValidationCode : MjotNDk4NTczMjc6Q3AxMjUyOjE2Mzg4MjU2NDE3Njg6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 06 Dec 2021 23:20:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0

$PACKAGE PP.EGACHService
SUBROUTINE PP.MX.ACK.NACK.PROC(iFileData, iBulkData, iMessageData, oDasError)
*------------------------------------------------------------------------------
* IN/OUT parameters:
* iFileData     - PP.InwardMappingFramework.FileDataObject, IN
* iBulkData     - PP.InwardMappingFramework.BulkDataObject, IN
* iMessageData  - PP.InwardMappingFramework.MessageDataObject, IN
* oDasError     - PaymentResponse, OUT
*------------------------------------------------------------------------------
    $INSERT I_COMMON
    $INSERT I_F.TSA.STATUS
    $INSERT I_TSA.COMMON
    $USING PP.SwiftOutService
    $USING PP.PaymentWorkflowDASService
    $USING PP.PaymentFrameworkService
    $USING PP.PaymentSTPFlowService
    $USING PP.ReversePostingService
    $USING PP.MessageAcceptanceService
    $USING PP.LocalClearingService
    $USING PP.PaymentWorkflowGUI
    $USING PP.InwardMappingFramework
    
    GOSUB initialise ; *
    GOSUB process ; *

RETURN

*-----------------------------------------------------------------------------

*** <region name= initialise>
initialise:
*** <desc> </desc>

* Initialise variables for PP.PaymentWorkflowDASService.getPaymentRecord routine
* Fetch the OriginalBahMessageID tag value from iMessageData present in admi.007 xml and it will have either FT Number or Bulk Reference Outgoing
    iPaymentID = ''
    oPaymentRecord = ''
    oAdditionalPaymentRecord = ''
    oReadErr = ''
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID> = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID>[1,3]    ;* Company ID
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber> = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID>          ;* FT Number
    
* Initialise variables for PP.SwiftOutService.SwiftOutServicegetBlobEntry routine
    iFindBlobRec = ''
    oBlobRec = ''
    oBlobResponse= ''

* Initialise iFileReference to get the Received File Details
    iFileReference =  iFileData<PP.InwardMappingFramework.FileDataObject.fileReference>
    PP.MessageAcceptanceService.getReceivedFileDetails(iFileReference, ioReceivedFileDetails, oRecdFileDetailsError)
    
* Get the Original XML content from the common variable set in InwardMappingFramework.genericMappingForIncomingMsgs routine
* For RTGS and Instant payments, Original XML will be set to this Common variable. For Non-RTGS, it will be set to the Incoming Message
    origMsg = ''
    origMsg = PP.InwardMappingFramework.getInwardINMsgContent()
    
* Initilaise variables for PP.SwiftOutService.SwiftOutServiceupdatePSMBlob routine
    ackMsgCount = ''
    iupdBlobDets = ''
    oWriteError = ''
    
* Initialise variables for PP.PaymentSTPFlowService.doSTPFlowMain routine
    iSelectedPayment = ''
    ioPaymentRecord = ''
    ioAdditionalPaymentRecord = ''
    oMainFlowEndStatus = ''
    oMainFlowConsolErrors = ''
    oSTPMainFlowRes = ''

* Initialise variables for PP.PaymentFrameworkService.getSource routine
    iSourceID = ''
    oSourceDetails = ''
    oGetSourceError = ''
    iSourceID = iFileData<PP.InwardMappingFramework.FileDataObject.originatingChannel>

    eventDescription = ''

* Initialise variables for PP.LocalClearing.getPPTClearing routine
    iClrRequest = ''
    oClrDetails = ''
    oClrError = ''
RETURN
*** </region>

*-----------------------------------------------------------------------------

*** <region name= process>
process:
*** <desc> </desc>

    GOSUB getOriginalPaymentRecord ; *To get the Original Payment Record

* If the Payment record is not able to fetch, then OriginalBahMessageID tag in admi.007 xml contains FT Number or Bulk Reference Outgoing is not found
* Then update the status as 'UNMATCHED' in Received File Details
    IF oPaymentRecord EQ '' THEN
        ioReceivedFileDetails<PP.MessageAcceptanceService.ReceivedFileDetails.statusCodeReceivedFile> = 'UNMATCHED'
        PP.MessageAcceptanceService.updateReceivedFileDetails(iFileReference, ioReceivedFileDetails, oUpdateError)
        RETURN
    END

    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingTransactionType> EQ 'RT' THEN
        GOSUB handleTechnicalNackForPacs004
    END
* Writing the Original XML content in PRF.BLOB for RTGS and Instant Payments
* '<T24GenericInputFile>' string will be the default tag line for Transformed XML. Neglecting this to insert the Original XML message content in PRF.BLOB
    IF origMsg NE '' THEN
        iMessageContent = ''
        iMessageContent = origMsg
        oPRFBlobInsError = ''
* To write the Original XML message content in PRF.BLOB
        PP.MessageAcceptanceService.insertPRFBlob(iFileReference, iMessageContent, oPRFBlobInsError)
    END

* To get the PSM Blob details with the Company ID and FT Number retrieved from Payment Record
    iFindBlobRec<PP.SwiftOutService.FindBlobRec.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
    iFindBlobRec<PP.SwiftOutService.FindBlobRec.ftNumber> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
* For Cover payments, the message type should be determined as 'pacs.009'. To achieve this, we read the CLEARING table and set the Outgoing Message Type.
* PSM.BLOB for Cover message should be listed in the getBlob Entry for Cover payments. To achieve this, we pass the messageType determined from Clearing.
    IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.coverFlag> EQ 'Y' THEN
        iClrRequest<PP.LocalClearingService.ClrRequest.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
        iClrRequest<PP.LocalClearingService.ClrRequest.clearingID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.outputChannel>
        iClrRequest<PP.LocalClearingService.ClrRequest.clearingCurrency> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.transactionCurrencyCode>
        PP.LocalClearingService.getPPTClearing(iClrRequest, oClrDetails, oClrError)
        
        clearingTransactionTypeArr = oClrDetails<PP.LocalClearingService.ClrDetails.clearingTransactionType>
        outgoingMessageTypeArr = oClrDetails<PP.LocalClearingService.ClrDetails.outgoingMessageType>
        incomingMessageTypeArr = oClrDetails<PP.LocalClearingService.ClrDetails.incomingMessageType>
        
        GOSUB getMessageType ;* Get the Outgoing Message Type for Cover payment
        iFindBlobRec<PP.SwiftOutService.FindBlobRec.sendersReference> = messageType
    END ELSE
*When cover flag is not set, outgoingmessage type can be retreived from POR.TRANSACTION itself
        iFindBlobRec<PP.SwiftOutService.FindBlobRec.sendersReference> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.outgoingMessageType>
    END
    
    PP.SwiftOutService.SwiftOutServicegetBlobEntry(iFindBlobRec, oBlobRec, oBlobResponse)
    
* Get the count of AcknowledgementMessage Received from oBlobRec.acknowledgementMessage value
* If Count is NULL, map the new fields of admi.007 xml in first position. There is no admi.007 values, so map it in the first position
* Else, map the new fields of admi.007 xml in count+1 position. AcknowledgementMessage is already present, so map it in the count+1 position
    iupdBlobDets<PP.SwiftOutService.UpdBlobDets.companyID>                  =   oBlobRec<PP.SwiftOutService.BlobRecID.companyID>
    iupdBlobDets<PP.SwiftOutService.UpdBlobDets.ftNumber>                   =   oBlobRec<PP.SwiftOutService.BlobRecID.ftNumber>
    iupdBlobDets<PP.SwiftOutService.UpdBlobDets.sendersReference>           =   oBlobRec<PP.SwiftOutService.BlobRecID.sendersReference>
    iupdBlobDets<PP.SwiftOutService.UpdBlobDets.MessageID,1>                =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID>
    ackMsgCount = DCOUNT(oBlobRec<PP.SwiftOutService.BlobRecID.acknowledgementMessage>, @VM)
    IF oBlobRec<PP.SwiftOutService.BlobRecID.acknowledgementMessage> = '' THEN
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.acknowledgementCode,1>      =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.acknowledgementMessage,1>   =   origMsg
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackReason,1>            =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackReason>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackDescription,1>       =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackDescription>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackMsgType,1>           =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackMsgType>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNacTimestamp,1>          =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNacTimestamp>
    END ELSE
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.acknowledgementCode,ackMsgCount+1>      =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.acknowledgementMessage,ackMsgCount+1>   =   origMsg
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackReason,ackMsgCount+1>            =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackReason>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackDescription,ackMsgCount+1>       =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackDescription>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNackMsgType,ackMsgCount+1>           =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackMsgType>
        iupdBlobDets<PP.SwiftOutService.UpdBlobDets.AckNacTimestamp,ackMsgCount+1>          =   iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNacTimestamp>
    END

* Need to update the incoming admi.007 xml values in the PSM Blob table
    PP.SwiftOutService.SwiftOutServiceupdatePSMBlob(iupdBlobDets, oWriteError)
    
    statusCode = ''
    statusCode = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.statusCode>

* Positive Acknowledgement (0 - Ack Status) with Payment status as COMPLETE (999) not required to go thru the Ack Nack Processing flow
* Negative Acknowledgement (1 - Ack Status) and Positive Acknowledgement (0 - Ack Status) with Payment status as not COMPLETE (other than 999) are required to go thru the Ack Nack Processing flow
    IF (iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> EQ '1') OR (iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> EQ '0' AND statusCode NE '999') THEN
* Status code is incremented by 2 if it is either '676' or '687'
* If the Negative Acknowledgement received, then set the status code to '689' if the Cover flag is 'Y'. Else set the status code to '678'
        BEGIN CASE
            CASE statusCode EQ '676'
                statusCode = '678'
            CASE statusCode EQ '687'
                statusCode = '689'
            CASE statusCode NE '676' AND statusCode NE '687' AND iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> EQ '1'
                IF oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.coverFlag> EQ 'Y' THEN
                    statusCode = '689'
                END ELSE
                    statusCode = '678'
                END
            CASE 1
                RETURN
        END CASE

* Set the updated Status code in the Payment Record
        oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.statusCode> = statusCode
        iServiceName = 'InwardMappingFramework'
        iPaymentRecord = oPaymentRecord
        iAdditionalPaymentRecord = oAdditionalPaymentRecord
        oWriteErr = ''
        PP.PaymentWorkflowDASService.updatePaymentRecord(iServiceName, iPaymentRecord, iAdditionalPaymentRecord, oWriteErr)
    
* Insert record Status in POR.PAYMENTSTATUSCODE
        iPORStatusCode = ''
        oPORStatusCodeError = ''
        iPORStatusCode<PP.PaymentFrameworkService.PORStatusCode.companyID>          = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
        iPORStatusCode<PP.PaymentFrameworkService.PORStatusCode.ftNumber>           = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
        iPORStatusCode<PP.PaymentFrameworkService.PORStatusCode.statusCode>         = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.statusCode>
        iPORStatusCode<PP.PaymentFrameworkService.PORStatusCode.weightCode>         = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.weightCode>
        iPORStatusCode<PP.PaymentFrameworkService.PORStatusCode.specificWeightCode> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.specificWeightCode>
        PP.PaymentFrameworkService.insertPORStatusCode(iPORStatusCode, oPORStatusCodeError)

* Payment will be moved to '680' exception queue for the acknowledgement received and to continue the further payment processing
        GOSUB updateSelectedPaymentArgument ; *Paragraph to assign Input argument for the parameter iSelectedPayment
        ioPaymentRecord = oPaymentRecord
        ioAdditionalPaymentRecord = oAdditionalPaymentRecord
        PP.PaymentSTPFlowService.doSTPFlowMain(iSelectedPayment, ioPaymentRecord, ioAdditionalPaymentRecord, oMainFlowEndStatus, oMainFlowConsolErrors, oSTPResponse)
    END

* To read PP.SOURCE table to check the ActionOnNACK is Cancel/Blank ('') for the respective Source
* For Negative Acknowledgement, we have to reverse the transaction if the ActionOnNACK is Cancel in PP.SOURCE table
    IF iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> EQ '1' THEN
        PP.PaymentFrameworkService.getSource(iSourceID, oSourceDetails, oGetSourceError)

        IF oSourceDetails<PP.PaymentFrameworkService.SourceDetails.ActionOnNACK> EQ 'Cancel' THEN
            iTransactionContext<PP.ReversePostingService.TransactionContext.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
            iTransactionContext<PP.ReversePostingService.TransactionContext.ftNumber> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
            iTransactionContext<PP.ReversePostingService.TransactionContext.businessDate> = ""
            PP.ReversePostingService.autoReverseTransaction(iTransactionContext, oAutoReverseResponse)
        END
    END

* Updation of Audit Trial '1 - Negative Acknowledgement; 0 - Positive Acknowledgement'
    IF iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> EQ '1' THEN
        eventDescription = "Negative Acknowledgement received from ESMIG layer"
    END ELSE
        eventDescription = "Positive Acknowledgement received from ESMIG layer"
    END

* Update local ref fields to store additional info received from the ACK message.
    GOSUB updateLocalRefFields
    GOSUB updatePORHistoryLog

* Updation of Received File Status as 'MAPPED' to continue with the existing flow
    ioReceivedFileDetails<PP.MessageAcceptanceService.ReceivedFileDetails.statusCodeReceivedFile> = 'MAPPED'
    PP.MessageAcceptanceService.updateReceivedFileDetails(iFileReference, ioReceivedFileDetails, oUpdateError)
    
RETURN
*** </region>

*-----------------------------------------------------------------------------

*** <region name= updateSelectedPaymentArgument>
updateSelectedPaymentArgument:
*** <desc>Paragraph to assign Input argument for the parameter iSelectedPayment </desc>
    p$Id = R.TSA.STATUS<TS.TSS.PORT.ID> ;* Assign port ID
    IF NOT(p$Id) THEN       ;* If not present
        p$Id = @USERNO      ;* Assign the port ID
    END

    iSelectedPayment<PP.PaymentSTPFlowService.SubflowData.selectedPayment> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
    iSelectedPayment<PP.PaymentSTPFlowService.SubflowData.processID> = p$Id
    iSelectedPayment<PP.PaymentSTPFlowService.SubflowData.businessDate> = PP.PaymentFrameworkService.getcurrentBusinessDate() ;* get current businessDate
    iSelectedPayment<PP.PaymentSTPFlowService.SubflowData.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
    iSelectedPayment<PP.PaymentSTPFlowService.SubflowData.tsaService> = 'InwardMappingFramework'
    
RETURN
*** </region>

*-----------------------------------------------------------------------------

*** <region name= getOriginalPaymentRecord>
getOriginalPaymentRecord:
*** <desc>To get the Original Payment Record </desc>

* If the OriginalBahMessageID tag contains FT Number, then we can get the Payment Record details for the particular company ID and respective FT Number
    PP.PaymentWorkflowDASService.getPaymentRecord(iPaymentID, oPaymentRecord, oAdditionalPaymentRecord, oReadErr)

* If the OriginalBahMessageID tag contains Bulk Reference Outgoing, then the oPaymentRecord will be empty as it cannot fetch the Payment Record
* Append the OriginatingSource with the Bulk Reference Outgoing and pass it to the getPORTransactionConcat routine to get the FT Number
    IF oPaymentRecord EQ '' THEN
        iConcatId = ''
        oConcatRecord = ''
        oError = ''

        iConcatId = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID>:'-':iFileData<PP.InwardMappingFramework.FileDataObject.originatingSource>
        PP.InwardMappingFramework.getPORTransactionConcat(iConcatId, oConcatRecord, oError)
    END

* oConcatRecord will hold the FT Number, then we can get the Payment Record details for the particular company ID with respective FT Number
    IF oConcatRecord NE '' THEN
        iPaymentID = ''
        oPaymentRecord = ''
        oAdditionalPaymentRecord = ''
        oReadErr = ''
        
        iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID> = oConcatRecord<1>[1,3]    ;* Company ID
        iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber> = oConcatRecord<1>         ;* FT Number
        PP.PaymentWorkflowDASService.getPaymentRecord(iPaymentID, oPaymentRecord, oAdditionalPaymentRecord, oReadErr)
    END
 
RETURN
*** </region>

*-----------------------------------------------------------------------------
updatePORHistoryLog:
* Insert record into PORHistoryLog table
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventType> = 'INF'
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventDescription> = eventDescription
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.errorCode> = ''
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.ftNumber> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
    PP.PaymentFrameworkService.insertPORHistoryLog(iPORHistoryLog, oPORHistoryLogErr)
*
RETURN
*-----------------------------------------------------------------------------
updateLocalRefFields:
    
    iPaymentFlowDets = '' ;* Input parameter for CT to hold the details from POR.PAYMENTFLOWDETAILS
    iPORPmtFlowDetailsReq = ''
    oPORPmtFlowDetailsList = ''
    oPORPmtFlowDetailsGetError = ''
    iUpdatePaymentFlowDetails = ''
    oPORPmtFlowDetailsUpdError = ''
    
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.companyID> = iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID>
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.ftNumber> =  iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber>

    PP.PaymentFrameworkService.getPORPaymentFlowDetails(iPORPmtFlowDetailsReq, oPORPmtFlowDetailsList, oPORPmtFlowDetailsGetError)  ;* To read POR.PAYMENTFLOWDETAILS table
    
    IF oPORPmtFlowDetailsGetError EQ '' THEN
    
        iUpdatePaymentFlowDetails = oPORPmtFlowDetailsList ;*Assign the output as an Input, required when updating again into the table

        IF iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName> NE '' THEN
            totalLrfFields = DCOUNT(iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName>,@VM) ;* Get the number of local field
            FOR i = 1 TO totalLrfFields
                LOCATE iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName,i> IN iUpdatePaymentFlowDetails<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName,1> SETTING pos THEN
* If the loca ref field is already present, update it's value
                    iUpdatePaymentFlowDetails<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldValue,pos> = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefValue,i>
                END ELSE
                    iUpdatePaymentFlowDetails<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldName,-1> = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName,i> ;* Update the local field name
                    iUpdatePaymentFlowDetails<PP.PaymentFrameworkService.PORPmtFlowDetailsList.localFieldValue,-1> = iMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefValue,i>
                END
            NEXT i
        END
     
        PP.PaymentFrameworkService.updatePORPaymentFlowDetails(iUpdatePaymentFlowDetails, oPORPmtFlowDetailsUpdError) ;* To update POR.PAYMENTFLOWDETAILS
    END
RETURN
**-----------------------------------------------------------------------------
getMessageType:
* Get the Outgoing Message Type for Cover payment
    transTypeEach = ''
    transTypePos = ''
    rPos = 1

    IF clearingTransactionTypeArr NE '' THEN
        LOOP
            REMOVE transTypeEach FROM clearingTransactionTypeArr SETTING transTypePos
        WHILE transTypeEach:transTypePos
            IF transTypeEach EQ 'CT' AND incomingMessageTypeArr<1,rPos> EQ 'COV' THEN
                messageType = outgoingMessageTypeArr<1,rPos>
                BREAK
            END
            rPos = rPos + 1
        REPEAT
    END
*
RETURN
*-----------------------------------------------------------------------------
handleTechnicalNackForPacs004:
* New logic developed as part of target2 recall
* When system receives technical nack for the outgoing pacs.004 which was generated by accepting the incoming camt.056, then we do the below logic
* to make the user to manually intervene with EBQA
* This is to ensure, when a outgoing pacs.004 is generated with recall indicator as 'Y',  then we are allowing the user to manually intervene with the EBQA
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.companyID> = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.companyID>
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.ftNumber>  = oPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.ftNumber>
    oPORPmtFlowDetailsList = ""
    oPORPmtFlowDetailsGetError = ""
    PP.PaymentFrameworkService.getPORPaymentFlowDetails(iPORPmtFlowDetailsReq, oPORPmtFlowDetailsList, oPORPmtFlowDetailsGetError)

    recallIndicator = oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.recallIndicator>
    IF recallIndicator EQ "Y" THEN
        orgnlFTNumber = oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.orgnlOrReturnId>
        PP.PaymentWorkflowGUI.getSupplementaryInfo('POR.PAYMENTFLOWDETAILS', orgnlFTNumber, '', Record, Error)
        ebqaIDVAL = Record<PP.PaymentWorkflowGUI.PorPaymentflowdetails.CancelMsgRef>
        IF ebqaIDVAL THEN
            PaymentDetails = ''
            oErrorRespose = ''
            PaymentDetails<PP.InwardMappingFramework.EbQaMessageType> ="camt.056"
            PaymentDetails<PP.InwardMappingFramework.EbQaProcessIndicator> = "MANUAL"
            PaymentDetails<PP.InwardMappingFramework.EbQaStatus> = "INWORK"
            PaymentDetails<PP.InwardMappingFramework.EbQaErrorReason> = "Return rejected by the clearing"
            PP.InwardMappingFramework.ebqaUpdatePmtDet(PaymentDetails, ebqaIDVAL, oErrorRespose)
        END
    END
RETURN
*-----------------------------------------------------------------------------
