* @ValidationCode : MjotNjczMjIwNjcyOkNwMTI1MjoxNjM4MTEyODI1NDQ0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Nov 2021 17:20:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.determineBulkCriteria(iClearingId, iClearingCurrency, oClearingBulking, oBulkingCriteriaResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iClearingId - String, IN
* iClearingCurrency - String, IN
* oClearingBulking - ClearingBulking (Single), OUT
* oBulkingCriteriaResponse - PaymentResponse (Single), OUT

	$USING PP.STEP2Service
	$INSERT I_STEP2Service_ClearingBulking
	$INSERT I_STEP2Service_PaymentResponse

*------------------------------------------------------------------------------
	GOSUB setInputLog
    GOSUB initialise
    GOSUB process
	GOSUB setOutputLog

RETURN

*------------------------------------------------------------------------------
process:
*
	oClearingBulking<PP.STEP2Service.ClearingBulking.clearingCurrency> = 'Y'
	oClearingBulking<PP.STEP2Service.ClearingBulking.clearingNatureCode> = 'Y'
	oClearingBulking<PP.STEP2Service.ClearingBulking.outgoingMessageType> = 'Y'
	oClearingBulking<PP.STEP2Service.ClearingBulking.clearingTransactionType> = 'Y'
	oClearingBulking<PP.STEP2Service.ClearingBulking.creditValueDate> = 'Y'
	oClearingBulking<PP.STEP2Service.ClearingBulking.debitValueDate> = 'N'
*
RETURN
    
*------------------------------------------------------------------------------
initialise:
*
    oClearingBulking = ''
	oBulkingCriteriaResponse = ''
    
*
RETURN

*------------------------------------------------------------------------------
setInputLog:
* Logging to see input
    CALL TPSLogging("Start", "EGACHService.determineBulkCriteria", "", "")
	CALL TPSLogging("Version", "EGACHService.determineBulkingCriteria", "Task - 1610097, Date - 21/03/16", "")
    CALL TPSLogging("Input Parameter", "EGACHService.determineBulkingCriteria", "iClearingId : <":iClearingId:">", "")
    CALL TPSLogging("Input Parameter", "EGACHService.determineBulkingCriteria", "iClearingCurrency : <":iClearingCurrency:">", "")

*
RETURN

*------------------------------------------------------------------------------
setOutputLog:
* Logging to see output
	CALL TPSLogging("Output Parameter", "EGACHService.determineBulkingCriteria", "oClearingBulking : <":oClearingBulking:">", "")
	CALL TPSLogging("Output Parameter", "EGACHService.determineBulkingCriteria", "oBulkingCriteriaResponse : <":oBulkingCriteriaResponse:">", "")
    CALL TPSLogging("End", "EGACHService.determineBulkingCriteria", "", "")
    
*
RETURN
 
*------------------------------------------------------------------------------

END
