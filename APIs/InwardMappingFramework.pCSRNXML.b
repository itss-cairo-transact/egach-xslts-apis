* @ValidationCode : MjotMzM0MDY4ODYxOkNwMTI1MjoxNjM4ODI0NjU2MDMwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 06 Dec 2021 23:04:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*-----------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE InwardMappingFramework.pCSRNXML(iFileData, iIncomingMessage, oMessageData, oCSRResponse)
*-----------------------------------------------------------------------------
    $USING PP.InwardFramework
    $USING EB.API
    $USING EB.SystemTables
    $USING PP.InwardMappingFramework
*-----------------------------------------------------------------------------
    GOSUB initialise
    GOSUB readMappingTable
    GOSUB assignOutput
RETURN
*-----------------------------------------------------------------------------
initialise:
    delimiter = ''
    fieldList = ''
    fieldPositionList = ''
    mandatoryList = ''
    routineList = ''
    constantList = ''
    totalFields = ''
    iterator = 0
RETURN
*-----------------------------------------------------------------------------
readMappingTable:
    recInEntryParam = ''
    Error = ''
*Using the MappingTableID read the table PP.IN.ENTRY.PARAM
    MappingTableID = iFileData<PP.InwardMappingFramework.FileDataObject.mappingTableID>
*MappingTableID = iFileData<PP.InwardMappingFramework.FileDataObject.queueName>:'-':'S'
    recInEntryParam = PP.InwardFramework.PpInEntryParam.Read(MappingTableID, Error)

* Initialise loop variables and local variables required for loop
    delimiter = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepFieldDelimiter>
    vmDelimiter = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepVMDelimiter>
    smDelimiter = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepSMDelimiter>
    fieldList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepFieldName>
    fieldPositionList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepFieldPosition>
    mandatoryList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepMandatory>
    routineList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepValRoutine>
    constantList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepConstant>
    localRefList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepLocalReference>
    fieldLengthList = recInEntryParam<PP.InwardFramework.PpInEntryParam.PpiepFieldLength>
RETURN
*-----------------------------------------------------------------------------
assignOutput:
    CHANGE @VM TO @FM IN fieldList
    CHANGE @VM TO @FM IN fieldPositionList
    CHANGE @VM TO @FM IN routineList
    CHANGE @VM TO @FM IN mandatoryList
    CHANGE @VM TO @FM IN constantList
    CHANGE @VM TO @FM IN localRefList
    CHANGE @VM TO @FM IN fieldLengthList
    iterator = 1
    totalFields = DCOUNT(fieldList,@FM)
    LOOP
        fieldName = fieldList<iterator>
        fieldPosition = fieldPositionList<iterator>
        constantValue = constantList<iterator>
        localRef = localRefList<iterator>
        fieldLength = fieldLengthList<iterator>
    WHILE iterator LE totalFields AND fieldName NE "" AND (fieldPosition NE "" OR constantValue NE "")
        IF fieldPosition NE '' THEN
            IF fieldLength NE '' THEN
                currentFieldValue = iIncomingMessage[fieldPosition,fieldLength]
            END ELSE
                currentFieldValue = FIELD(iIncomingMessage,delimiter,fieldPosition)
            END
        END ELSE
            currentFieldValue = '' ;* If then field position was not given then NULL the field value
        END
* Check whether the value from the incoming field position was not retrieved
* and the constant value is given then map the same as field value.
        IF (constantValue NE '') AND (currentFieldValue EQ '') THEN
            currentFieldValue = constantValue
        END
        IF routineList<iterator> NE "" THEN
            COMPILED.OR.NOT = ''
            RETURN.INFO = ''
            EB.API.CheckRoutineExist(routineList<iterator>,COMPILED.OR.NOT,RETURN.INFO)
            IF (COMPILED.OR.NOT NE 1 AND RETURN.INFO NE 'Subroutine' ) THEN
* Error handling
                messageInfo = 'Mapping Enrich API missing for field:':fieldName
                messageType = 'FATAL_ERROR'
                GOSUB updateErrResponse       ;* To fatal out from the flow
                GOSUB exitPoint
            END
            iEnrichAPI = ''
            argumentsList = ''
            iEnrichAPI <1> = routineList<iterator>
            argumentsList = currentFieldValue
            EB.SystemTables.CallBasicRoutine(iEnrichAPI, argumentsList, @FM)
            currentFieldValue  = argumentsList
        END

        IF mandatoryList<iterator> EQ "Y" AND (currentFieldValue EQ '' AND constantValue EQ '') THEN
            messageInfo = 'Value missing in mandatory field:':fieldName
            messageType = 'FATAL_ERROR'
            GOSUB updateErrResponse
            GOSUB exitPoint
        END
        IF currentFieldValue NE "" THEN
            IF localRef EQ 'Y' THEN
                oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpLocalRefName,-1> = fieldName
                oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpLocalRefValue,-1> = currentFieldValue
            END ELSE
                GOSUB assignLogic
            END
        END

        iterator = iterator + 1
    REPEAT
RETURN

*************************************************************************************************************
assignLogic:
    BEGIN CASE
        CASE fieldName EQ "StsId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsId> = currentFieldValue
        CASE fieldName EQ "OrgnlInstrId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglInstrId> = currentFieldValue
        CASE fieldName EQ "OrgnlEndToEndId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglEndToEndId> = currentFieldValue
        CASE fieldName EQ "OrgnlTxId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId> = currentFieldValue
        CASE fieldName EQ "TxSts"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxSts> = currentFieldValue
        CASE fieldName EQ "ChrgsInf-Amt"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfAm> = currentFieldValue
        CASE fieldName EQ "ChrgsInf-AmtCcy"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfCcy> = currentFieldValue
        CASE fieldName EQ "ChrgsInf-AgtBICFI"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdBIC> = currentFieldValue
        CASE fieldName EQ "StsRsnInf-Orgtr-Nm"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfOrgnName> = currentFieldValue
        CASE fieldName EQ "StsRsnInf-Orgtr-AnyBIC"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfOrgnIdOrgIdBIC> = currentFieldValue
        CASE fieldName EQ "StsRsnInf-Cd"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnCd> = currentFieldValue
        CASE fieldName EQ "StsRsnInf-Prtry"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnProp> = currentFieldValue
        CASE fieldName EQ "StsRsnInf-AddtlInf"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfAdInf> = currentFieldValue
*        CASE fieldName EQ "AccptncDtTm"
* oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.> = currentFieldValue
        CASE fieldName EQ "OrgnlIntrBkSttlmAmt"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlAm> = currentFieldValue
        CASE fieldName EQ "OrgnlIntrBkSttlmAmtCcy"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlAmCcy> = currentFieldValue
        CASE fieldName EQ "OrgnlIntrBkSttlmDt"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlDt> = currentFieldValue
        CASE fieldName EQ "OrgnlDbtrAgt-BICFI"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgBICFI> = currentFieldValue
        CASE fieldName EQ "OrgnlDbtrAgt-ClrSysIdCd"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemIdCgSysIdCd> = currentFieldValue
        CASE fieldName EQ "OrgnlDbtrAgt-ClrSysIdPrtry"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemIdCgSysIdProp> = currentFieldValue
        CASE fieldName EQ "OrgnlDbtrAgt-ClrSysMmbId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemId> = currentFieldValue
        CASE fieldName EQ "OrgnlCdtrAgt-BICFI"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgBICFI> = currentFieldValue
        CASE fieldName EQ "OrgnlCdtrAgt-ClrSysIdCd"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemIdCgSysIdCd> = currentFieldValue
        CASE fieldName EQ "OrgnlCdtrAgt-ClrSysIdPrtry"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemIdCgSysIdProp> = currentFieldValue
        CASE fieldName EQ "OrgnlCdtrAgt-ClrSysMmbId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemId> = currentFieldValue
        CASE fieldName EQ "FinalAcceptanceDate"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpFinalAcceptanceDate> = currentFieldValue
* Added the new tags as part of admi.007 AckNack Message Type
        CASE fieldName EQ "AckNackInf-OrgBAHMsgId"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID> = currentFieldValue
        CASE fieldName EQ "AckNackInf-Reason"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackReason> = currentFieldValue
        CASE fieldName EQ "AckNackInf-Description"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackDescription> = currentFieldValue
        CASE fieldName EQ "AckNackInf-MsgType"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackMsgType> = currentFieldValue
        CASE fieldName EQ "AckNackInf-Status"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> = currentFieldValue
        CASE fieldName EQ "AckNackInf-TimeStamp"
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNacTimestamp> = currentFieldValue
    END CASE
    
    CHANGE vmDelimiter TO @VM IN oMessageData
    CHANGE smDelimiter TO @SM IN oMessageData
RETURN
*-----------------------------------------------------------------                                                                                                                                      ------------
updateErrResponse:
    oCSRResponse<1,PP.InwardMappingFramework.PaymentResponse.returnCode> = "FAILURE"
    oCSRResponse<1,PP.InwardMappingFramework.PaymentResponse.serviceName> = "IMF/INWARD.MAPPING"
    oCSRResponse<1,PP.InwardMappingFramework.PaymentResponse.responseMessages,PP.InwardMappingFramework.ResponseMessage.messageType> = messageType
    oCSRResponse<1,PP.InwardMappingFramework.PaymentResponse.responseMessages,PP.InwardMappingFramework.ResponseMessage.messageInfo> = messageInfo
RETURN
*-----------------------------------------------------------------------------
exitPoint:
RETURN TO exitPoint
*-----------------------------------------------------------------------------
END

    
