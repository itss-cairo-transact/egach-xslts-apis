<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.002.001.02" exclude-result-prefixes="xs ns0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes" />
    <xsl:strip-space elements="*" />
    <xsl:template name="firstCharacter">
        <xsl:param name="value" select="/.." />
        <xsl:param name="default" select="/.." />
        <xsl:choose>
            <xsl:when test="(string-length($value) = 0)">
                <xsl:value-of select="$default" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring($value, 1, 1)" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="/">
        <T24GenericInputFile>
            <xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">file:///C:/Users/mgodeanu/Desktop/StandardInputGenericXML.xsd</xsl:attribute>
            <FileInfo>
                <FileName>.</FileName>
                <QueueName>.</QueueName>
                <ReceivedDate>.</ReceivedDate>
                <BulkIndex>.</BulkIndex>
                <TransactionIndex>.</TransactionIndex>
                <UniqueReference>.</UniqueReference>
                <ProcessingStatus>RECEIVED</ProcessingStatus>
                <ProcessingStatusDescription>.</ProcessingStatusDescription>
                <Content>.</Content>
            </FileInfo>
            <FileHeader>
                <FileMessageFormat>pacs.002.001.02</FileMessageFormat>
                <xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId">
                    <FileHeaderFileReference>
                        <xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:GrpHdr/ns0:MsgId" />
                    </FileHeaderFileReference>
                </xsl:if>
                <xsl:if test="ns0:Document/ns0:pacs.002.001.02/ns0:TxInfAndSts/ns0:OrgnlTxId">
                    <SenderReferenceIncoming>
                        <xsl:value-of select="ns0:Document/ns0:pacs.002.001.02/ns0:TxInfAndSts/ns0:OrgnlTxId" />
                    </SenderReferenceIncoming>
                </xsl:if>
               <FileTypeIndicator>R</FileTypeIndicator>
            </FileHeader>
            <FileBulks>
                <ClearingStatusReports>
                    <TotalClearingStatusReportsBulks>
                        <xsl:value-of select="string(count(ns0:Document/ns0:pacs.002.001.02))" />
                    </TotalClearingStatusReportsBulks>
                    <xsl:for-each select="ns0:Document/ns0:pacs.002.001.02">
                        <Bulk>
                            <xsl:variable name="blkOrgnlNm">
                                <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Orgtr/ns0:Nm" />
                            </xsl:variable>
                            <xsl:variable name="blkOrgnlBICorBEI">
                                <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI" />
                            </xsl:variable>
                            <xsl:variable name="blkStsRsnInf">
                                <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Rsn/ns0:Cd" />
                            </xsl:variable>
                            <BulkHeader>
                                <BulkSchemeIndicator>C</BulkSchemeIndicator>
                                <BulkFormat>pacs.002</BulkFormat>
                                <xsl:if test="./ns0:GrpHdr/ns0:MsgId">
                                    <BulkSendersReference>
                                        <xsl:value-of select="./ns0:GrpHdr/ns0:MsgId" />
                                    </BulkSendersReference>
                                </xsl:if>
                                <xsl:if test="./ns0:GrpHdr/ns0:CreDtTm">
                                    
                                    <BulkCreationDateTime>                                      
                                        <xsl:value-of
                                            select="concat(concat(concat(concat(concat(concat(substring(string(./ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '18', '2')), '000')" />
                                    </BulkCreationDateTime>
                                    
                                </xsl:if>
                                <xsl:if test="./ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
                                    <BulkInstructingAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC" />
                                    </BulkInstructingAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if>
                                <!--  Added as part of IG2 Spec -->
                                <!-- Instructed agent has to be mapped at bulk header level -->
                                
                                <xsl:if test="./ns0:GrpHdr/ns0:InstdAgt">
                                    <BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" />
                                    </BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgId">
                                    <BulkClearingStatusReportOriginalBulkReference>
                                        <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgId" />
                                    </BulkClearingStatusReportOriginalBulkReference>
                                </xsl:if>
                                <xsl:if test="./ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgNmId">
                                    <BulkClearingStatusReportOriginalBulkFormat>
                                        <xsl:value-of select="substring(string(./ns0:OrgnlGrpInfAndSts/ns0:OrgnlMsgNmId),1,8)" />
                                    </BulkClearingStatusReportOriginalBulkFormat>
                                </xsl:if>
                                <xsl:choose> 
                                    <xsl:when test="ns0:OrgnlGrpInfAndSts/ns0:GrpSts">
                                        <BulkClearingStatusReportGroupStatus>
                                            <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:GrpSts" />
                                        </BulkClearingStatusReportGroupStatus>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:if test="ns0:TxInfAndSts/ns0:TxSts">
                                            <BulkClearingStatusReportGroupStatus>
                                                <xsl:value-of select="ns0:TxInfAndSts/ns0:TxSts" />
                                            </BulkClearingStatusReportGroupStatus>
                                        </xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                                
                                <xsl:if test="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:AddtlInf">
                                    <BulkOriginalClearingStatusReportReasonCodeAddInfo>
                                    <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:AddtlInf"/>
                                    </BulkOriginalClearingStatusReportReasonCodeAddInfo>
                                </xsl:if>
                                
                                <xsl:if test="$blkOrgnlNm != ''">
                                    <BulkClearingStatusReportReasonOriginatorName>
                                        <xsl:value-of select="$blkOrgnlNm" />
                                    </BulkClearingStatusReportReasonOriginatorName>
                                </xsl:if>
                                <xsl:if test="$blkOrgnlBICorBEI != ''">
                                    <BulkClearingStatusReportReasonOriginatorBICOrBEI>
                                        <xsl:value-of select="$blkOrgnlBICorBEI" />
                                    </BulkClearingStatusReportReasonOriginatorBICOrBEI>
                                </xsl:if>
                                <xsl:if test="$blkStsRsnInf != ''">
                                    <BulkClearingStatusReportReasonCode>
                                        <xsl:value-of select="$blkStsRsnInf" />
                                    </BulkClearingStatusReportReasonCode>
                                </xsl:if>
                                
                                
                                <xsl:if test="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Rsn/ns0:Prtry">
                                    <BulkClearingStatusReportReasonProprietary>
                                        <xsl:value-of select="./ns0:OrgnlGrpInfAndSts/ns0:StsRsnInf/ns0:Rsn/ns0:Prtry" />
                                    </BulkClearingStatusReportReasonProprietary>
                                </xsl:if>
                                                            
                                <xsl:if test="(ns0:OrgnlGrpInfAndSts/ns0:GrpSts = 'ACCP') or (ns0:OrgnlGrpInfAndSts/ns0:GrpSts = 'RJCT') ">
                                            <xsl:if test="ns0:OrgnlGrpInfAndSts/ns0:OrgnlNbOfTxs">
                                                <BulkNumberOfTransactions>
                                                    <xsl:value-of select="ns0:OrgnlGrpInfAndSts/ns0:OrgnlNbOfTxs" />
                                                </BulkNumberOfTransactions>
                                            </xsl:if>
                                            <xsl:if test="ns0:OrgnlGrpInfAndSts/ns0:OrgnlCtrlSum">
                                                <BulkTotalAmount>
                                                    <xsl:variable name="tempValue" select="ns0:OrgnlGrpInfAndSts/ns0:OrgnlCtrlSum" />
                                                    <xsl:variable name="format1">
                                                        <xsl:call-template name="firstCharacter">
                                                            <xsl:with-param name="value" select="'.'" />
                                                            <xsl:with-param name="default" select="'.'" />
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:variable name="format2">
                                                        <xsl:call-template name="firstCharacter">
                                                            <xsl:with-param name="value" select="','" />
                                                            <xsl:with-param name="default" select="','" />
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:variable name="formatValue">
                                                        <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                                    </xsl:variable>
                                                    <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                                </BulkTotalAmount>
                                                 </xsl:if>
                                                 <BulkAmountCurrency>EGP</BulkAmountCurrency>                                           
                                        </xsl:if>
										    <BulkAmountCurrency>EGP</BulkAmountCurrency>  
                                        <xsl:if test="(ns0:OrgnlGrpInfAndSts/ns0:GrpSts = 'PART') ">
                                            <xsl:if test="ns0:OrgnlGrpInfAndSts/ns0:NbOfTxsPerSts/ns0:DtldNbOfTxs">
                                                <BulkNumberOfTransactions>
                                                    <xsl:value-of select="ns0:OrgnlGrpInfAndSts/ns0:NbOfTxsPerSts/ns0:DtldNbOfTxs" />
                                                </BulkNumberOfTransactions>
                                            </xsl:if>                                           
                                            <xsl:if test="ns0:TxInfAndSts/ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt">
                                                <BulkTotalAmount>
                                                    <xsl:variable name="tempValue">
                                                        <xsl:value-of select="sum(ns0:TxInfAndSts/ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt)"/>                                            
                                                    </xsl:variable>                                                 
                                                    <xsl:variable name="format1">
                                                        <xsl:call-template name="firstCharacter">
                                                            <xsl:with-param name="value" select="'.'" />
                                                            <xsl:with-param name="default" select="'.'" />
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:variable name="format2">
                                                        <xsl:call-template name="firstCharacter">
                                                            <xsl:with-param name="value" select="','" />
                                                            <xsl:with-param name="default" select="','" />
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:variable name="formatValue">
                                                        <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                                    </xsl:variable>
                                                    <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                                </BulkTotalAmount>
                                                 </xsl:if> 
                                                 <BulkAmountCurrency>EGP</BulkAmountCurrency>                                     
                                    </xsl:if> 
                            </BulkHeader> 
                            <xsl:for-each select="//ns0:Document/ns0:pacs.002.001.02/ns0:TxInfAndSts">
                                <Transaction>
                                    <xsl:variable name="txnOrgnlNm">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Orgtr/ns0:Nm" />
                                    </xsl:variable>
                                    <xsl:variable name="txnOrgnlBICorBEI">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI" />
                                    </xsl:variable>
                                    <xsl:variable name="txnStsRsnInf">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:Rsn/ns0:Cd" />
                                    </xsl:variable>
                                    <xsl:variable name="txnStsRsnAddInf">
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:AddtlInf" />
                                    </xsl:variable>
									
									<!--xsl:if test="./ns0:StsRsnInf/ns0:StsRsn/ns0:Prtry">
                                    <StatusReasonInformationReason>
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:StsRsn/ns0:Prtry" />   
                                    </StatusReasonInformationReason>
                                </xsl:if-->
								<StatusReasonInformationReason>
                                 <Code>AC01</Code>
                                 </StatusReasonInformationReason>
									<xsl:if test="./ns0:StsRsnInf/ns0:AddtlStsRsnInf">
                                    <StatusReasonInformationAdditionalInformation>
                                        <xsl:value-of select="./ns0:StsRsnInf/ns0:AddtlStsRsnInf" />   
                                    </StatusReasonInformationAdditionalInformation>
                                </xsl:if>
								
                                    <!-- Added as part of IG2 Spec -->
                                    <!--  StatusReasonInformationAdditionalInformation is mapped at transaction level-->
                                    <xsl:if test="$txnStsRsnAddInf">
                                    <StatusReasonInformationAdditionalInformation>  
                                            <xsl:value-of select="$txnStsRsnAddInf" />
                                    </StatusReasonInformationAdditionalInformation>
                                   <!-- Charges related fields are mapped -->
                                    <xsl:if test="ns0:ChrgsInf">
                                        <xsl:if test="ns0:ChrgsInf/ns0:Amt">
                                            <ChargesInformationAmount>
                                                <xsl:variable name="tempValue" select="ns0:ChrgsInf/ns0:Amt" />
                                                <xsl:variable name="format1">
                                                    <xsl:call-template name="firstCharacter">
                                                        <xsl:with-param name="value" select="'.'" />
                                                        <xsl:with-param name="default" select="'.'" />
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:variable name="format2">
                                                    <xsl:call-template name="firstCharacter">
                                                        <xsl:with-param name="value" select="','" />
                                                        <xsl:with-param name="default" select="','" />
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:variable name="formatValue">
                                                    <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                                </xsl:variable>
                                                <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                            </ChargesInformationAmount>
                                            <xsl:if test="ns0:ChrgsInf/ns0:Amt/@Ccy">
                                                <ChargesInformationAmountCurrency>
                                                   <xsl:value-of select="ns0:ChrgsInf/ns0:Amt/@Ccy" />
                                                </ChargesInformationAmountCurrency>
                                            </xsl:if>
                                        </xsl:if>
                                        
                                        <ChargesInformationAgentFinancialInstitutionIdentificationBICFI>
                                            <xsl:value-of select="ns0:ChrgsInf/ns0:Pty/ns0:FinInstnId/ns0:BIC" />
                                        </ChargesInformationAgentFinancialInstitutionIdentificationBICFI>
                                    </xsl:if>
                                    </xsl:if>       
                                    <xsl:if test="./ns0:StsId">
                                        <StsId>
                                            <xsl:value-of select="./ns0:StsId" />
                                        </StsId>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlInstrId">
                                        <OriginalInstructionIdentification>
                                            <xsl:value-of select="./ns0:OrgnlInstrId" />
                                        </OriginalInstructionIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlEndToEndId">
                                        <OriginalEndToEndIdentification>
                                            <xsl:value-of select="./ns0:OrgnlEndToEndId" />
                                        </OriginalEndToEndIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxId">
                                        <OriginalTransactionIdentification>
                                            <xsl:value-of select="./ns0:OrgnlTxId" />
                                        </OriginalTransactionIdentification>
                                    </xsl:if>
                                    <xsl:if test="./ns0:TxSts">
                                        <TransactionStatus>
                                            <xsl:value-of select="./ns0:TxSts" />
                                        </TransactionStatus>
                                    </xsl:if>
                                    <xsl:if test="($txnOrgnlNm != '') or ($blkOrgnlNm != '') or ($txnOrgnlBICorBEI != '') or ($blkOrgnlBICorBEI != '')">
                                        <StatusReasonInformationOriginator>
                                            <xsl:if test="($txnOrgnlNm != '') or ($blkOrgnlNm != '')">
                                                <Name>
                                                    <xsl:choose>
                                                        <xsl:when test="$txnOrgnlNm != ''">
                                                            <xsl:value-of select="$txnOrgnlNm" />
                                                        </xsl:when>
                                                        <xsl:when test="$blkOrgnlNm != ''">
                                                            <xsl:value-of select="$blkOrgnlNm" />
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </Name>
                                            </xsl:if>
                                            <xsl:if test="($txnOrgnlBICorBEI != '') or ($blkOrgnlBICorBEI != '')">
                                                <BICOrBEI>
                                                    <xsl:choose>
                                                        <xsl:when test="$txnOrgnlBICorBEI != ''">
                                                            <xsl:value-of select="$txnOrgnlBICorBEI" />
                                                        </xsl:when>
                                                        <xsl:when test="$blkOrgnlBICorBEI != ''">
                                                            <xsl:value-of select="$blkOrgnlBICorBEI" />
                                                        </xsl:when>
                                                    </xsl:choose>
                                                </BICOrBEI>
                                            </xsl:if>
                                        </StatusReasonInformationOriginator>
                                    </xsl:if>
                                    <xsl:if test="($txnStsRsnInf != '' or ./ns0:StsRsnInf/ns0:Rsn/ns0:Prtry !='')">
                                        <StatusReasonInformationReason>
                                        <xsl:if test="($txnStsRsnInf != '')">
                                            <Code>
                                                <xsl:value-of select="$txnStsRsnInf" />
                                            </Code>
                                            </xsl:if>
                                            <xsl:if test="./ns0:StsRsnInf/ns0:Rsn/ns0:Prtry !=''">
                                            <Proprietary>
                                                <xsl:value-of select="./ns0:StsRsnInf/ns0:Rsn/ns0:Prtry" />
                                            </Proprietary>
                                            </xsl:if>
                                        </StatusReasonInformationReason>
                                    </xsl:if>   
                                     <xsl:if test="ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt">
                                        <OriginalInterbankSettlementAmount>
                                            <xsl:variable name="tempValue" select="ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt" />
                                            <xsl:variable name="format1">
                                                <xsl:call-template name="firstCharacter">
                                                    <xsl:with-param name="value" select="'.'" />
                                                    <xsl:with-param name="default" select="'.'" />
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:variable name="format2">
                                                <xsl:call-template name="firstCharacter">
                                                    <xsl:with-param name="value" select="','" />
                                                    <xsl:with-param name="default" select="','" />
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:variable name="formatValue">
                                                <xsl:value-of select="format-number($tempValue,'###0.0#')" />
                                            </xsl:variable>
                                            <xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))" />
                                        </OriginalInterbankSettlementAmount>
                                        <xsl:if test="ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt/@Ccy" >
                                            <OriginalInterbankSettlementAmountCurrency>
                                                <xsl:value-of select="ns0:OrgnlTxRef/ns0:IntrBkSttlmAmt/@Ccy " />
                                            </OriginalInterbankSettlementAmountCurrency>
                                        </xsl:if>
                                    </xsl:if>
                                     
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt">
                                        <OriginalInterbankSettlementDate>
                                            <xsl:value-of
                                                select="concat(concat(substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '1', '4'), substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '6', '2')), substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '9', '2'))" />
                                        </OriginalInterbankSettlementDate>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalDebtorAgent>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC" />
                                            </BICFI>
                                        </OriginalDebtorAgent>
                                    </xsl:if>
                                    <xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
                                        <OriginalCreditorAgent>
                                            <BICFI>
                                                <xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC" />
                                            </BICFI>
                                        </OriginalCreditorAgent>
                                    </xsl:if>
                                </Transaction>
                            </xsl:for-each> 
                        </Bulk>
                    </xsl:for-each>
                </ClearingStatusReports> 
            </FileBulks>
        </T24GenericInputFile>
    </xsl:template>
</xsl:stylesheet>