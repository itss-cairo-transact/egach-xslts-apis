<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.007.001.01" exclude-result-prefixes="xs ns0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<T24GenericInputFile>
			<xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance"/>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>.</ReceivedDate>
				<BulkIndex>.</BulkIndex>
				<TransactionIndex>.</TransactionIndex>
				<UniqueReference>.</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>.</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<SenderReferenceIncoming>
					<xsl:value-of select="./ns0:OrgnlTxId"/>
				</SenderReferenceIncoming>
				<FileTypeIndicator>R</FileTypeIndicator>
				<xsl:if test="ns0:Document/ns0:pacs.007.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
					<FileHeaderSendingInstitution>
						<xsl:value-of select="//ns0:pacs.007.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC"/>
					</FileHeaderSendingInstitution>
				</xsl:if>
				<xsl:if test="ns0:Document/ns0:pacs.007.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
					<FileHeaderReceivingInstitution>
						<xsl:value-of select="ns0:Document/ns0:pacs.007.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC"/>
					</FileHeaderReceivingInstitution>
				</xsl:if>
				<FileMessageFormat>pacs.007.001.01</FileMessageFormat>
				<xsl:if test="//ns0:pacs.007.001.01/ns0:GrpHdr/ns0:MsgId">
					<FileHeaderFileReference>
						<xsl:value-of select="//ns0:pacs.007.001.01/ns0:GrpHdr/ns0:MsgId"/>
					</FileHeaderFileReference>
				</xsl:if>
			</FileHeader>
			<FileBulks>
				<DirectDebits>
					<TotalDirectDebitReversalsBulks>1</TotalDirectDebitReversalsBulks>
					<xsl:for-each select="//ns0:pacs.007.001.01">
						<Bulk>
							<BulkHeader>
								<xsl:if test="./ns0:GrpHdr/ns0:MsgId">
									<BulkSendersReference>
										<xsl:value-of select="./ns0:GrpHdr/ns0:MsgId"/>
									</BulkSendersReference>
								</xsl:if>
								<xsl:if test="./ns0:GrpHdr/ns0:CreDtTm">
									<BulkCreationDateTime>
										<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(./ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '18', '2')), substring(string(./ns0:GrpHdr/ns0:CreDtTm), '21', '3'))"/>
									</BulkCreationDateTime>
								</xsl:if>
								<xsl:if test="./ns0:GrpHdr/ns0:NbOfTxs">
									<BulkNumberOfTransactions>
										<xsl:value-of select="./ns0:GrpHdr/ns0:NbOfTxs"/>
									</BulkNumberOfTransactions>
								</xsl:if>
								<xsl:if test="./ns0:GrpHdr/ns0:TtlRvsdIntrBkSttlmAmt">
									<BulkTotalAmount>
										<xsl:value-of select="./ns0:GrpHdr/ns0:TtlRvsdIntrBkSttlmAmt"/>
									</BulkTotalAmount>
								</xsl:if>
								<BulkAmountCurrency>EGP</BulkAmountCurrency>
								<xsl:if test="./ns0:GrpHdr/ns0:IntrBkSttlmDt">
									<BulkSettlementDate>
										<xsl:value-of select="concat(concat(substring(string(./ns0:GrpHdr/ns0:IntrBkSttlmDt), '1', '4'), substring(string(./ns0:GrpHdr/ns0:IntrBkSttlmDt), '6', '2')), substring(string(./ns0:GrpHdr/ns0:IntrBkSttlmDt), '9', '2'))"/>
									</BulkSettlementDate>
								</xsl:if>
								<xsl:if test="./ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
									<BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
										<xsl:value-of select="./ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC"/>
									</BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
								</xsl:if>
								<BulkFormat>pacs.007</BulkFormat>
								<BulkSchemeIndicator>C</BulkSchemeIndicator>
							</BulkHeader>
							<Transaction>
								<ReversalReasonInformationReasonCode>
									<xsl:value-of select="substring(./ns0:TxInf/ns0:RvslRsnInf/ns0:RvslRsn/ns0:Prtry,3,4)"/>
								</ReversalReasonInformationReasonCode>
								<OriginalGroupInformationOriginalMessageIdentification>
									<xsl:value-of select="substring(string(./ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId), '1', '8')"/>
								</OriginalGroupInformationOriginalMessageIdentification>
								<ReversalReasonInformationReasonProprietary>
									<xsl:value-of select="substring(./ns0:TxInf/ns0:RvslRsnInf/ns0:RvslRsn/ns0:Prtry,3,4)"/>
								</ReversalReasonInformationReasonProprietary>
								<xsl:if test="./ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId">
									<OriginalGroupInformationOriginalMessageName>
										<xsl:value-of select="substring(string(./ns0:OrgnlGrpInf/ns0:OrgnlMsgNmId), '1', '8')"/>
									</OriginalGroupInformationOriginalMessageName>
								</xsl:if>
								<xsl:for-each select="./ns0:TxInf">
									<xsl:if test="./ns0:RvslId">
										<ReversalIdentification>
											<xsl:value-of select="./ns0:RvslId"/>
										</ReversalIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlInstrId">
										<OriginalInstructionIdentification>
											<xsl:value-of select="./ns0:OrgnlInstrId"/>
										</OriginalInstructionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlEndToEndId">
										<OriginalEndtoEndIdentification>
											<xsl:value-of select="./ns0:OrgnlEndToEndId"/>
										</OriginalEndtoEndIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxId">
										<OriginalTransactionIdentification>
											<xsl:value-of select="./ns0:OrgnlTxId"/>
										</OriginalTransactionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlIntrBkSttlmAmt">
										<OriginalInterbankSettlementAmount>
											<xsl:value-of select="./ns0:OrgnlIntrBkSttlmAmt"/>
										</OriginalInterbankSettlementAmount>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlIntrBkSttlmAmt/@Ccy">
										<OriginalInterbankSettlementAmountCurrency>
											<xsl:value-of select="./ns0:OrgnlIntrBkSttlmAmt/@Ccy"/>
										</OriginalInterbankSettlementAmountCurrency>
									</xsl:if>
									<xsl:if test="./ns0:RvsdIntrBkSttlmAmt">
										<ReversedInterbankSettlementAmount>
											<xsl:value-of select="./ns0:RvsdIntrBkSttlmAmt"/>
										</ReversedInterbankSettlementAmount>
									</xsl:if>
									<xsl:if test="./ns0:RvsdIntrBkSttlmAmt/@Ccy">
										<ReversedInterbankSettlementAmountCurrency>
											<xsl:value-of select="./ns0:RvsdIntrBkSttlmAmt/@Ccy"/>
										</ReversedInterbankSettlementAmountCurrency>
									</xsl:if>
									<xsl:if test="./ns0:RvsdInstdAmt">
										<ReversedInstructedAmount>
											<xsl:value-of select="./ns0:RvsdInstdAmt"/>
										</ReversedInstructedAmount>
									</xsl:if>
									<xsl:if test="./ns0:RvsdInstdAmt/@Ccy">
										<ReversedInstructedAmountCurrency>
											<xsl:value-of select="./ns0:RvsdInstdAmt/@Ccy"/>
										</ReversedInstructedAmountCurrency>
									</xsl:if>
									<xsl:if test="./ns0:ChrgBr">
										<ChargeBearer>
											<xsl:value-of select="./ns0:ChrgBr"/>
										</ChargeBearer>
									</xsl:if>
									<xsl:if test="./ns0:ChrgsInf/ns0:Amt">
										<ChargesInformationAmount>
											<xsl:value-of select="./ns0:ChrgsInf/ns0:Amt"/>
										</ChargesInformationAmount>
									</xsl:if>
									<xsl:if test="./ns0:ChrgsInf/ns0:Amt/@Ccy">
										<ChargesInformationAmountCurrency>
											<xsl:value-of select="./ns0:ChrgsInf/ns0:Amt/@Ccy"/>
										</ChargesInformationAmountCurrency>
									</xsl:if>
									<xsl:if test="./ns0:ChrgsInf/ns0:Pty/ns0:FinInstnId/ns0:BIC">
										<ChargesInformationPartyFinancialInstitutionIdentification>
											<BICFI>
												<xsl:value-of select="./ns0:ChrgsInf/ns0:Pty/ns0:FinInstnId/ns0:BIC"/>
											</BICFI>
										</ChargesInformationPartyFinancialInstitutionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
										<InstructingAgentFinancialInstitutionIdentification>
											<BICFI>
												<xsl:value-of select="./ns0:InstgAgt/ns0:FinInstnId/ns0:BIC"/>
											</BICFI>
										</InstructingAgentFinancialInstitutionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:RvslRsnInf/ns0:Orgtr">
										<ReversalReasonInformationOriginator>
											<xsl:if test="./ns0:RvslRsnInf/ns0:Orgtr/ns0:Nm">
												<Name>
													<xsl:value-of select="./ns0:RvslRsnInf/ns0:Orgtr/ns0:Nm"/>
												</Name>
											</xsl:if>
											<xsl:if test="./ns0:RvslRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
												<BICOrBEI>
													<xsl:value-of select="./ns0:RvslRsnInf/ns0:Orgtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>
												</BICOrBEI>
											</xsl:if>
										</ReversalReasonInformationOriginator>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt">
										<OriginalInterbankSettlementDate>
											<xsl:value-of select="concat(concat(substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '1', '4'), substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '6', '2')), substring(string(./ns0:OrgnlTxRef/ns0:IntrBkSttlmDt), '9', '2'))"/>
										</OriginalInterbankSettlementDate>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:ReqdColltnDt">
										<OriginalRequestedCollectionDate>
											<xsl:value-of select="concat(concat(substring(string(./ns0:OrgnlTxRef/ns0:ReqdColltnDt), '1', '4'), substring(string(./ns0:OrgnlTxRef/ns0:ReqdColltnDt), '6', '2')), substring(string(./ns0:OrgnlTxRef/ns0:ReqdColltnDt), '9', '2'))"/>
										</OriginalRequestedCollectionDate>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
										<CreditorSchemeIdentificationIdentification>
											<PrivateIentification>
												<OtherIdentification>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
														<Identification>
															<xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id"/>
														</Identification>
													</xsl:if>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
														<SchemeName>
															<Proprietary>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
															</Proprietary>
														</SchemeName>
													</xsl:if>
												</OtherIdentification>
											</PrivateIentification>
										</CreditorSchemeIdentificationIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:SttlmInf/ns0:SttlmMtd">
										<SettlementMethod>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:SttlmInf/ns0:SttlmMtd"/>
										</SettlementMethod>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:SttlmInf/ns0:ClrSys/ns0:Cd">
										<SettlementClearingSystem>
											<Code>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:SttlmInf/ns0:ClrSys/ns0:Cd"/>
											</Code>
										</SettlementClearingSystem>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:SvcLvl/ns0:Cd">
										<PaymentTypeInformationServiceLevel>
											<Code>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:SvcLvl/ns0:Cd"/>
											</Code>
										</PaymentTypeInformationServiceLevel>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:LclInstrm/ns0:Cd">
										<PaymentTypeInformationLocalInstrument>
											<Code>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:LclInstrm/ns0:Cd"/>
											</Code>
										</PaymentTypeInformationLocalInstrument>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:SeqTp">
										<PaymentTypeInformationSequenceType>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:SeqTp"/>
										</PaymentTypeInformationSequenceType>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:CtgyPurp">
										<PaymentTypeInformationCategoryPurpose>
											<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:CtgyPurp/ns0:Cd">
												<Code>
													<xsl:value-of select="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:CtgyPurp/ns0:Cd"/>
												</Code>
											</xsl:if>
											<xsl:if test="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:CtgyPurp/ns0:Prtry">
												<Proprietary>
													<xsl:value-of select="./ns0:OrgnlTxRef/ns0:PmtTpInf/ns0:CtgyPurp/ns0:Prtry"/>
												</Proprietary>
											</xsl:if>
										</PaymentTypeInformationCategoryPurpose>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:MndtId">
										<MandateIdentification>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:MndtId"/>
										</MandateIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:DtOfSgntr">
										<MandateDateOfSignature>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:DtOfSgntr"/>
										</MandateDateOfSignature>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInd">
										<AmendmentIndicator>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInd"/>
										</AmendmentIndicator>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlMndtId">
										<OriginalMandateIdentification>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlMndtId"/>
										</OriginalMandateIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Nm">
										<MandateAmendmentOriginalCreditorSchemeIdentificationName>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Nm"/>
										</MandateAmendmentOriginalCreditorSchemeIdentificationName>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id or ./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
										<MandateAmendmentOriginalCreditorSchemeIdentificationIdentification>
											<PrivateIdentification>
												<OtherIdentification>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
														<Identification>
															<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id"/>
														</Identification>
													</xsl:if>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
														<SchemeName>
															<Proprietary>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlCdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
															</Proprietary>
														</SchemeName>
													</xsl:if>
												</OtherIdentification>
											</PrivateIdentification>
										</MandateAmendmentOriginalCreditorSchemeIdentificationIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlDbtrAcct/ns0:Id/ns0:IBAN">
										<MandateAmendmentOriginalDebtorAccountIdentificationIBAN>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlDbtrAcct/ns0:Id/ns0:IBAN"/>
										</MandateAmendmentOriginalDebtorAccountIdentificationIBAN>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlDbtrAcct/ns0:Id/ns0:Othr/ns0:Id">
										<MandateAmendmentOriginalDebtorAgentFinancialInstitutionIdentification>
											<OtherIdentification>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:AmdmntInfDtls/ns0:OrgnlDbtrAcct/ns0:Id/ns0:Othr/ns0:Id"/>
											</OtherIdentification>
										</MandateAmendmentOriginalDebtorAgentFinancialInstitutionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:ElctrncSgntr">
										<MandateElectronicSignature>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:MndtRltdInf/ns0:ElctrncSgntr"/>
										</MandateElectronicSignature>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf">
										<RemittanceInformation>
											<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Ustrd">
												<Unstructured>
													<xsl:value-of select="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Ustrd"/>
												</Unstructured>
											</xsl:if>
											<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:CdOrPrtry/ns0:Cd or ./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:Issr or ./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Ref">
												<Structured>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:CdOrPrtry/ns0:Cd">
														<CreditorReferenceInformationTypeCodeOrProprietary>
															<Code>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:CdOrPrtry/ns0:Cd"/>
															</Code>
														</CreditorReferenceInformationTypeCodeOrProprietary>
													</xsl:if>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:Issr">
														<CreditorReferenceInformationTypeIssuer>
															<xsl:value-of select="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Tp/ns0:Issr"/>
														</CreditorReferenceInformationTypeIssuer>
													</xsl:if>
													<xsl:if test="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Ref">
														<CreditorReferenceInformationReference>
															<xsl:value-of select="./ns0:OrgnlTxRef/ns0:RmtInf/ns0:Strd/ns0:CdtrRefInf/ns0:Ref"/>
														</CreditorReferenceInformationReference>
													</xsl:if>
												</Structured>
											</xsl:if>
										</RemittanceInformation>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Nm">
										<OriginalUltimateDebtorName>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Nm"/>
										</OriginalUltimateDebtorName>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id">
										<OriginalUltimateDebtorIdentification>
											<xsl:choose>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId">
													<OrganisationIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
															<AnyBIC>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>
															</AnyBIC>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</OrganisationIdentification>
												</xsl:when>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId">
													<PrivateIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt">
															<BirthDate>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt"/>
															</BirthDate>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth">
															<ProvinceOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth"/>
															</ProvinceOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth">
															<CityOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth"/>
															</CityOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth">
															<CountryOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth"/>
															</CountryOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtDbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</PrivateIdentification>
												</xsl:when>
											</xsl:choose>
										</OriginalUltimateDebtorIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Nm">
										<OriginalDebtorName>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Nm"/>
										</OriginalDebtorName>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:PstlAdr/ns0:Ctry">
										<OriginalDebtorPostalAddressCountry>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:PstlAdr/ns0:Ctry"/>
										</OriginalDebtorPostalAddressCountry>
									</xsl:if>
									<xsl:for-each select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:PstlAdr/ns0:AdrLine">
										<xsl:if test="string((position() = '1')) != 'false'">
											<OriginalDebtorPostalAddressAddressLine1>
												<xsl:value-of select="string(.)"/>
											</OriginalDebtorPostalAddressAddressLine1>
										</xsl:if>
										<xsl:if test="string((position() = '2')) != 'false'">
											<OriginalDebtorPostalAddressAddressLine2>
												<xsl:value-of select="string(.)"/>
											</OriginalDebtorPostalAddressAddressLine2>
										</xsl:if>
									</xsl:for-each>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId">
										<OriginalDebtorIdentification>
											<xsl:choose>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId">
													<OrganisationIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
															<AnyBIC>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>
															</AnyBIC>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</OrganisationIdentification>
												</xsl:when>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId">
													<PrivateIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt">
															<BirthDate>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt"/>
															</BirthDate>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth">
															<ProvinceOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth"/>
															</ProvinceOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth">
															<CityOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth"/>
															</CityOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth">
															<CountryOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth"/>
															</CountryOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</PrivateIdentification>
												</xsl:when>
											</xsl:choose>
										</OriginalDebtorIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAcct/ns0:Id/ns0:IBAN">
										<OriginalDebtorAccountIdentification>
											<IBAN>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAcct/ns0:Id/ns0:IBAN"/>
											</IBAN>
										</OriginalDebtorAccountIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
										<OriginalDebtorAgentFinancialInstitutionIdentification>
											<BICFI>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC"/>
											</BICFI>
										</OriginalDebtorAgentFinancialInstitutionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
										<OriginalCreditorAgentFinancialInstitutionIdentification>
											<BICFI>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
											</BICFI>
										</OriginalCreditorAgentFinancialInstitutionIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:Cdtr/ns0:Nm">
										<OriginalCreditorName>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Cdtr/ns0:Nm"/>
										</OriginalCreditorName>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:Cdtr/ns0:PstlAdr/ns0:Ctry">
										<OriginalCreditorPostalAddressCountry>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:Cdtr/ns0:PstlAdr/ns0:Ctry"/>
										</OriginalCreditorPostalAddressCountry>
									</xsl:if>
									<xsl:for-each select="./ns0:OrgnlTxRef/ns0:Cdtr/ns0:PstlAdr/ns0:AdrLine">
										<xsl:if test="string((position() = '1')) != 'false'">
											<OriginalCreditorPostalAddressAddressLine1>
												<xsl:value-of select="string(.)"/>
											</OriginalCreditorPostalAddressAddressLine1>
										</xsl:if>
										<xsl:if test="string((position() = '2')) != 'false'">
											<OriginalCreditorPostalAddressAddressLine2>
												<xsl:value-of select="string(.)"/>
											</OriginalCreditorPostalAddressAddressLine2>
										</xsl:if>
									</xsl:for-each>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:CdtrAcct/ns0:Id/ns0:IBAN">
										<OriginalCreditorAccountIdentification>
											<IBAN>
												<xsl:value-of select="./ns0:OrgnlTxRef/ns0:CdtrAcct/ns0:Id/ns0:IBAN"/>
											</IBAN>
										</OriginalCreditorAccountIdentification>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Nm">
										<OriginalUltimateCreditorName>
											<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Nm"/>
										</OriginalUltimateCreditorName>
									</xsl:if>
									<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr">
										<OriginalUltimateCreditorIdentification>
											<xsl:choose>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId">
													<OrganisationIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
															<AnyBIC>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>
															</AnyBIC>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</OrganisationIdentification>
												</xsl:when>
												<xsl:when test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId">
													<PrivateIdentification>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt">
															<BirthDate>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt"/>
															</BirthDate>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth">
															<ProvinceOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth"/>
															</ProvinceOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth">
															<CityOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth"/>
															</CityOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth">
															<CountryOfBirth>
																<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth"/>
															</CountryOfBirth>
														</xsl:if>
														<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr">
															<OtherIdentification>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id">
																	<Identification>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id"/>
																	</Identification>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm">
																	<SchemeName>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																			<Code>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																			</Code>
																		</xsl:if>
																		<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																			<Proprietary>
																				<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																			</Proprietary>
																		</xsl:if>
																	</SchemeName>
																</xsl:if>
																<xsl:if test="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr">
																	<Issuer>
																		<xsl:value-of select="./ns0:OrgnlTxRef/ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr"/>
																	</Issuer>
																</xsl:if>
															</OtherIdentification>
														</xsl:if>
													</PrivateIdentification>
												</xsl:when>
											</xsl:choose>
										</OriginalUltimateCreditorIdentification>
									</xsl:if>
								</xsl:for-each>
							</Transaction>
						</Bulk>
					</xsl:for-each>
				</DirectDebits>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>