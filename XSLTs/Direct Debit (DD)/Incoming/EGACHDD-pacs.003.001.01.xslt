<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.003.001.01" exclude-result-prefixes="xs ns0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template name="firstCharacter">
		<xsl:param name="value" select="/.."/>
		<xsl:param name="default" select="/.."/>
		<xsl:choose>
			<xsl:when test="(string-length($value) = 0)">
				<xsl:value-of select="$default"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring($value, 1, 1)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="/">
		<T24GenericInputFile>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>.</ReceivedDate>
				<BulkIndex>.</BulkIndex>
				<TransactionIndex>.</TransactionIndex>
				<UniqueReference>.</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>.</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
					<FileHeaderReceivingInstitution>
						<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
					</FileHeaderReceivingInstitution>
				</xsl:if>
				<FileMessageFormat>pacs.003.001.01</FileMessageFormat>
				<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId">
					<FileHeaderFileReference>
						<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId"/>
					</FileHeaderFileReference>
				</xsl:if>
			</FileHeader>
			<FileBulks>
				<DirectDebits>
					<TotalDirectDebitsBulks>
						<xsl:value-of select="string(count(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf))"/>
					</TotalDirectDebitsBulks>
					<Bulk>
						<BulkHeader>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId">
								<BulkSendersReference>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:MsgId"/>
								</BulkSendersReference>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<BulkCreationDateTime>
									<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '1', '4'), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '6', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '9', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '12', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '15', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '18', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:CreDtTm), '21', '3'))"/>
								</BulkCreationDateTime>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:NbOfTxs">
								<BulkNumberOfTransactions>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
								</BulkNumberOfTransactions>
							</xsl:if>
							<BulkTotalAmount>
								<xsl:variable name="tempValue" select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt"/>
								<xsl:variable name="format1">
									<xsl:call-template name="firstCharacter">
										<xsl:with-param name="value" select="'.'"/>
										<xsl:with-param name="default" select="'.'"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="format2">
									<xsl:call-template name="firstCharacter">
										<xsl:with-param name="value" select="','"/>
										<xsl:with-param name="default" select="','"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="formatValue">
									<xsl:value-of select="format-number($tempValue,'###0.0#')"/>
								</xsl:variable>
								<xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))"/>
							</BulkTotalAmount>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt/@Ccy">
								<BulkAmountCurrency>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt/@Ccy"/>
								</BulkAmountCurrency>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt">
								<BulkSettlementDate>
									<xsl:value-of select="concat(concat(substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '1', '4'), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '6', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '9', '2'))"/>
								</BulkSettlementDate>
							</xsl:if>
							<BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
								<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
							</BulkInstructedAgentFinancialInstitutionIdentificationBICFI>
							<BulkFormat>pacs.003</BulkFormat>
							<BulkSchemeIndicator>D</BulkSchemeIndicator>
						</BulkHeader>
						<Transaction>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:TxId">
								<PaymentIdentificationInstructionIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:TxId"/>
								</PaymentIdentificationInstructionIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:EndToEndId">
								<PaymentIdentificationEndToEndIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:EndToEndId"/>
								</PaymentIdentificationEndToEndIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:TxId">
								<PaymentIdentificationTransactionIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:PmtId/ns0:TxId"/>
								</PaymentIdentificationTransactionIdentification>
							</xsl:if>
							<PaymentTypeInformationServiceLevel>
								<Code>EGACHDD</Code>
							</PaymentTypeInformationServiceLevel>
							<PaymentTypeInformationLocalInstrument>
								<Code>CORE</Code>
							</PaymentTypeInformationLocalInstrument>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:InstrPrty">
								<PaymentTypeInformationInstructionPriority>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:InstrPrty"/>
								</PaymentTypeInformationInstructionPriority>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:ClrChanl">
								<PaymentTypeInformationSequenceType>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:ClrChanl"/>
								</PaymentTypeInformationSequenceType>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:SeqTp">
								<TPTransactionNatureCode>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:SeqTp"/>
								</TPTransactionNatureCode>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp">
								<PaymentTypeInformationCategoryPurpose>
									<Code>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp"/>
									</Code>
								</PaymentTypeInformationCategoryPurpose>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:IntrBkSttlmAmt">
								<InterbankSettlementAmount>
									<xsl:variable name="tempValue" select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:IntrBkSttlmAmt"/>
									<xsl:variable name="format1">
										<xsl:call-template name="firstCharacter">
											<xsl:with-param name="value" select="'.'"/>
											<xsl:with-param name="default" select="'.'"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="format2">
										<xsl:call-template name="firstCharacter">
											<xsl:with-param name="value" select="','"/>
											<xsl:with-param name="default" select="','"/>
										</xsl:call-template>
									</xsl:variable>
									<xsl:variable name="formatValue">
										<xsl:value-of select="format-number($tempValue,'###0.0#')"/>
									</xsl:variable>
									<xsl:value-of select="translate($formatValue, '.,', concat($format1, $format2))"/>
								</InterbankSettlementAmount>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:IntrBkSttlmAmt/@Ccy">
									<InterbankSettlementAmountCurrency>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:IntrBkSttlmAmt/@Ccy"/>
									</InterbankSettlementAmountCurrency>
								</xsl:if>
							</xsl:if>
							<ChargeBearer>
								<xsl:choose>
									<xsl:when test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:ChrgBr !=''">
										<xsl:choose>
											<xsl:when test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:ChrgBr = 'SLEV'">
												<xsl:value-of select="'SHA'"/>
											</xsl:when>
											<xsl:when test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:ChrgBr = 'SHAR'">
												<xsl:value-of select="'SHA'"/>
											</xsl:when>
											<xsl:when test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:ChrgBr = 'DEBT'">
												<xsl:value-of select="'OUR'"/>
											</xsl:when>
											<xsl:when test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:ChrgBr = 'CRED'">
												<xsl:value-of select="'BEN'"/>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="'SHA'"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
								</xsl:choose>
							</ChargeBearer>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:ReqdColltnDt">
								<RequestedCollectionDate>
									<xsl:value-of select="concat(concat(substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:ReqdColltnDt), '1', '4'), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:ReqdColltnDt), '6', '2')), substring(string(ns0:Document/ns0:pacs.003.001.01/ns0:ReqdColltnDt), '9', '2'))"/>
								</RequestedCollectionDate>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId">
								<MandateRelatedInformationMandateIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId"/>
								</MandateRelatedInformationMandateIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:DtOfSgntr">
								<MandateRelatedInformationDateOfSignature>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:DtOfSgntr"/>
								</MandateRelatedInformationDateOfSignature>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId">
								<MandateRelatedInformationAmendmentIndicator>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:MndtId"/>
								</MandateRelatedInformationAmendmentIndicator>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:ElctrncSgntr">
								<RelatedInformationElectronicSignature>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTx/ns0:MndtRltdInf/ns0:ElctrncSgntr"/>
								</RelatedInformationElectronicSignature>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DrctDbtTx/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:OthrId/ns0:Id">
								<CreditorSchemePrivateOtherIdentification>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DrctDbtTx/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:OthrId/ns0:Id"/>
								</CreditorSchemePrivateOtherIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
								<CreditorSchemePrivateOtherSchemeNameProprietary>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrSchmeId/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
								</CreditorSchemePrivateOtherSchemeNameProprietary>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Cdtr/ns0:Nm">
								<CreditorName>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Cdtr/ns0:Nm"/>
								</CreditorName>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Cdtr/ns0:PstlAdr/ns0:Ctry">
								<CreditorPostalAddressCountry>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Cdtr/ns0:PstlAdr/ns0:Ctry"/>
								</CreditorPostalAddressCountry>
							</xsl:if>
							<xsl:for-each select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Cdtr/ns0:PstlAdr/ns0:AdrLine">
								<xsl:if test="string((position() = '1')) != 'false'">
									<CreditorPostalAddressAddressLine1>
										<xsl:value-of select="string(.)"/>
									</CreditorPostalAddressAddressLine1>
								</xsl:if>
								<xsl:if test="string((position() = '2')) != 'false'">
									<CreditorPostalAddressAddressLine2>
										<xsl:value-of select="string(.)"/>
									</CreditorPostalAddressAddressLine2>
								</xsl:if>
							</xsl:for-each>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
								<CreditorAccountIdentification>
									<IBAN>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id"/>
									</IBAN>
								</CreditorAccountIdentification>
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
								<CreditorAgentFinancialInstitutionIdentification>
									<BICFI>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
									</BICFI>
								</CreditorAgentFinancialInstitutionIdentification>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Nm">
									<UltimateCreditorName>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Nm"/>
									</UltimateCreditorName>
								</xsl:if>
								<xsl:if test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr)or(ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:BirthDt) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:PrvcOfBirth) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CityOfBirth) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:DtAndPlcOfBirth/ns0:CtryOfBirth) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Id) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:SchmeNm/ns0:Prtry) or (ns0:UltmtCdtr/ns0:Id/ns0:PrvtId/ns0:Othr/ns0:Issr)">
									<UltimateCreditorIdentification>
										<xsl:if test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr)">
											<OrganisationIdentification>
												<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
													<AnyBIC>
														<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI"/>
													</AnyBIC>
												</xsl:if>
												<xsl:if test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry) or (ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr)">
													<OtherIdentification>
														<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id">
															<Identification>
																<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id"/>
															</Identification>
														</xsl:if>
														<xsl:if test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry)">
															<SchemeName>
																<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
																	<Code>
																		<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd"/>
																	</Code>
																</xsl:if>
																<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry">
																	<Proprietary>
																		<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry"/>
																	</Proprietary>
																</xsl:if>
															</SchemeName>
														</xsl:if>
														<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr">
															<Issuer>
																<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtCdtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr"/>
															</Issuer>
														</xsl:if>
													</OtherIdentification>
												</xsl:if>
											</OrganisationIdentification>
										</xsl:if>
									</UltimateCreditorIdentification>
								</xsl:if>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
									<InstructingAgentFinancialInstitutionIdentificationBICFI>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC"/>
									</InstructingAgentFinancialInstitutionIdentificationBICFI>
								</xsl:if>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Dbtr/ns0:Nm">
									<DebtorName>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:Dbtr/ns0:Nm"/>
									</DebtorName>
								</xsl:if>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
									<DebtorAccountIdentification>
										<IBAN>
											<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id"/>
										</IBAN>
									</DebtorAccountIdentification>
								</xsl:if>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
									<DebtorAgentFinancialInstitutionIdentification>
										<BICFI>
											<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC"/>
										</BICFI>
									</DebtorAgentFinancialInstitutionIdentification>
								</xsl:if>
								<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Nm">
									<UltimateDebtorName>
										<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Nm"/>
									</UltimateDebtorName>
								</xsl:if>
								<!--UltimateDebtorIdentification>
                                            <OrganisationIdentification>
                                                <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI">
                                                    <AnyBIC>
                                                        <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:BICOrBEI" />
                                                    </AnyBIC>
                                                </xsl:if>
                                                <xsl:if
                                                    test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry) or (ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr)">
                                                    <OtherIdentification>
                                                        <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id">
                                                            <Identification>
                                                                <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Id" />
                                                            </Identification>
                                                        </xsl:if>
                                                        <xsl:if
                                                            test="(ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd) or (ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Prtry)">
                                                            <SchemeName>
                                                                <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd">
                                                                    <Code>
                                                                        <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:SchmeNm/ns0:Cd" />
                                                                    </Code>
                                                                </xsl:if>
																 </SchemeName>
                                                        </xsl:if>
                                                        <xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr">
                                                            <Issuer>
                                                                <xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:UltmtDbtr/ns0:Id/ns0:OrgId/ns0:Othr/ns0:Issr" />
                                                            </Issuer>
                                                        </xsl:if>
                                                    </OtherIdentification>
                                                </xsl:if>
                                            </OrganisationIdentification>
											</UltimateDebtorIdentification-->
							</xsl:if>
							<xsl:if test="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:RmtInf/ns0:Ustrd">
								<TransactionRemittanceInformationUnstructured>
									<xsl:value-of select="ns0:Document/ns0:pacs.003.001.01/ns0:DrctDbtTxInf/ns0:RmtInf/ns0:Ustrd"/>
								</TransactionRemittanceInformationUnstructured>
							</xsl:if>
							<TPTransactionTransferType>C</TPTransactionTransferType>
							<ChargeWaiver>C</ChargeWaiver>
						</Transaction>
					</Bulk>
				</DirectDebits>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>              