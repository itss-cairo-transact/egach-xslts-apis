* @ValidationCode : MjotMTU4MTk4NDk2NjpDcDEyNTI6MTYzODExMzk3ODQzOTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Nov 2021 17:39:38
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE EGACHService.EGACHBulkCriteria(iBulkingCriteriaDetails, oClearingBulking, oBulkingCriteriaResponse)
*------------------------------------------------------------------------------
* In/out parameters:
* iBulkingCriteriaDetails - BulkingCriteriaDetails, IN
* oClearingBulking - ClearingBulking (Single), OUT
* oBulkingCriteriaResponse - BulkingCriteriaResponse (Single), OUT
*
* Program Description:
*  This program is used to invoke, when the bulkingCriteria needs to be created for EGACH clearing.
*------------------------------------------------------------------------------
    $USING PP.OutwardMappingFramework
*------------------------------------------------------------------------------

    GOSUB setInputLog
    GOSUB initialise
    GOSUB process
    GOSUB setOutputLog

RETURN
*------------------------------------------------------------------------------
process:

    GOSUB determineBulkPrint
    GOSUB determineFileformat
*
RETURN
*------------------------------------------------------------------------------
determineBulkPrint:

    IF iClearingTransactionType EQ 'CT' OR iClearingTransactionType EQ 'RT' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.clearingCurrency> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.clearingNatureCode> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.clearingTransactionType> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.creditValueDate> = 'Y'
    END
* message pacs28 for cancellation will be processed as part of the bulk
    
* for camt.087 and camt.027 bulksendersReference also taken as bulking criteria so that individual transactions will be part of seprate bulk.
    IF iClearingTransactionType EQ 'CA' OR iClearingTransactionType EQ 'CM' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.bulkSendersReference> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END
    
* for camt.029 send for camt.027 and camt.087 alone, the same bulking logic for camt.087 and camt.027 should be applied.
    IF iClearingTransactionType EQ 'RI-CA' OR iClearingTransactionType EQ 'RI-CM' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.bulkSendersReference> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END
* message pacs28 for cancellation will be processed as part of the bulk
    IF iClearingTransactionType EQ 'RI' OR iClearingTransactionType EQ 'CR' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END
    
* For pacs.028 if criteria configuration API is configured, the STEP2 API will enrich generic criteria input as bulkReference
* and hence pacs.028 will be bulked based on the bulkreference of the original message.
    IF iClearingTransactionType EQ 'SR-CA' OR iClearingTransactionType EQ 'SR-CM' OR iClearingTransactionType EQ 'SR' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.genericCriteriaInput1> = 'Y'
    END

    IF iClearingTransactionType EQ 'CR-DD' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END
    
    IF iClearingTransactionType EQ 'RF' OR iClearingTransactionType EQ 'RD' OR iClearingTransactionType EQ 'RV' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.debitValueDate> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.bankOperationCode> = 'Y'
    END

    IF iClearingTransactionType EQ 'DD' AND iOutgoingMessageType EQ 'pacs.002' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.bulkSendersReference> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END

    IF iClearingTransactionType EQ 'DD' AND iOutgoingMessageType NE 'pacs.002' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.debitValueDate> = 'Y'
    END
    
    IF iClearingTransactionType EQ 'CC' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.clearingCurrency> = 'Y'
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.outgoingMessageType> = 'Y'
    END
    

RETURN
*------------------------------------------------------------------------------
determineFileformat:

    IF iClearingTransactionType EQ 'CT' OR iClearingTransactionType EQ 'RT' OR iClearingTransactionType EQ 'CR' OR iClearingTransactionType EQ 'SR' OR iClearingTransactionType EQ 'RI' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.fileFormat> = 'ICF'
    END

    IF iClearingTransactionType EQ 'CA' OR iClearingTransactionType EQ 'CM' THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.fileFormat> = 'IQF'
    END
    
    IF iClearingTransactionType EQ 'RI-CA' OR iClearingTransactionType EQ 'RI-CM' OR iClearingTransactionType EQ "SR-CA" OR iClearingTransactionType EQ "SR-CM" THEN
        oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.fileFormat> = 'IQF'
    END
    
    IF iClearingTransactionType EQ 'DD' OR iClearingTransactionType EQ 'RD' OR iClearingTransactionType EQ 'RJ' OR iClearingTransactionType EQ 'RF' OR iClearingTransactionType EQ 'RV' OR iClearingTransactionType EQ 'CR-DD' THEN
        IF iBankOperationCode EQ 'CORE' THEN
            oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.fileFormat> = 'IDFC'
        END
        IF iBankOperationCode EQ 'B2B' THEN
            oClearingBulking<PP.OutwardMappingFramework.ClearingBulking.fileFormat> = 'IDFB'
        END

    END

RETURN
*------------------------------------------------------------------------------
initialise:
*
    iClearingTransactionType = ''
    iBankOperationCode = ''
    CONVERT @VM TO  @FM IN iBulkingCriteriaDetails
    iClearingTransactionType = iBulkingCriteriaDetails<PP.OutwardMappingFramework.BulkingCriteriaDetails.clearingTransactionType>
    iBankOperationCode = iBulkingCriteriaDetails<PP.OutwardMappingFramework.BulkingCriteriaDetails.bankOperationCode>
    iOutgoingMessageType = iBulkingCriteriaDetails<PP.OutwardMappingFramework.BulkingCriteriaDetails.outgoingMessageType>
    oClearingBulking = ''
    oBulkingCriteriaResponse = ''
    isoMsgType = iBulkingCriteriaDetails<PP.OutwardMappingFramework.BulkingCriteriaDetails.genericCriteriaInput1>
    origMsgType =  iBulkingCriteriaDetails<PP.OutwardMappingFramework.BulkingCriteriaDetails.genericCriteriaInput2>
*
RETURN

*------------------------------------------------------------------------------
setInputLog:
* Logging to see input
    CALL TPSLogging("Start", "EGACHService.EGACHBulkingCriteria", "", "")
    CALL TPSLogging("Version", "OutwardMappingFramework.STEP2determineBulkingCriteria", "DEFECT 2332633 , Date - 07-NOV-2017", "")
    CALL TPSLogging("Input Parameter", "OutwardMappingFramework.STEP2determineBulkingCriteria", "iBulkingCriteriaDetails : <":iBulkingCriteriaDetails:">", "")


*
RETURN
*------------------------------------------------------------------------------
setOutputLog:
* Logging to see output
    CALL TPSLogging("Output Parameter", "OutwardMappingFramework.STEP2determineBulkingCriteria", "oClearingBulking : <":oClearingBulking:">", "")
    CALL TPSLogging("Output Parameter", "OutwardMappingFramework.STEP2determineBulkingCriteria", "oBulkingCriteriaResponse : <":oBulkingCriteriaResponse:">", "")
    CALL TPSLogging("End", "OutwardMappingFramework.STEP2determineBulkingCriteria", "", "")

*
RETURN
*------------------------------------------------------------------------------

END
