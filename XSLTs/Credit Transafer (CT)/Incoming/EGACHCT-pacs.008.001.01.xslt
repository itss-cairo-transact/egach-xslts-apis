<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Liquid Studio 2020 (https://www.liquid-technologies.com) -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ns0="urn:iso:std:iso:20022:tech:xsd:pacs.008.001.01" exclude-result-prefixes="xs ns0">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="/">
		<T24GenericInputFile>
			<xsl:attribute name="xsi:noNamespaceSchemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">file:///C:/Users/mgodeanu/Desktop/StandardInputGenericXML.xsd</xsl:attribute>
			<FileInfo>
				<FileName>.</FileName>
				<QueueName>.</QueueName>
				<ReceivedDate>.</ReceivedDate>
				<BulkIndex>.</BulkIndex>
				<TransactionIndex>.</TransactionIndex>
				<UniqueReference>.</UniqueReference>
				<ProcessingStatus>RECEIVED</ProcessingStatus>
				<ProcessingStatusDescription>.</ProcessingStatusDescription>
				<Content>.</Content>
			</FileInfo>
			<FileHeader>
				<FileMessageFormat>pacs.008.001.01</FileMessageFormat>
				<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId">
					<FileHeaderFileReference>
						<xsl:value-of select="concat(//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId,'-',//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:ClrChanl)"/>
					</FileHeaderFileReference>
				</xsl:if>
				<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId">
                    <FileHeaderFileReference>
                        <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId" />
                    </FileHeaderFileReference>
                </xsl:if-->
				<!--Changes for Equens SEPA CT-->
				<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:CdtTrfTxInf/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
					<FileHeaderSendingInstitution>
						<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:CdtTrfTxInf/ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC"/>
					</FileHeaderSendingInstitution>
				</xsl:if>
				<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:CdtTrfTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
					<FileHeaderReceivingInstitution>
						<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:CdtTrfTxInf/ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
					</FileHeaderReceivingInstitution>
				</xsl:if>
				<!-- Changes for Equens SEPA CT-->
			</FileHeader>
			<FileBulks>
				<CreditTransfers>
					<TotalNumberOfCreditTransfersBulks>1</TotalNumberOfCreditTransfersBulks>
					<Bulk>
						<BulkHeader>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId">
								<BulkSendersReference>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:MsgId"/>
								</BulkSendersReference>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:CreDtTm">
								<!-- Added as part of IG2DEFECT -->
								<BulkCreationDateTime>
									<!--<xsl:value-of select="concat(concat(concat(concat(concat(concat(substring(string(//ns0:Document/pacs.008.001.01/GrpHdr/CreDtTm), '1', '4'), substring(string(//ns0:Document/pacs.008.001.01/GrpHdr/CreDtTm), '6', '2')), substring(string(//ns0:Document/pacs.008.001.01/GrpHdr/CreDtTm), '9', '2')), substring(string(//ns0:Document/pacs.008.001.01/GrpHdr/CreDtTm), '12', '2')), substring(string(Document/pacs.008.001.01/GrpHdr/CreDtTm), '15', '2')), substring(string(//ns0:Document/pacs.008.001.01/GrpHdr/CreDtTm), '18', '2')), '000')" />-->
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:CreDtTm"/>
								</BulkCreationDateTime>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:NbOfTxs">
								<BulkNumberOfTransactions>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:NbOfTxs"/>
								</BulkNumberOfTransactions>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt">
								<BulkTotalAmount>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt"/>
								</BulkTotalAmount>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt/@Ccy">
								<BulkAmountCurrency>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:TtlIntrBkSttlmAmt/@Ccy"/>
								</BulkAmountCurrency>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt">
								<BulkSettlementDate>
									<xsl:value-of select="concat(concat(substring(string(//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '1', '4'), substring(string(//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '6', '2')), substring(string(//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:IntrBkSttlmDt), '9', '2'))"/>
								</BulkSettlementDate>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:SttlmMtd">
								<SettlementMethod>CLRG</SettlementMethod>
							</xsl:if>
							<!--Changes for Equens Clearing-->
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:SttlmInf/ns0:ClrSys/ns0:ClrSysId">
								<BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemCode>PEG</BulkInstructedAgentFinancialInstitutionIdentificationClearingSystemCode>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:InstrPrty">
								<PaymentTypeInformationInstructionPriority>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:InstrPrty"/>
								</PaymentTypeInformationInstructionPriority>
							</xsl:if>
							<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:ClrChanl">
                                <ClrChanl>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:ClrChanl"/>
                                </ClrChanl>
                            </xsl:if-->
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp">
								<PaymentTypeInformationCategoryPurpose>
									<Proprietary>
										<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:PmtTpInf/ns0:CtgyPurp"/>
									</Proprietary>
								</PaymentTypeInformationCategoryPurpose>
							</xsl:if>
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
								<FileHeaderSendingInstitution>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:FinInstnId/ns0:BIC"/>
								</FileHeaderSendingInstitution>
							</xsl:if>
							<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:BrnchId/ns0:Id">
                                <Id>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:BrnchId/ns0:Id">
                                </Id>
                            </xsl:if-->
							<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:BrnchId/ns0:Nm">
                                <Nm>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstgAgt/ns0:BrnchId/ns0:Nm">
                                </Nm>
                            </xsl:if-->
							<xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
								<FileHeaderReceivingInstitution>
									<xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:FinInstnId/ns0:BIC"/>
								</FileHeaderReceivingInstitution>
							</xsl:if>
							<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:BrnchId/ns0:Id">
                                <Id>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:BrnchId/ns0:Id">
                                </Id>
                            </xsl:if-->
							<!--xsl:if test="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:BrnchId/ns0:Nm">
                                <Nm>
                                    <xsl:value-of select="//ns0:Document/ns0:pacs.008.001.01/ns0:GrpHdr/ns0:InstdAgt/ns0:BrnchId/ns0:Nm">
                                </Nm>
                            </xsl:if-->
							<!--Changes for Equens Clearing-->
							<BulkFormat>pacs.008</BulkFormat>
							<BulkSchemeIndicator>C</BulkSchemeIndicator>
						</BulkHeader>
						<xsl:for-each select="//ns0:Document/ns0:pacs.008.001.01/ns0:CdtTrfTxInf">
							<Transaction>
								<!-- Added as part of IG2 Spec -->
								<!--  Instructing agent at transaction level is mapped -->
								<!--xsl:if test="./ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
                                    <InstructingAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC" />
                                    </InstructingAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if-->
								<xsl:if test="./ns0:PmtId/ns0:InstrId">
									<PaymentIdentificationInstructionIdentification>
										<xsl:value-of select="./ns0:PmtId/ns0:InstrId"/>
									</PaymentIdentificationInstructionIdentification>
								</xsl:if>
								<xsl:if test="./ns0:PmtId/ns0:EndToEndId">
									<PaymentIdentificationEndToEndIdentification>
										<xsl:value-of select="./ns0:PmtId/ns0:EndToEndId"/>
									</PaymentIdentificationEndToEndIdentification>
								</xsl:if>
								<xsl:if test="./ns0:PmtId/ns0:TxId">
									<PaymentIdentificationTransactionIdentification>
										<xsl:value-of select="./ns0:PmtId/ns0:TxId"/>
									</PaymentIdentificationTransactionIdentification>
								</xsl:if>
								<xsl:if test="./ns0:IntrBkSttlmAmt">
									<InterbankSettlementAmount>
										<xsl:value-of select="./ns0:IntrBkSttlmAmt"/>
									</InterbankSettlementAmount>
								</xsl:if>
								<xsl:if test="./ns0:IntrBkSttlmAmt/@Ccy">
									<InterbankSettlementAmountCurrency>
										<xsl:value-of select="./ns0:IntrBkSttlmAmt/@Ccy"/>
									</InterbankSettlementAmountCurrency>
								</xsl:if>
								<xsl:choose>
									<xsl:when test="./ns0:ChrgBr = 'SLEV'">
										<ChargeBearer>SHA</ChargeBearer>
									</xsl:when>
									<xsl:when test="./ns0:ChrgBr = 'SHAR'">
										<ChargeBearer>SHA</ChargeBearer>
									</xsl:when>
									<xsl:when test="./ns0:ChrgBr = 'DEBT'">
										<ChargeBearer>OUR</ChargeBearer>
									</xsl:when>
									<xsl:when test="./ns0:ChrgBr = 'CRED'">
										<ChargeBearer>BEN</ChargeBearer>
									</xsl:when>
									<xsl:otherwise>
										<ChargeBearer>SHA</ChargeBearer>
									</xsl:otherwise>
								</xsl:choose>
								<!--                                <xsl:if test="./ns0:InstgAgt/ns0:FinInstnId/ns0:BIC">
                                    <InstructingAgentFinancialInstitutionIdentificationBICFI>
                                        <xsl:value-of select="./ns0:InstgAgt/ns0:FinInstnId/ns0:BIC" />
                                    </InstructingAgentFinancialInstitutionIdentificationBICFI>
                                </xsl:if>-->
								<!--xsl:if test="./ns0:InstgAgt/ns0:FinInstnId/ns0:Id">
                                    <Id>
                                        <xsl:value-of select="./ns0:InstgAgt/ns0:FinInstnId/ns0:Id" />
                                    </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:InstgAgt/ns0:FinInstnId/ns0:Nm">
                                    <Nm>
                                        <xsl:value-of select="./ns0:InstgAgt/ns0:FinInstnId/ns0:Nm" />
                                    </Nm>
                                </xsl:if-->
								<!--                                <xsl:if test="./ns0:InstdAgt/ns0:FinInstnId/ns0:BIC">
                                    <FileHeaderReceivingInstitution>
                                        <xsl:value-of select="./ns0:InstdAgt/ns0:FinInstnId/ns0:BIC" />
                                    </FileHeaderReceivingInstitution>
                                </xsl:if>-->
								<!--xsl:if test="./ns0:InstdAgt/ns0:FinInstnId/ns0:Id">
                                    <Id>
                                        <xsl:value-of select="./ns0:InstdAgt/ns0:FinInstnId/ns0:Id" />
                                    </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:InstdAgt/ns0:FinInstnId/ns0:Nm">
                                    <Nm>
                                        <xsl:value-of select="./ns0:InstdAgt/ns0:FinInstnId/ns0:Nm" />
                                    </Nm>
                                </xsl:if-->
								<xsl:if test="./ns0:Dbtr/ns0:Nm">
									<DebtorName>
										<xsl:value-of select="./ns0:Dbtr/ns0:Nm"/>
									</DebtorName>
								</xsl:if>
								<xsl:for-each select="./ns0:Dbtr/ns0:PstlAdr/ns0:AdrLine">
									<xsl:if test="string((position() = '1')) != 'false'">
										<DebtorPostalAddressAddressLine1>
											<xsl:value-of select="string(.)"/>
										</DebtorPostalAddressAddressLine1>
									</xsl:if>
									<xsl:if test="string((position() = '2')) != 'false'">
										<DebtorPostalAddressAddressLine2>
											<xsl:value-of select="string(.)"/>
										</DebtorPostalAddressAddressLine2>
									</xsl:if>
									<xsl:if test="./ns0:Dbtr/ns0:PstlAdr/ns0:Ctry">
										<DebtorPostalAddressCountry>
											<xsl:value-of select="./ns0:Dbtr/ns0:PstlAdr/ns0:Ctry"/>
										</DebtorPostalAddressCountry>
									</xsl:if>
								</xsl:for-each>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:PsptNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:PsptNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb">
									<DebtorIdentification>
										<xsl:value-of select="./ns0:Dbtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb"/>
									</DebtorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
									<DebtorAccountIdentification>
									<IBAN>
										<xsl:value-of select="./ns0:DbtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id"/></IBAN>
									</DebtorAccountIdentification>
								</xsl:if>
								<!--xsl:if test="./ns0:DbtrAcct/ns0:Tp/ns0:Cd">
                                    <Cd>
                                        <xsl:value-of select="./ns0:DbtrAcct/ns0:Tp/ns0:Cd" />
                                    </Cd>
                                </xsl:if-->
								<xsl:if test="./ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC">
									<DebtorAgentFinancialInstitutionIdentification>
										<BICFI>
											<xsl:value-of select="./ns0:DbtrAgt/ns0:FinInstnId/ns0:BIC"/>
										</BICFI>
									</DebtorAgentFinancialInstitutionIdentification>
								</xsl:if>
								<!--xsl:if test="./ns0:DbtrAgt/ns0:BrnchId/ns0:Id">
                                    <Id>
                                            <xsl:value-of select="./ns0:DbtrAgt/ns0:BrnchId/ns0:Id">
                                        </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:DbtrAgt/ns0:BrnchId/ns0:Nm">
                                    <Nm>
                                            <xsl:value-of select="./ns0:DbtrAgt/ns0:BrnchId/ns0:Nm">
                                        </Nm>
                                </xsl:if-->
								<!--xsl:if test="./ns0:DbtrAgtAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                    <Id>
                                            <xsl:value-of select="./ns0:DbtrAgtAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                        </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:DbtrAgtAcct/ns0:Tp/ns0:Cd>
                                    <Cd>
                                            <xsl:value-of select="./ns0:DbtrAgtAcct/ns0:DbtrAgtAcct/ns0:Tp/ns0:Cd">
                                        </Cd>
                                </xsl:if-->
								<xsl:if test="./ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC">
									<CreditorAgentFinancialInstitutionIdentification>
										<BICFI>
											<xsl:value-of select="./ns0:CdtrAgt/ns0:FinInstnId/ns0:BIC"/>
										</BICFI>
									</CreditorAgentFinancialInstitutionIdentification>
								</xsl:if>
								<!--xsl:if test="./ns0:CdtrAgt/ns0:BrnchId/ns0:Id">
                                    <Id>
                                            <xsl:value-of select="./ns0:CdtrAgt/ns0:BrnchId/ns0:Id">
                                        </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:CdtrAgt/ns0:BrnchId/ns0:Nm">
                                    <Nm>
                                            <xsl:value-of select="./ns0:CdtrAgt/ns0:BrnchId/ns0:Nm">
                                        </Nm>
                                </xsl:if-->
								<!--xsl:if test="./ns0:CdtrAgt/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                    <Id>
                                            <xsl:value-of select="./ns0:CdtrAgt/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                        </Id>
                                </xsl:if-->
								<!--xsl:if test="./ns0:CdtrAgt/ns0:Tp/ns0:Cd>
                                    <Cd>
                                            <xsl:value-of select="./ns0:CdtrAgt/ns0:DbtrAgtAcct/ns0:Tp/ns0:Cd">
                                        </Cd>
                                </xsl:if-->
								<xsl:if test="./ns0:Cdtr/ns0:Nm">
									<CreditorName>
										<xsl:value-of select="./ns0:Cdtr/ns0:Nm"/>
									</CreditorName>
								</xsl:if>
								<xsl:for-each select="./ns0:Cdtr/ns0:PstlAdr/ns0:AdrLine">
									<xsl:if test="string((position() = '1')) != 'false'">
										<CreditorPostalAddressAddressLine1>
											<xsl:value-of select="string(.)"/>
										</CreditorPostalAddressAddressLine1>
									</xsl:if>
									<xsl:if test="string((position() = '2')) != 'false'">
										<CreditorPostalAddressAddressLine2>
											<xsl:value-of select="string(.)"/>
										</CreditorPostalAddressAddressLine2>
									</xsl:if>
									<xsl:if test="./ns0:Cdtr/ns0:PstlAdr/ns0:Ctry">
										<CreditorPostalAddressCountry>
											<xsl:value-of select="./ns0:Cdtr/ns0:PstlAdr/ns0:Ctry"/>
										</CreditorPostalAddressCountry>
									</xsl:if>
								</xsl:for-each>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:DrvrsLicNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:CstmrNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:SclSctyNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:AlnRegnNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:PsptNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:PsptNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:TaxIdNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:IdntyCardNb"/>
									</CreditorIdentification>
								</xsl:if>
								<xsl:if test="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb">
									<CreditorIdentification>
										<xsl:value-of select="./ns0:Cdtr/ns0:Id/ns0:PrvtId/ns0:MplyrIdNb"/>
									</CreditorIdentification>
								</xsl:if>
								<!--xsl:if test="./ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                    <CreditorAccountIdentification>
                                        <xsl:value-of select="./ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id" />
                                    </CreditorAccountIdentification>
                                </xsl:if-->
								<xsl:if test="./ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id">
                                                                            <CreditorAccountIdentification>
                                                                                    <IBAN>
                                                                                            <xsl:value-of select="./ns0:CdtrAcct/ns0:Id/ns0:PrtryAcct/ns0:Id"/>
                                                                                        </IBAN>
                                                                                </CreditorAccountIdentification>
                                                                        </xsl:if>
								<!--xsl:if test="./ns0:CdtrAcct/ns0:Tp/ns0:Cd">
                                    <Cd>
                                        <xsl:value-of select="./ns0:CdtrAcct/ns0:Tp/ns0:Cd" />
                                    </Cd>
                                </xsl:if-->
								<xsl:if test="./ns0:Purp/ns0:Cd">
									<PaymentTypeInformationCategoryPurpose><Proprietary>
										<xsl:value-of select="./ns0:Purp/ns0:Cd"/></Proprietary>
									</PaymentTypeInformationCategoryPurpose>
								</xsl:if>
								<xsl:if test="./ns0:RmtInf/ns0:Ustrd">
									<RemittanceInformation><Unstructured>
										<xsl:value-of select="./ns0:RmtInf/ns0:Ustrd"/></Unstructured>
									</RemittanceInformation>
								</xsl:if>
							</Transaction>
						</xsl:for-each>
					</Bulk>
				</CreditTransfers>
			</FileBulks>
		</T24GenericInputFile>
	</xsl:template>
</xsl:stylesheet>