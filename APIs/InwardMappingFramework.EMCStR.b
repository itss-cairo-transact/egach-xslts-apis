* @ValidationCode : Mjo2NjgxNDA3OTM6Q3AxMjUyOjE2Mzg4MjU4MTc0ODU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 06 Dec 2021 23:23:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0


*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE InwardMappingFramework.EMCStR(iFileData, iBulkData, ioMessageData, iIncomingMessage, ioPaymentObject, oCSRMappingResponse)
*-----------------------------------------------------------------------------
* In/out parameters:
* iFileData - PP.InwardMappingFramework.FileDataObject, IN
* iBulkData - PP.InwardMappingFramework.BulkDataObject, IN
* ioMessageData - PP.InwardMappingFramework.MessageDataObject, INOUT
* iIncomingMessage - String, IN
* oCSRMappingResponse - PaymentResponse, OUT
*-----------------------------------------------------------------------------
    $USING PP.InwardMappingFramework
    $USING PP.ClearingStatusReport
    $USING PP.OrderEntryRepairService
    $USING PP.PaymentFrameworkService
    $USING PP.PaymentReturn
    $INSERT I_PaymentWorkflowService_PaymentResponse
    $INSERT I_PaymentWorkflowService_ResponseMessage
    $INSERT I_LocalClearingService_ClearingSettingRequest
    $INSERT I_ChequeService_ChqTxnDetails
    $INSERT I_ChequeService_ChqRequestStatus
    $INSERT I_PaymentWorkflowDASService_PaymentRecord
    $INSERT I_PaymentWorkflowDASService_PaymentID
    $INSERT I_F.PP.INVST.FILE
    $INSERT I_PaymentFrameworkService_PORHistoryLog
    $USING PP.PaymentWorkflowDASService
    
*------------------------------------------------------------------------------
    GOSUB initialise ;* Local Initialise
    GOSUB parseCSR
    GOSUB getNSRDetails
    GOSUB process ;* Main process
*
RETURN
*------------------------------------------------------------------------------
initialise:
* Output parameters
    oFTNumber = ''
    oMappingResponse = ''

* Local initialise
    iClearingSetting = ''
    oClearingSetting = ''
    oClearingSettingErr = ''
    oActionType = ''
    oCSRMappingResponse = ''
    iAccpInstContext = ''
    iRjctInstContext = ''
    oAccpResponse = ''
    oRjctResponse = ''
    iCompanyId = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
    iOrglFormat = ''
*
RETURN
*------------------------------------------------------------------------------
parseCSR:
* Call Parse Routine for XMl and NON XML based on mapping Table ID
    IF iFileData<PP.InwardMappingFramework.FileDataObject.mappingTableID> EQ '' THEN
        PP.EGACHService.parseClearingStatusReport(iIncomingMessage, oMessageData, oCTResponse)
    END ELSE
        PP.EGACHService.parseClearingStatusReportNonXML(iFileData, iIncomingMessage, oMessageData, oCTResponse)
    END
    iParseData = oMessageData
RETURN
*-----------------------------------------------------------------------------

getNSRDetails:
*--------------

    LOCATE "NSRDetails" IN oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName,1> SETTING POS THEN
        ioMessageData<PP.InwardMappingFramework.MessageDataObject.nsrFlag> = 1
        GOSUB programExit
    END
   
RETURN
*-----------------------------------------------------------------------------
process:

* File Type Indicator is set as 'T' for 'admi.007' AckNack Message Type
* AckNackProcessing routine is called for admi.007 and returns from the existing flow as the process is different from other message types
* Pushes the payment to exception queue or complete the payment based on the Acknowledgement received
    IF iFileData<PP.InwardMappingFramework.FileDataObject.typeIndicator> EQ 'T' THEN
        PP.EGACHService.AckNackProcessing(iFileData, iBulkData, iParseData, oDasError)
        RETURN
    END
* read from Clearing Settings with the following input in order to determine the action to be done further based on automatedReturnIndicator and singleMultipleIndicator fields values
* Added one more condition i.e. If OriginalMsgFormat for Instant payments is pacs.008, then only rejectProcessing need to called otherwise bypass it.
    iOrglFormat = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCgStsReportOrglFormat>
    iOrglFormat = SUBSTRINGS(iOrglFormat,1,8)
    iOrglFormatLower = iOrglFormat
    iOrglFormat = UPCASE(iOrglFormat)
    iGroupStatus = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCgStsReportGroupStatus>
    
* Get the local instrument code and check whether it is Instant or Near Real Instant payment
* if not the get corresponding instant payment method given in the message mapping param
    paymentMethod = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrCd>
    IF paymentMethod AND (paymentMethod NE "INST") AND (paymentMethod NE "NRINST") THEN
* Now we have other local instrument code, so get the instant payment method from the message mapping param.
        LOCATE paymentMethod IN ioMessageData<PP.InwardMappingFramework.MessageDataObject.instantLocalInstrumentCode,1> SETTING codePosition THEN
            paymentMethod = ioMessageData<PP.InwardMappingFramework.MessageDataObject.instantPaymentMethod,codePosition>
        END
    END
    GOSUB getPaymentMethod
* New group status will be allowed in pacs 002 which is ACSC And ACWC
* ACSC - Accepted with settlement (Funds made available to the beneficiary instantly)
* ACWC - Accepted with change (Funds not made available to the beneficiary instantly)
* pacs 004 will now be changed to non instant message hence the check for pacs004
    
*    IF ((paymentMethod EQ "INST") OR (paymentMethod EQ "NRINST")) AND ((iOrglFormat EQ "PACS.008") OR ((iOrglFormat EQ "PACS.004") AND (iGroupStatus EQ "ACCP"))) THEN
    IF ((paymentMethod EQ "INST") OR (paymentMethod EQ "NRINST")) THEN
        IF (iGroupStatus EQ "RJCT") THEN
* If the group status is RJCT then it is reject message so call rejectProcessingForInstPymt
            iRjctInstContext<PP.ClearingStatusReport.RjctInstContext.companyID> = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
            iRjctInstContext<PP.ClearingStatusReport.RjctInstContext.ftNumber> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
            iRjctInstContext<PP.ClearingStatusReport.RjctInstContext.rjctCode> = iFileData<PP.InwardMappingFramework.FileDataObject.originatingSource>
            PP.ClearingStatusReport.rejectProcessingForInstPymt(iFileData, iBulkData, oMessageData, iRjctInstContext, oCSRMappingResponse)
        END ELSE
* Other then RJCT all other will be assumed as accepted so call the accpProcessingForInstPymt
            iAccpInstContext<PP.ClearingStatusReport.AccpInstContext.companyID> = iCompanyId
            iAccpInstContext<PP.ClearingStatusReport.AccpInstContext.ftNumber> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
            iAccpInstContext<PP.ClearingStatusReport.AccpInstContext.accpCode> = iFileData<PP.InwardMappingFramework.FileDataObject.originatingSource>
            PP.ClearingStatusReport.accpProcessingForInstPymt(iFileData, iBulkData, oMessageData, iAccpInstContext, oCSRMappingResponse)
        END
        RETURN
    END
* For Cheques Clearing
    IF (iFileData<PP.InwardMappingFramework.FileDataObject.queueName> MATCHES 'LCLG':@VM:'OCLG':@VM:'FCLG') OR (iFileData<PP.InwardMappingFramework.FileDataObject.typeIndicator> EQ "CR") THEN
        companyID = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
        ftNumber = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
        
        GOSUB getPORPaymentFlowDetails
* If the Status is already updated by other modes like Auto Clear or Manually through Enquiry then Return
        IF oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.chequeStatus> EQ 'CLEARED' OR oPORPmtFlowDetailsList<PP.PaymentFrameworkService.PORPmtFlowDetailsList.chequeStatus> EQ 'RETURNED' THEN
            RETURN
        END
    
        iChequeDetails = ''
        iChequeDetails<ChqTxnDetails.processingCompany> = companyID
        iChequeDetails<ChqTxnDetails.ftNumber>  = ftNumber
        iChequeDetails<ChqTxnDetails.chequeNumber>  = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglInstrId>
    
        iChequeStatus = ''
        BEGIN CASE
            CASE oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxSts> EQ "ACCP" AND iFileData<PP.InwardMappingFramework.FileDataObject.typeIndicator> EQ "CR"
                chequeStatus = "ACCEPTED"
            CASE oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxSts> EQ "ACCP"
                chequeStatus = 'CLEARED'
            CASE oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxSts> EQ "RJCT"
                chequeStatus = 'RETURNED'
                iFTNumber = ftNumber
                GOSUB updateClearingReturnCode
        END CASE
        ioChequeStatus<ChqRequestStatus.chequeStatus> = chequeStatus
        
        PP.PaymentWorkflowDASService.updateStatusFromChequeCollection(iChequeDetails, ioChequeStatus, oPaymentStatusUpdateError)
        IF oPaymentStatusUpdateError THEN
            messageType = "FATAL_ERROR"
            messageText = ""
            messageCode = oPaymentStatusUpdateError<PP.OrderEntryRepairService.PaymentResponse.responseMessages, 1, PP.OrderEntryRepairService.ResponseMessage.messageCode>
            messageInfo = ""
            GOSUB updateResponse
            GOSUB programExit
        END ELSE
            GOSUB updateOEStatus
        END
        RETURN
    END
    IF iFileData<PP.InwardMappingFramework.FileDataObject.hdrFileType> EQ 'PCF' THEN
        GOSUB setProcessingDate
        iReturnTransaction = ""
        iReturnTransaction<PP.PaymentReturn.RtCompanyID> = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
        iReturnTransaction<PP.PaymentReturn.RtTransactionReferenceIncoming> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
        iReturnTransaction<PP.PaymentReturn.RtClearingTransactionType> = "RT"
        iReturnTransaction<PP.PaymentReturn.RtIncomingMessageType> = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkFormat>
        iReturnTransaction<PP.PaymentReturn.RtReturnReasonCode> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnProp>
        iReturnTransaction<PP.PaymentReturn.RtReturnReasonAddInfo> = ''
        iReturnTransaction<PP.PaymentReturn.RtRequestedExecutionDate> = currBusinessDate
        iReturnTransaction<PP.PaymentReturn.RtRequestedCreditValueDate> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlDt>
        iReturnTransaction<PP.PaymentReturn.RtRequestedCreditValueDateImpFlag> = 'Y'
        iReturnTransaction<PP.PaymentReturn.RtDebitValueDate> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlDt>
        iReturnTransaction<PP.PaymentReturn.RtDebitValueDateImpFlag> = 'Y'
        iReturnTransaction<PP.PaymentReturn.RtHdrFileType> = iFileData<PP.InwardMappingFramework.FileDataObject.hdrFileType>
        oReturnFtNumber = "";oCreateResponse = ""
        PP.PaymentReturn.createReturnTransaction(iReturnTransaction, oReturnFtNumber, oCreateResponse)
        IF oCreateResponse NE '' THEN
            oReverseResponse = oCreateResponse
        END
        RETURN
    END
* No need to get Clearing Setting record with MessageDirection as S for EBAINST RSF files, so skipping it when Type Indicator is set to C
    IF iFileData<PP.InwardMappingFramework.FileDataObject.typeIndicator> NE 'C' THEN
    
        iClearingSetting<ClearingSettingRequest.companyID> = iCompanyId
        iClearingSetting<ClearingSettingRequest.clearingID> = iFileData<PP.InwardMappingFramework.FileDataObject.originatingSource>
        iClearingSetting<ClearingSettingRequest.messageDirection> = 'S'
        iClearingSetting<ClearingSettingRequest.clearingCurrency> = iBulkData<PP.InwardMappingFramework.BulkDataObject.grpHdrTotItbkSttlAmCcy>
;*Since message payment type from the file must be trimmed to 8 characters as camt.056 and not camt.056.001.01
        iClearingSetting<ClearingSettingRequest.messagePaymentType> = iOrglFormatLower
        iClearingSetting<ClearingSettingRequest.clearingNatureCode> = "*"
        iClearingSetting<ClearingSettingRequest.clearingTransactionType> = "*"

        CALL LocalClearingService.getClearingSetting(iClearingSetting, oClearingSetting, oClearingSettingErr)
    
    END
    
    IF oClearingSettingErr NE "" THEN
        messageType = "FATAL_ERROR"
        messageText = ""
        messageCode = "CST00002"
        messageInfo = iClearingSetting<ClearingSettingRequest.companyID>:' / ':iClearingSetting<ClearingSettingRequest.clearingID>:' / ':iClearingSetting<ClearingSettingRequest.clearingCurrency>:' / ':iClearingSetting<ClearingSettingRequest.messageDirection>:' / ':iClearingSetting<ClearingSettingRequest.messagePaymentType>:' / ':iClearingSetting<ClearingSettingRequest.clearingNatureCode>
        GOSUB updateResponse
        GOSUB programExit
    END ELSE

* determine the action to be taken further
        clearingSettingRecord = oClearingSetting
        GOSUB determineAction

    END

   
*
RETURN
*-----------------------------------------------------------------------------
getPaymentRecord:
*   In this GOSUB get the Payment Record
    oPaymentOrder = ""
    oReadErr = ""
    iPaymentID = ""
    oAdditionalPaymentRecord = ""
   
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.companyID> = iFTNumber[1,3]
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.ftNumber>  = iFTNumber
    iPaymentID<PP.PaymentWorkflowDASService.PaymentID.lockFlag>  = 'Y'
       
    PP.PaymentWorkflowDASService.getPaymentRecord(iPaymentID,oPaymentOrder,oAdditionalPaymentRecord,oReadErr)
RETURN
*-----------------------------------------------------------------------------
getPORTranConcat:
*   In this GOSUB, Process the confirmation for the payment that was sent out.
    FN.POR.TRANSACTION.CONCAT = 'F.POR.TRANSACTION.CONCAT'
    F.POR.TRANSACTION.CONCAT = ''
    R.TRANSACTION.CONCAT = ''
    ERR.CONCAT = ''

    IDVAL = iOrgnlTrxnId:'-':iOriginatingSource
    PP.InwardMappingFramework.getPORTransactionConcat(IDVAL, R.TRANSACTION.CONCAT, ERR.CONCAT)
RETURN
*------------------------------------------------------------------------------
determineAction:
*
    iClearingSetting = clearingSettingRecord
    iLevel = "T"
    skipProcessing = 'NO'
    IF iOrglFormat EQ "PACS.004" THEN ;* To get the original FTNumber for Incoming pacs.004 if we are processing incoming pacs.002 from clearing for Incoming pacs.004
        iOrgnlTrxnId = iParseData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
        iOriginatingSource = iFileData<PP.InwardMappingFramework.FileDataObject.originatingSource>
        GOSUB getPORTranConcat
        BEGIN CASE
            CASE R.TRANSACTION.CONCAT<2> NE ""
                iFTNumber = R.TRANSACTION.CONCAT<2> ;* if POR.TRANSACTION.CONCAT is having multiple records with same SendersReferenceIncoming and OriginatingSource
            CASE R.TRANSACTION.CONCAT<1> NE ""
                iFTNumber = R.TRANSACTION.CONCAT<1> ;* if POR.TRANSACTION.CONCAT is having  record with same SendersReferenceIncoming and OriginatingSource
            CASE 1
                iFTNumber = iOrgnlTrxnId
        END CASE
        GOSUB getPaymentRecord
        IF oPaymentOrder NE "" AND oPaymentOrder<PP.PaymentWorkflowDASService.PaymentRecord.paymentDirection> EQ 'I' THEN ;* If Original Payment is Incoming
            BEGIN CASE
                CASE oPaymentOrder<PP.PaymentWorkflowDASService.PaymentRecord.statusCode> EQ '656' ;* If Original Payment is incoming and it is awaiting confirmation from clearing.
                    ioMessageData<PP.InwardMappingFramework.MessageDataObject.ftNumber> = iFTNumber
                CASE oPaymentOrder<PP.PaymentWorkflowDASService.PaymentRecord.statusCode> LT '656'
                    fnTableName = 'F.PP.INVST.FILE'
                    oRecord = ''
                    eventDescription = ''
                    iReasonCodeForRejection = ''
                    IF iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCgStsReportReasonCd> THEN
                        iReasonCodeForRejection = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCgStsReportReasonCd>
                    END ELSE
                        iReasonCodeForRejection = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCgStsReportReasonProp>
                    END
                    oRecord<PP.INV.StatusAcceptanceCode> = 'RJCT'
                    oRecord<PP.INV.ReasonCodeForRejection> = iReasonCodeForRejection
                    oRecord<PP.INV.AdditionalInfo> = 'Negative confirmation received from clearing'
*                   Insert into PP.INVST.FILE with StatusAcceptanceCode so that while putting the payment into 656,
*                   this condition will be checked directly and processed accordingly
                    CALL F.WRITE(fnTableName,iFTNumber,oRecord)
                    eventDescription = 'Negative confirmation received from clearing'
                    GOSUB insertHistoryLog ;* Insert into HISTORYLOG
                    skipProcessing = 'YES' ;* If Clearing Status Report is received before payment is awaiting confirmation from clearing.
                CASE 1                     ;*oPaymentOrder<PP.PaymentWorkflowDASService.PaymentRecord.statusCode> MATCHES '997':@VM:'998':@VM:'999'
                    eventDescription = 'Duplicate Negative confirmation received from clearing'
                    GOSUB insertHistoryLog ;* Insert into HISTORYLOG
                    skipProcessing = 'YES' ;* If Clearing Status Report is received before payment is awaiting confirmation from clearing.
            END CASE
        END
    END
    IF ioMessageData<PP.InwardMappingFramework.MessageDataObject.ftNumber> EQ "" THEN
        ioMessageData<PP.InwardMappingFramework.MessageDataObject.ftNumber> = iParseData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
    END
    IF skipProcessing EQ 'NO' THEN
        PP.ClearingStatusReport.DetermineActionType(iClearingSetting, iFileData, iBulkData,iParseData, ioMessageData,iLevel, oActionType, oClearingReportResponse)
        IF oClearingReportResponse NE '' THEN
            oCSRMappingResponse = oClearingReportResponse
            GOSUB programExit
        END
    END
*
RETURN
*------------------------------------------------------------------------------
updateOEStatus:
* Update the Status in PP.ORDER.ENTRY
    ioPaymentDetails<PP.OrderEntryRepairService.PaymentDetails.processCompany> = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
    ioPaymentDetails<PP.OrderEntryRepairService.PaymentDetails.transactionReferenceNumber> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId>
    PP.OrderEntryRepairService.createOrderEntry(ioPaymentDetails,oOrderEntryResponse)
    
RETURN
*------------------------------------------------------------------------------
setProcessingDate:
    currBusinessDate = ''
    iCompanyKey = ''
    iCompanyKey = iBulkData<PP.InwardMappingFramework.BulkDataObject.bulkCompanyId>
    PP.PaymentFrameworkService.getCurrBusinessDate(iCompanyKey, oBusinessDate, oGetCurDateError)
    currBusinessDate = oBusinessDate<PP.PaymentFrameworkService.BusinessDate.currBusinessDate>
RETURN
*-----------------------------------------------------------------------------
getPORPaymentFlowDetails:

    iPORPmtFlowDetailsReq = ''; oPORPmtFlowDetailsList = ''; oPORPmtFlowDetailsGetError = ''
*
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.companyID> = companyID
    iPORPmtFlowDetailsReq<PP.PaymentFrameworkService.PORPmtFlowDetailsReq.ftNumber> = ftNumber
*
    PP.PaymentFrameworkService.getPORPaymentFlowDetails(iPORPmtFlowDetailsReq, oPORPmtFlowDetailsList, oPORPmtFlowDetailsGetError)  ;* To read POR.PAYMENTFLOWDETAILS table
  
RETURN
*----------------------------------------------------------------------------------
updateClearingReturnCode:

    GOSUB getPaymentRecord

    iServiceName = 'InwardMappingFramework'
    iPaymentRecord = oPaymentOrder
    iPaymentRecord<PP.PaymentWorkflowDASService.PaymentRecord.clearingReturnCode> = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnProp>
    iAdditionalPaymentRecord = oAdditionalPaymentRecord

    oUpdateErr = ''
    PP.PaymentWorkflowDASService.updatePaymentRecord (iServiceName, iPaymentRecord, iAdditionalPaymentRecord, oUpdateErr) ;* Update the Payment Record

RETURN
*----------------------------------------------------------------------------------
updateResponse:
* Update output response
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.returnCode> = 'FAILURE'
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.serviceName>      = 'InwardMappingFramework.mapClearingStatusReport'
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.responseMessages,1,PP.ClearingStatusReport.ResponseMessage.messageType> = messageType
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.responseMessages,1,PP.ClearingStatusReport.ResponseMessage.messageText> = messageText
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.responseMessages,1,PP.ClearingStatusReport.ResponseMessage.messageCode> = messageCode
    oCSRMappingResponse<PP.ClearingStatusReport.PaymentResponse.responseMessages,1,PP.ClearingStatusReport.ResponseMessage.messageInfo> = messageInfo
RETURN
*-----------------------------------------------------------------------------
insertHistoryLog:
*** <desc>Insert in the POR_HistoryLog table </desc>

    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.companyID> = iFTNumber[1,3]
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.ftNumber> = iFTNumber
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventType> = 'INF'
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.eventDescription> = eventDescription
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.errorCode> = ''
    iPORHistoryLog<PP.PaymentFrameworkService.PORHistoryLog.additionalInfo> = "StatusCode :":oPaymentOrder<PP.PaymentWorkflowDASService.PaymentRecord.statusCode>
    PP.PaymentFrameworkService.insertPORHistoryLog(iPORHistoryLog, oPORHistoryLogError)

RETURN
*------------------------------------------------------------------------------
*** <region name= getPaymentMethod>
getPaymentMethod:
*** <desc>Check the local instrument code and get the payment method </desc>

    codePosition = ''
    paymentMethod = ''
* Check whether the given payment is Instant or Near Real Instant payment based on the message mapping parameter.
* If it is then copy the value in the POR_SUPPLEMENTARY_INFO and update it in paymentMethod field.
    IF oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrCd> THEN
* Check whether local instrument code has any value and get the value for the CT request.
        paymentMethod = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrCd>
    END ELSE
* Now check whether proprietary value given in the CT request.
        IF oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrPrtry> THEN
            paymentMethod = oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrPrtry>
        END
    END

* Get the instant payment method for that local instrument code given in the CT request.
* If the local instrument code has value as INST or NRINST then the payment is already instant payment. So continue next step.
* Else check the local instrument code in the PP_MSGMAPPINGPARAMETER table and get the corresponding instant payment method.
    IF paymentMethod AND (paymentMethod NE "INST") AND (paymentMethod NE "NRINST") THEN
* Now we have other local instrument code, so get the instant payment method from the message mapping parameter.
        LOCATE paymentMethod IN ioMessageData<PP.InwardMappingFramework.MessageDataObject.instantLocalInstrumentCode,1> SETTING codePosition THEN
            paymentMethod = ioMessageData<PP.InwardMappingFramework.MessageDataObject.instantPaymentMethod,codePosition>
        END
    END

    
RETURN
*** </region>
*------------------------------------------------------------------------------

programExit:
*
RETURN TO programExit
*------------------------------------------------------------------------------
END
    
