<?xml version="1.0" encoding="UTF-8"?>
<!--  29 Jul 2019 - Task - Added missing mapping -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:vmf="http://www.altova.com/MapForce/UDF/vmf"
	xmlns:ns0="http://www.temenos.com/T24/event/Common/EventCommon" xmlns:ns1="http://www.temenos.com/T24/LocalClearingService/PaymentDetailsA"
	xmlns:ns2="http://www.temenos.com/T24/LocalClearingService/PorTransaction" xmlns:ns3="http://www.temenos.com/T24/LocalClearingService/PorPartyCredit"
	xmlns:ns4="http://www.temenos.com/T24/LocalClearingService/PorPartyDebit" xmlns:ns5="http://www.temenos.com/T24/LocalClearingService/PorInformation"
	xmlns:ns6="http://www.temenos.com/T24/LocalClearingService/PorAdditionalInf" xmlns:ns7="http://www.temenos.com/T24/LocalClearingService/PorAccountInfo"
	xmlns:ns8="http://www.temenos.com/T24/LocalClearingService/PorRemittanceInfoPart1" xmlns:ns9="http://www.temenos.com/T24/LocalClearingService/PorRemittanceInfoPart2"
	xmlns:ns10="http://www.temenos.com/T24/LocalClearingService/PpCanReq" xmlns:ns11="http://www.temenos.com/T24/event/OutwardMappingIF/doOutwardMappingFlow"
	xmlns:ns12="http://www.temenos.com/T24/LocalClearingService/PorExtentedRemittanceInfo"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" version="1.0" exclude-result-prefixes="vmf ns0 ns1 ns2 ns3 ns4 ns5 ns6 ns7 ns8 ns9 ns10 ns11 ns12 xs">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />
	<xsl:strip-space elements="*" />
	<xsl:template match="/">
		<xsl:variable name="var1_initial" select="." />
		<Document xmlns="urn:iso:std:iso:20022:tech:xsd:pacs.028.001.01">
			<xsl:attribute name="xsi:schemaLocation" namespace="http://www.w3.org/2001/XMLSchema-instance">urn:iso:std:iso:20022:tech:xsd:pacs.028.001.01 pacs.028.001.01.xsd</xsl:attribute>
			<FIToFIPmtStsReq>
				<GrpHdr>
					<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:bulkReference">
						<MsgId>
							<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:bulkReference" />
						</MsgId>
					</xsl:if>
					<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime">
						<CreDtTm>
							<xsl:value-of
								select="concat(substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '1', '4'), '-', substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '5', '2'), '-', substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '7', '2'), 'T', substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '9', '2'), ':', substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '11', '2'), ':', substring(string(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:createdDateTime), '13', '2'), '.0Z')" />
						</CreDtTm>
					</xsl:if>
					<InstgAgt>
						<FinInstnId>
							<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:companyBIC">
								<BICFI>
									<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:companyBIC" />
								</BICFI>
							</xsl:if>
						</FinInstnId>
					</InstgAgt>
					<InstdAgt>
						<FinInstnId>
							<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:clearingBIC">
								<BICFI>
									<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:clearingBIC" />
								</BICFI>
							</xsl:if>
						</FinInstnId>
					</InstdAgt>
				</GrpHdr>
				<OrgnlGrpInf>
					<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:bulkReferenceOutgoing">
						<OrgnlMsgId>
							<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:bulkReferenceOutgoing" />
						</OrgnlMsgId>
					</xsl:if>
					<OrgnlMsgNmId>
						<xsl:choose>
							<xsl:when test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:originalMessageType">
								<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:originalMessageType" />
							</xsl:when>
                            <xsl:otherwise>camt.056</xsl:otherwise>
						</xsl:choose>
					</OrgnlMsgNmId>
				</OrgnlGrpInf>
				<TxInf>
					<xsl:if test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqInvestigationId">
						<StsReqId>
							<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqInvestigationId" />
						</StsReqId>
					</xsl:if>
					<xsl:if
						test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqCancelReqId or ns11:doOutwardMappingFlow/ns11:icanreq/ns10:caseId">
						<OrgnlInstrId>
							<xsl:choose>
								<xsl:when test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:originalMessageType = 'camt.056'">
									<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqCancelReqId" />
								</xsl:when>
								<xsl:when test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:originalMessageType = 'camt.027'">
									<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:caseId" />
								</xsl:when>
								<xsl:when test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:originalMessageType = 'camt.087'">
									<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:caseId" />
								</xsl:when>
							</xsl:choose>
						</OrgnlInstrId>
					</xsl:if>
					<OrgnlEndToEndId>
						<xsl:choose>
							<xsl:when test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:customerSpecifiedReference">
								<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:customerSpecifiedReference" />
							</xsl:when>
                            <xsl:otherwise>NOTPROVIDED</xsl:otherwise>
						</xsl:choose>
					</OrgnlEndToEndId>
					<xsl:if test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqRelRef">
						<OrgnlTxId>
							<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:canReqRelRef" />
						</OrgnlTxId>
					</xsl:if>
					<OrgnlTxRef>
						<xsl:if test="ns11:doOutwardMappingFlow/ns11:icanreq/ns10:settlementDate">
							<IntrBkSttlmDt>
								<xsl:value-of
									select="concat(substring(string(ns11:doOutwardMappingFlow/ns11:icanreq/ns10:settlementDate), '1', '4'), '-', substring(string(ns11:doOutwardMappingFlow/ns11:icanreq/ns10:settlementDate), '5', '2'), '-', substring(string(ns11:doOutwardMappingFlow/ns11:icanreq/ns10:settlementDate), '7', '2'))" />
							</IntrBkSttlmDt>
						</xsl:if>
						<SttlmInf>
							<SttlmMtd>CLRG</SttlmMtd>
							<ClrSys>
								<Prtry>ST2</Prtry>
							</ClrSys>
						</SttlmInf>
						<PmtTpInf>
							<SvcLvl>
								<Cd>SEPA</Cd>
							</SvcLvl>

							<xsl:variable name="isLclHasValue">
								<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:iporinformation[ns5:informationCode = 'INSBNK']">
									<xsl:if test="ns5:instructionCode = 'LCLINSCD'">
										<xsl:value-of select="'TRUE'" />
									</xsl:if>
								</xsl:for-each>
							</xsl:variable>
							<LclInstrm>
								<Cd>
									<xsl:choose>
										<xsl:when test="$isLclHasValue = 'TRUE'">
											<xsl:for-each
												select="ns11:doOutwardMappingFlow/ns11:iporinformation[ns5:informationCode = 'INSBNK' and ns5:instructionCode = 'LCLINSCD']">
												<xsl:if test="ns5:informationLine">
													<xsl:value-of select="ns5:informationLine" />
												</xsl:if>
											</xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'TRF'" />
										</xsl:otherwise>
									</xsl:choose>
								</Cd>
							</LclInstrm>

							<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:iporinformation[ns5:informationCode = 'INSBNK']">
								<xsl:if test="(ns5:instructionCode = 'CYPURPCD' or ns5:instructionCode = 'CYPURPPY') and ns5:informationLine">
									<CtgyPurp>
										<xsl:if test="ns5:instructionCode = 'CYPURPCD' and ns5:informationLine">
											<Cd>
												<xsl:value-of select="ns5:informationLine" />
											</Cd>
										</xsl:if>
										<xsl:if test="ns5:instructionCode = 'CYPURPPY' and ns5:informationLine">
											<Prtry>
												<xsl:value-of select="ns5:informationLine" />
											</Prtry>
										</xsl:if>
									</CtgyPurp>
								</xsl:if>
							</xsl:for-each>
						</PmtTpInf>
						<xsl:variable name="PERIFlag">
							<xsl:for-each
								select="ns11:doOutwardMappingFlow/ns11:iporinformation[ns5:informationCode = 'INSBNK' and ns5:instructionCode = 'LCLINSCD']">
								<xsl:if test="ns5:informationLine = 'PERI'">
									<xsl:value-of select="'TRUE'" />
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:variable name="RMTINFValue">
							<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:iadditionalinf[ns6:additionalInformationCode = 'RMTINF']">
								<xsl:if test="ns6:additionalInfLine">
									<Ustrd>
										<xsl:value-of select="ns6:additionalInfLine" />
									</Ustrd>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd">
								<xsl:if
									test="($RMTINFValue != '') or (($RMTINFValue = '') and ($PERIFlag != 'TRUE') and
                                            ((ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd) or 
                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpIssuer) or
                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfRef)))">
									<RmtInf>
										<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo">
											<xsl:if test="$RMTINFValue != ''">
												<Ustrd>
													<xsl:value-of select="$RMTINFValue" />
												</Ustrd>
											</xsl:if>
											<xsl:if
												test="($RMTINFValue = '') and ($PERIFlag != 'TRUE') and 
                                            ((ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd) or 
                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpIssuer) or
                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropProp) or
                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfRef))">
												<Strd>
													<CdtrRefInf>
														<xsl:if
															test=" (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd) or 
															(ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropProp) or 
                                                            (ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpIssuer)">
															<Tp>
																<xsl:choose>
																    <xsl:when test="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd = 'SCOR'">
                                                                        <CdOrPrtry>
                                                                            <Prtry>
                                                                                <xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropProp" />
                                                                            </Prtry>
                                                                        </CdOrPrtry>
                                                                    </xsl:when>
																	<xsl:when test="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd = 'SCOR'">
																		<CdOrPrtry>
																			<Cd>
																				<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpCdOrPropCd" />
																			</Cd>
																		</CdOrPrtry>
																	</xsl:when>
																	<xsl:otherwise>
																		<CdOrPrtry>
																			<Cd>SCOR</Cd>
																		</CdOrPrtry>
																	</xsl:otherwise>
																</xsl:choose>
																<xsl:if test="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpIssuer">
																	<Issr>
																		<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfTpIssuer" />
																	</Issr>
																</xsl:if>
															</Tp>
														</xsl:if>
														<xsl:if test="ns11:doOutwardMappingFlow/ns11:iextentedremittanceinfo/ns12:crdRefInfRef">
															<Ref>
																<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns12:crdRefInfRef" />
															</Ref>
														</xsl:if>
													</CdtrRefInf>
												</Strd>
											</xsl:if>
										</xsl:for-each>
									</RmtInf>
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if
									test="($RMTINFValue != '') or (($RMTINFValue = '') and ($PERIFlag != 'TRUE') and
                                            ((ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropCd) or 
                                            (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpIssuer) or
                                            (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfRef)))">
									<RmtInf>
										<xsl:if test="$RMTINFValue != ''">
											<Ustrd>
												<xsl:value-of select="$RMTINFValue" />
											</Ustrd>
										</xsl:if>
										<xsl:if
											test="($RMTINFValue = '') and ($PERIFlag != 'TRUE') and 
                                            ((ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropCd) or 
                                            (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpIssuer) or
                                            (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropProp) or
                                            (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfRef))">
											<Strd>
												<CdtrRefInf>
													<xsl:if
														test=" (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropCd) or
														(ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropProp) or 
                                                        (ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpIssuer)">
														<Tp>
															<xsl:choose>
															 <xsl:when test="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropProp">
                                                                    <CdOrPrtry>
                                                                        <Prtry>
                                                                            <xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropProp" />
                                                                        </Prtry>
                                                                    </CdOrPrtry>
                                                                </xsl:when>
																<xsl:when test="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropCd = 'SCOR'">
																	<CdOrPrtry>
																		<Cd>
																			<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpCdOrPropCd" />
																		</Cd>
																	</CdOrPrtry>
																</xsl:when>
																<xsl:otherwise>
																	<CdOrPrtry>
																		<Cd>SCOR</Cd>
																	</CdOrPrtry>
																</xsl:otherwise>
															</xsl:choose>
															<xsl:if test="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpIssuer">
																<Issr>
																	<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfTpIssuer" />
																</Issr>
															</xsl:if>
														</Tp>
													</xsl:if>
													<xsl:if test="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfRef">
														<Ref>
															<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iremittanceinfopart1/ns8:crdRefInfRef" />
														</Ref>
													</xsl:if>
												</CdtrRefInf>
											</Strd>
										</xsl:if>
									</RmtInf>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:for-each
							select="ns11:doOutwardMappingFlow/ns11:ipartydebit[ns4:dbPtyRole = 'ULTDBT' and ns4:dbPtyRoleIndicator = 'R']">
							<UltmtDbtr>
								<xsl:if test="ns4:dbPtyName">
									<Nm>
										<xsl:value-of select="ns4:dbPtyName" />
									</Nm>
								</xsl:if>
								<xsl:if
									test="(ns4:dbPtyIdentifierCode or ns4:dbPtyOrgIdOtherId) or ns4:dbPtyBirthDate or ns4:dbPtyProvinceOfBirth or ns4:dbPtyCityOfBirth or ns4:dbPtyCountryOfBirth or ns4:dbPtyOrgIdOtherId or ns4:dbPtyPrvIdOtherId or ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp or ns4:dbPtyPrvIdOtherIssuer ">
									<Id>
										<xsl:choose>
											<xsl:when test="(ns4:dbPtyIdentifierCode) or (ns4:dbPtyOrgIdOtherId)">
												<xsl:if test="(ns4:dbPtyIdentifierCode) or (ns4:dbPtyOrgIdOtherId)">
													<OrgId>
														<xsl:choose>
															<xsl:when test="ns4:dbPtyIdentifierCode">
																<AnyBIC>
																	<xsl:value-of select="ns4:dbPtyIdentifierCode" />
																</AnyBIC>
															</xsl:when>
															<xsl:otherwise>
																<Othr>
																	<xsl:if test="ns4:dbPtyOrgIdOtherId">
																		<Id>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns4:dbPtyOrgIdOtherId, '/')">
																					<xsl:value-of select="substring-after(ns4:dbPtyOrgIdOtherId, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns4:dbPtyOrgIdOtherId" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</Id>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyOrgIdOtherSchCode or ns4:dbPtyOrgIdOtherSchProp">
																		<SchmeNm>
																			<xsl:if test="ns4:dbPtyOrgIdOtherSchCode">
																				<Cd>
																					<xsl:value-of select="ns4:dbPtyOrgIdOtherSchCode" />
																				</Cd>
																			</xsl:if>
																			<xsl:if test="ns4:dbPtyOrgIdOtherSchProp">
																				<Prtry>
																					<xsl:value-of select="ns4:dbPtyOrgIdOtherSchProp" />
																				</Prtry>
																			</xsl:if>
																		</SchmeNm>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyOrgIdOtherIssuer">
																		<Issr>
																			<xsl:value-of select="ns4:dbPtyOrgIdOtherIssuer" />
																		</Issr>
																	</xsl:if>
																</Othr>
															</xsl:otherwise>
														</xsl:choose>
													</OrgId>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if
													test="ns4:dbPtyBirthDate or ns4:dbPtyProvinceOfBirth or ns4:dbPtyCityOfBirth or ns4:dbPtyCountryOfBirth or ns4:dbPtyPrvIdOtherId">
													<PrvtId>
														<xsl:choose>
															<xsl:when
																test="ns4:dbPtyBirthDate or ns4:dbPtyProvinceOfBirth or ns4:dbPtyCityOfBirth or ns4:dbPtyCountryOfBirth">
																<DtAndPlcOfBirth>
																	<xsl:if test="ns4:dbPtyBirthDate">
																		<BirthDt>
																			<xsl:value-of
																				select="concat(substring(string(ns4:dbPtyBirthDate), '1', '4'), '-', substring(string(ns4:dbPtyBirthDate), '5', '2'), '-', substring(string(ns4:dbPtyBirthDate), '7', '2'))" />
																		</BirthDt>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyProvinceOfBirth">
																		<PrvcOfBirth>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns4:dbPtyProvinceOfBirth, '/')">
																					<xsl:value-of select="substring-after(ns4:dbPtyProvinceOfBirth, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns4:dbPtyProvinceOfBirth" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</PrvcOfBirth>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyCityOfBirth">
																		<CityOfBirth>
																			<xsl:value-of select="ns4:dbPtyCityOfBirth" />
																		</CityOfBirth>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyCountryOfBirth">
																		<CtryOfBirth>
																			<xsl:value-of select="ns4:dbPtyCountryOfBirth" />
																		</CtryOfBirth>
																	</xsl:if>
																</DtAndPlcOfBirth>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if
																	test="(ns4:dbPtyOrgIdOtherId) != 'true' or (ns4:dbPtyPrvIdOtherId or ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp or ns4:dbPtyPrvIdOtherIssuer)">
																	<Othr>
																		<xsl:if test="ns4:dbPtyPrvIdOtherId">
																			<Id>
																				<xsl:value-of select="ns4:dbPtyPrvIdOtherId" />
																			</Id>
																		</xsl:if>
																		<xsl:if test="ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp">
																			<SchmeNm>
																				<xsl:if test="ns4:dbPtyPrvIdOtherSchCode">
																					<Cd>
																						<xsl:value-of select="ns4:dbPtyPrvIdOtherSchCode" />
																					</Cd>
																				</xsl:if>
																				<xsl:if test="ns4:dbPtyPrvIdOtherSchProp">
																					<Prtry>
																						<xsl:choose>
																							<xsl:when test="starts-with(ns4:dbPtyPrvIdOtherSchProp, '/')">
																								<xsl:value-of select="substring-after(ns4:dbPtyPrvIdOtherSchProp, '/')" />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="ns4:dbPtyPrvIdOtherSchProp" />
																							</xsl:otherwise>
																						</xsl:choose>
																					</Prtry>
																				</xsl:if>
																			</SchmeNm>
																		</xsl:if>
																		<xsl:if test="ns4:dbPtyPrvIdOtherIssuer">
																			<Issr>
																				<xsl:value-of select="ns4:dbPtyPrvIdOtherIssuer" />
																			</Issr>
																		</xsl:if>
																	</Othr>
																</xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</PrvtId>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</Id>
								</xsl:if>
							</UltmtDbtr>
						</xsl:for-each>
						<Dbtr>
							<xsl:for-each select="ns11:doOutwardMappingFlow">
								<xsl:variable name="var46_cur" select="." />
								<xsl:for-each select="ns11:iportransaction">
									<xsl:variable name="var47_cur" select="." />
									<xsl:for-each select="ns2:paymentDirection">
										<xsl:variable name="var48_cur" select="." />
										<xsl:for-each select="$var47_cur/ns2:singleMultipleIndicator">
											<xsl:variable name="var49_cur" select="." />
											<xsl:variable name="var50_nested">
												<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction">
													<xsl:variable name="var51_cur" select="." />
													<xsl:value-of select="number(boolean(ns2:orderingPartyTagOptions))" />
												</xsl:for-each>
											</xsl:variable>
											<xsl:choose>
												<xsl:when test="boolean(translate(normalize-space($var50_nested), ' 0', ''))">
													<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions">
														<xsl:variable name="var52_cur" select="." />
														<xsl:variable name="var53_nested">
															<xsl:choose>
																<xsl:when test="((($var48_cur = 'O') and ($var49_cur != 'B')) and (. != 'F'))">
																	<xsl:for-each select="$var46_cur/ns11:iaccountinfo">
																		<xsl:variable name="var54_cur" select="." />
																		<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																			<xsl:variable name="var55_filter" select="." />
																			<xsl:for-each select="$var54_cur/ns7:customerName">
																				<xsl:variable name="var56_cur" select="." />
																				<xsl:value-of select="'1'" />
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:for-each select="$var46_cur/ns11:ipartydebit">
																		<xsl:variable name="var57_cur" select="." />
																		<xsl:for-each select="ns4:dbPtyRole">
																			<xsl:variable name="var58_cur" select="." />
																			<xsl:for-each select="($var57_cur/ns4:dbPtyRoleIndicator)[(($var58_cur = 'ORDPTY') and (. = 'R'))]">
																				<xsl:variable name="var59_filter" select="." />
																				<xsl:for-each select="$var57_cur/ns4:dbPtyName">
																					<xsl:variable name="var60_cur" select="." />
																					<xsl:choose>
																						<xsl:when test="(. != &quot;''&quot;)">
																							<xsl:value-of select="'1'" />
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:for-each select="$var57_cur/ns4:dbPtyFreeLine1">
																								<xsl:variable name="var61_cur" select="." />
																								<xsl:value-of select="'1'" />
																							</xsl:for-each>
																						</xsl:otherwise>
																					</xsl:choose>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:variable>
														<xsl:if test="boolean(translate(normalize-space($var53_nested), ' 0', ''))">
															<Nm>
																<xsl:choose>
																	<xsl:when test="((($var48_cur = 'O') and ($var49_cur != 'B')) and (. != 'F'))">
																		<xsl:for-each select="$var46_cur/ns11:iaccountinfo">
																			<xsl:variable name="var62_cur" select="." />
																			<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																				<xsl:variable name="var63_filter" select="." />
																				<xsl:for-each select="$var62_cur/ns7:customerName">
																					<xsl:variable name="var64_cur" select="." />
																					<xsl:value-of select="." />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each select="$var46_cur/ns11:ipartydebit">
																			<xsl:variable name="var65_cur" select="." />
																			<xsl:for-each select="ns4:dbPtyRole">
																				<xsl:variable name="var66_cur" select="." />
																				<xsl:for-each select="($var65_cur/ns4:dbPtyRoleIndicator)[(($var66_cur = 'ORDPTY') and (. = 'R'))]">
																					<xsl:variable name="var67_filter" select="." />
																					<xsl:for-each select="$var65_cur/ns4:dbPtyName">
																						<xsl:variable name="var68_cur" select="." />
																						<xsl:choose>
																							<xsl:when test="(. != &quot;''&quot;)">
																								<xsl:value-of select="." />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:variable name="var69_nested">
																									<xsl:for-each select="$var65_cur/ns4:dbPtyFreeLine1">
																										<xsl:variable name="var70_cur" select="." />
																										<xsl:value-of select="." />
																									</xsl:for-each>
																								</xsl:variable>
																								<xsl:value-of select="$var69_nested" />
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:otherwise>
																</xsl:choose>
															</Nm>
														</xsl:if>
													</xsl:for-each>
												</xsl:when>
												<xsl:otherwise>
													<xsl:variable name="var71_nested">
														<xsl:choose>
															<xsl:when test="((($var48_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																<xsl:for-each select="$var46_cur/ns11:iaccountinfo">
																	<xsl:variable name="var72_cur" select="." />
																	<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																		<xsl:variable name="var73_filter" select="." />
																		<xsl:for-each select="$var72_cur/ns7:customerName">
																			<xsl:variable name="var74_cur" select="." />
																			<xsl:value-of select="'1'" />
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:when>
															<xsl:otherwise>
																<xsl:for-each select="$var46_cur/ns11:ipartydebit">
																	<xsl:variable name="var75_cur" select="." />
																	<xsl:for-each select="ns4:dbPtyRole">
																		<xsl:variable name="var76_cur" select="." />
																		<xsl:for-each select="($var75_cur/ns4:dbPtyRoleIndicator)[(($var76_cur = 'ORDPTY') and (. = 'R'))]">
																			<xsl:variable name="var77_filter" select="." />
																			<xsl:for-each select="$var75_cur/ns4:dbPtyName">
																				<xsl:variable name="var78_cur" select="." />
																				<xsl:choose>
																					<xsl:when test="(. != &quot;''&quot;)">
																						<xsl:value-of select="'1'" />
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:for-each select="$var75_cur/ns4:dbPtyFreeLine1">
																							<xsl:variable name="var79_cur" select="." />
																							<xsl:value-of select="'1'" />
																						</xsl:for-each>
																					</xsl:otherwise>
																				</xsl:choose>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:variable>
													<xsl:if test="boolean(translate(normalize-space($var71_nested), ' 0', ''))">
														<Nm>
															<xsl:choose>
																<xsl:when test="((($var48_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																	<xsl:for-each select="$var46_cur/ns11:iaccountinfo">
																		<xsl:variable name="var80_cur" select="." />
																		<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																			<xsl:variable name="var81_filter" select="." />
																			<xsl:for-each select="$var80_cur/ns7:customerName">
																				<xsl:variable name="var82_cur" select="." />
																				<xsl:value-of select="." />
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:for-each select="$var46_cur/ns11:ipartydebit">
																		<xsl:variable name="var83_cur" select="." />
																		<xsl:for-each select="ns4:dbPtyRole">
																			<xsl:variable name="var84_cur" select="." />
																			<xsl:for-each select="($var83_cur/ns4:dbPtyRoleIndicator)[(($var84_cur = 'ORDPTY') and (. = 'R'))]">
																				<xsl:variable name="var85_filter" select="." />
																				<xsl:for-each select="$var83_cur/ns4:dbPtyName">
																					<xsl:variable name="var86_cur" select="." />
																					<xsl:choose>
																						<xsl:when test="(. != &quot;''&quot;)">
																							<xsl:value-of select="." />
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:variable name="var87_nested">
																								<xsl:for-each select="$var83_cur/ns4:dbPtyFreeLine1">
																									<xsl:variable name="var88_cur" select="." />
																									<xsl:value-of select="." />
																								</xsl:for-each>
																							</xsl:variable>
																							<xsl:value-of select="$var87_nested" />
																						</xsl:otherwise>
																					</xsl:choose>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:otherwise>
															</xsl:choose>
														</Nm>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:for-each>
							<PstlAdr>
								<xsl:for-each select="ns11:doOutwardMappingFlow">
									<xsl:variable name="var89_cur" select="." />
									<xsl:for-each select="ns11:iportransaction">
										<xsl:variable name="var90_cur" select="." />
										<xsl:for-each select="ns2:paymentDirection">
											<xsl:variable name="var91_cur" select="." />
											<xsl:for-each select="$var90_cur/ns2:singleMultipleIndicator">
												<xsl:variable name="var92_cur" select="." />
												<xsl:variable name="var93_nested">
													<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction">
														<xsl:variable name="var94_cur" select="." />
														<xsl:value-of select="number(boolean(ns2:orderingPartyTagOptions))" />
													</xsl:for-each>
												</xsl:variable>
												<xsl:choose>
													<xsl:when test="boolean(translate(normalize-space($var93_nested), ' 0', ''))">
														<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions">
															<xsl:variable name="var95_cur" select="." />
															<xsl:variable name="var96_nested">
																<xsl:choose>
																	<xsl:when test="((($var91_cur = 'O') and ($var92_cur != 'B')) and (. != 'F'))">
																		<xsl:for-each select="$var89_cur/ns11:iaccountinfo">
																			<xsl:variable name="var97_cur" select="." />
																			<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																				<xsl:variable name="var98_filter" select="." />
																				<xsl:for-each select="$var97_cur/ns7:customerResidency">
																					<xsl:variable name="var99_cur" select="." />
																					<xsl:value-of select="'1'" />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each select="$var89_cur/ns11:ipartydebit">
																			<xsl:variable name="var100_cur" select="." />
																			<xsl:for-each select="ns4:dbPtyRole">
																				<xsl:variable name="var101_cur" select="." />
																				<xsl:for-each select="($var100_cur/ns4:dbPtyRoleIndicator)[(($var101_cur = 'ORDPTY') and (. = 'R'))]">
																					<xsl:variable name="var102_filter" select="." />
																					<xsl:for-each select="$var100_cur/ns4:dbPtyCountry">
																						<xsl:variable name="var103_cur" select="." />
																						<xsl:value-of select="'1'" />
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:otherwise>
																</xsl:choose>
															</xsl:variable>
															<xsl:if test="boolean(translate(normalize-space($var96_nested), ' 0', ''))">
																<Ctry>
																	<xsl:choose>
																		<xsl:when test="((($var91_cur = 'O') and ($var92_cur != 'B')) and (. != 'F'))">
																			<xsl:for-each select="$var89_cur/ns11:iaccountinfo">
																				<xsl:variable name="var104_cur" select="." />
																				<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																					<xsl:variable name="var105_filter" select="." />
																					<xsl:for-each select="$var104_cur/ns7:customerResidency">
																						<xsl:variable name="var106_cur" select="." />
																						<xsl:value-of select="." />
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:for-each select="$var89_cur/ns11:ipartydebit">
																				<xsl:variable name="var107_cur" select="." />
																				<xsl:for-each select="ns4:dbPtyRole">
																					<xsl:variable name="var108_cur" select="." />
																					<xsl:for-each select="($var107_cur/ns4:dbPtyRoleIndicator)[(($var108_cur = 'ORDPTY') and (. = 'R'))]">
																						<xsl:variable name="var109_filter" select="." />
																						<xsl:for-each select="$var107_cur/ns4:dbPtyCountry">
																							<xsl:variable name="var110_cur" select="." />
																							<xsl:value-of select="." />
																						</xsl:for-each>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:otherwise>
																	</xsl:choose>
																</Ctry>
															</xsl:if>
														</xsl:for-each>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="var111_nested">
															<xsl:choose>
																<xsl:when test="((($var91_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																	<xsl:for-each select="$var89_cur/ns11:iaccountinfo">
																		<xsl:variable name="var112_cur" select="." />
																		<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																			<xsl:variable name="var113_filter" select="." />
																			<xsl:for-each select="$var112_cur/ns7:customerResidency">
																				<xsl:variable name="var114_cur" select="." />
																				<xsl:value-of select="'1'" />
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:for-each select="$var89_cur/ns11:ipartydebit">
																		<xsl:variable name="var115_cur" select="." />
																		<xsl:for-each select="ns4:dbPtyRole">
																			<xsl:variable name="var116_cur" select="." />
																			<xsl:for-each select="($var115_cur/ns4:dbPtyRoleIndicator)[(($var116_cur = 'ORDPTY') and (. = 'R'))]">
																				<xsl:variable name="var117_filter" select="." />
																				<xsl:for-each select="$var115_cur/ns4:dbPtyCountry">
																					<xsl:variable name="var118_cur" select="." />
																					<xsl:value-of select="'1'" />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:variable>
														<xsl:if test="boolean(translate(normalize-space($var111_nested), ' 0', ''))">
															<Ctry>
																<xsl:choose>
																	<xsl:when test="((($var91_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																		<xsl:for-each select="$var89_cur/ns11:iaccountinfo">
																			<xsl:variable name="var119_cur" select="." />
																			<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																				<xsl:variable name="var120_filter" select="." />
																				<xsl:for-each select="$var119_cur/ns7:customerResidency">
																					<xsl:variable name="var121_cur" select="." />
																					<xsl:value-of select="." />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each select="$var89_cur/ns11:ipartydebit">
																			<xsl:variable name="var122_cur" select="." />
																			<xsl:for-each select="ns4:dbPtyRole">
																				<xsl:variable name="var123_cur" select="." />
																				<xsl:for-each select="($var122_cur/ns4:dbPtyRoleIndicator)[(($var123_cur = 'ORDPTY') and (. = 'R'))]">
																					<xsl:variable name="var124_filter" select="." />
																					<xsl:for-each select="$var122_cur/ns4:dbPtyCountry">
																						<xsl:variable name="var125_cur" select="." />
																						<xsl:value-of select="." />
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:otherwise>
																</xsl:choose>
															</Ctry>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
								<xsl:for-each select="ns11:doOutwardMappingFlow">
									<xsl:variable name="var126_cur" select="." />
									<xsl:for-each select="ns11:iportransaction">
										<xsl:variable name="var127_cur" select="." />
										<xsl:for-each select="ns2:paymentDirection">
											<xsl:variable name="var128_cur" select="." />
											<xsl:for-each select="$var127_cur/ns2:singleMultipleIndicator">
												<xsl:variable name="var129_cur" select="." />
												<xsl:variable name="var130_nested">
													<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction">
														<xsl:variable name="var131_cur" select="." />
														<xsl:value-of select="number(boolean(ns2:orderingPartyTagOptions))" />
													</xsl:for-each>
												</xsl:variable>
												<xsl:choose>
													<xsl:when test="boolean(translate(normalize-space($var130_nested), ' 0', ''))">
														<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions">
															<xsl:variable name="var132_cur" select="." />
															<xsl:variable name="var133_nested">
																<xsl:choose>
																	<xsl:when test="((($var128_cur = 'O') and ($var129_cur != 'B')) and (. != 'F'))">
																		<xsl:for-each select="$var126_cur/ns11:iaccountinfo">
																			<xsl:variable name="var134_cur" select="." />
																			<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																				<xsl:variable name="var135_filter" select="." />
																				<xsl:for-each select="$var134_cur/ns7:customerAddress">
																					<xsl:variable name="var136_cur" select="." />
																					<xsl:value-of select="'1'" />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each select="$var126_cur/ns11:ipartydebit">
																			<xsl:variable name="var137_cur" select="." />
																			<xsl:for-each select="ns4:dbPtyRole">
																				<xsl:variable name="var138_cur" select="." />
																				<xsl:for-each select="($var137_cur/ns4:dbPtyRoleIndicator)[(($var138_cur = 'ORDPTY') and (. = 'R'))]">
																					<xsl:variable name="var139_filter" select="." />
																					<xsl:for-each select="$var137_cur/ns4:dbPtyAddressLine1">
																						<xsl:variable name="var140_cur" select="." />
																						<xsl:choose>
																							<xsl:when test="(. != &quot;''&quot;)">
																								<xsl:value-of select="'1'" />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:for-each select="$var137_cur/ns4:dbPtyFreeLine2">
																									<xsl:variable name="var141_cur" select="." />
																									<xsl:for-each select="$var137_cur/ns4:dbPtyFreeLine3">
																										<xsl:variable name="var142_cur" select="." />
																										<xsl:value-of select="'1'" />
																									</xsl:for-each>
																								</xsl:for-each>
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:otherwise>
																</xsl:choose>
															</xsl:variable>
															<xsl:if test="boolean(translate(normalize-space($var133_nested), ' 0', ''))">
																<AdrLine>
																	<xsl:choose>
																		<xsl:when test="((($var128_cur = 'O') and ($var129_cur != 'B')) and (. != 'F'))">
																			<xsl:for-each select="$var126_cur/ns11:iaccountinfo">
																				<xsl:variable name="var143_cur" select="." />
																				<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																					<xsl:variable name="var144_filter" select="." />
																					<xsl:for-each select="$var143_cur/ns7:customerAddress">
																						<xsl:variable name="var145_cur" select="." />
																						<xsl:value-of select="." />
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:when>
																		<xsl:otherwise>
																			<xsl:for-each select="$var126_cur/ns11:ipartydebit">
																				<xsl:variable name="var146_cur" select="." />
																				<xsl:for-each select="ns4:dbPtyRole">
																					<xsl:variable name="var147_cur" select="." />
																					<xsl:for-each select="($var146_cur/ns4:dbPtyRoleIndicator)[(($var147_cur = 'ORDPTY') and (. = 'R'))]">
																						<xsl:variable name="var148_filter" select="." />
																						<xsl:for-each select="$var146_cur/ns4:dbPtyAddressLine1">
																							<xsl:variable name="var149_cur" select="." />
																							<xsl:choose>
																								<xsl:when test="(. != &quot;''&quot;)">
																									<xsl:value-of select="." />
																								</xsl:when>
																								<xsl:otherwise>
																									<xsl:variable name="var150_nested">
																										<xsl:for-each select="$var146_cur/ns4:dbPtyFreeLine2">
																											<xsl:variable name="var151_cur" select="." />
																											<xsl:for-each select="$var146_cur/ns4:dbPtyFreeLine3">
																												<xsl:variable name="var152_cur" select="." />
																												<xsl:value-of select="concat($var151_cur, .)" />
																											</xsl:for-each>
																										</xsl:for-each>
																									</xsl:variable>
																									<xsl:value-of select="$var150_nested" />
																								</xsl:otherwise>
																							</xsl:choose>
																						</xsl:for-each>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:otherwise>
																	</xsl:choose>
																</AdrLine>
															</xsl:if>
														</xsl:for-each>
													</xsl:when>
													<xsl:otherwise>
														<xsl:variable name="var153_nested">
															<xsl:choose>
																<xsl:when test="((($var128_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																	<xsl:for-each select="$var126_cur/ns11:iaccountinfo">
																		<xsl:variable name="var154_cur" select="." />
																		<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																			<xsl:variable name="var155_filter" select="." />
																			<xsl:for-each select="$var154_cur/ns7:customerAddress">
																				<xsl:variable name="var156_cur" select="." />
																				<xsl:value-of select="'1'" />
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:for-each select="$var126_cur/ns11:ipartydebit">
																		<xsl:variable name="var157_cur" select="." />
																		<xsl:for-each select="ns4:dbPtyRole">
																			<xsl:variable name="var158_cur" select="." />
																			<xsl:for-each select="($var157_cur/ns4:dbPtyRoleIndicator)[(($var158_cur = 'ORDPTY') and (. = 'R'))]">
																				<xsl:variable name="var159_filter" select="." />
																				<xsl:for-each select="$var157_cur/ns4:dbPtyAddressLine1">
																					<xsl:variable name="var160_cur" select="." />
																					<xsl:choose>
																						<xsl:when test="(. != &quot;''&quot;)">
																							<xsl:value-of select="'1'" />
																						</xsl:when>
																						<xsl:otherwise>
																							<xsl:for-each select="$var157_cur/ns4:dbPtyFreeLine2">
																								<xsl:variable name="var161_cur" select="." />
																								<xsl:for-each select="$var157_cur/ns4:dbPtyFreeLine3">
																									<xsl:variable name="var162_cur" select="." />
																									<xsl:value-of select="'1'" />
																								</xsl:for-each>
																							</xsl:for-each>
																						</xsl:otherwise>
																					</xsl:choose>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:variable>
														<xsl:if test="boolean(translate(normalize-space($var153_nested), ' 0', ''))">
															<AdrLine>
																<xsl:choose>
																	<xsl:when test="((($var128_cur = 'O') and (. != 'B')) and ('' != 'F'))">
																		<xsl:for-each select="$var126_cur/ns11:iaccountinfo">
																			<xsl:variable name="var163_cur" select="." />
																			<xsl:for-each select="(./ns7:mainOrChargeAccType)[(. = 'D')]">
																				<xsl:variable name="var164_filter" select="." />
																				<xsl:for-each select="$var163_cur/ns7:customerAddress">
																					<xsl:variable name="var165_cur" select="." />
																					<xsl:value-of select="." />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each select="$var126_cur/ns11:ipartydebit">
																			<xsl:variable name="var166_cur" select="." />
																			<xsl:for-each select="ns4:dbPtyRole">
																				<xsl:variable name="var167_cur" select="." />
																				<xsl:for-each select="($var166_cur/ns4:dbPtyRoleIndicator)[(($var167_cur = 'ORDPTY') and (. = 'R'))]">
																					<xsl:variable name="var168_filter" select="." />
																					<xsl:for-each select="$var166_cur/ns4:dbPtyAddressLine1">
																						<xsl:variable name="var169_cur" select="." />
																						<xsl:choose>
																							<xsl:when test="(. != &quot;''&quot;)">
																								<xsl:value-of select="." />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:variable name="var170_nested">
																									<xsl:for-each select="$var166_cur/ns4:dbPtyFreeLine2">
																										<xsl:variable name="var171_cur" select="." />
																										<xsl:for-each select="$var166_cur/ns4:dbPtyFreeLine3">
																											<xsl:variable name="var172_cur" select="." />
																											<xsl:value-of select="concat($var171_cur, .)" />
																										</xsl:for-each>
																									</xsl:for-each>
																								</xsl:variable>
																								<xsl:value-of select="$var170_nested" />
																							</xsl:otherwise>
																						</xsl:choose>
																					</xsl:for-each>
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:otherwise>
																</xsl:choose>
															</AdrLine>
														</xsl:if>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</PstlAdr>
							<xsl:for-each
								select="ns11:doOutwardMappingFlow/ns11:ipartydebit[ns4:dbPtyRoleIndicator = 'R' and ns4:dbPtyRole = 'ORDPTY']">

								<xsl:choose>
									<xsl:when test="(ns4:dbPtyIdentifierCode) or (ns4:dbPtyOrgIdOtherId)">
										<Id>
											<xsl:if test="(ns4:dbPtyIdentifierCode) or (ns4:dbPtyOrgIdOtherId)">
												<OrgId>
													<xsl:choose>
														<xsl:when test="ns4:dbPtyIdentifierCode">
															<AnyBIC>
																<xsl:value-of select="ns4:dbPtyIdentifierCode" />
															</AnyBIC>
														</xsl:when>
														<xsl:otherwise>
															<Othr>
																<xsl:if test="ns4:dbPtyOrgIdOtherId">
																	<Id>
																		<xsl:value-of select="ns4:dbPtyOrgIdOtherId" />
																	</Id>
																</xsl:if>
																<xsl:if test="ns4:dbPtyOrgIdOtherSchCode or ns4:dbPtyOrgIdOtherSchProp">
																	<SchmeNm>
																		<xsl:if test="ns4:dbPtyOrgIdOtherSchCode">
																			<Cd>
																				<xsl:choose>
																					<xsl:when test="starts-with(ns4:dbPtyOrgIdOtherSchCode, '/')">
																						<xsl:value-of select="substring-after(ns4:dbPtyOrgIdOtherSchCode, '/')" />
																					</xsl:when>
																					<xsl:otherwise>
																						<xsl:value-of select="ns4:dbPtyOrgIdOtherSchCode" />
																					</xsl:otherwise>
																				</xsl:choose>
																			</Cd>
																		</xsl:if>
																		<xsl:if test="ns4:dbPtyOrgIdOtherSchProp">
																			<Prtry>
																				<xsl:value-of select="ns4:dbPtyOrgIdOtherSchProp" />
																			</Prtry>
																		</xsl:if>
																	</SchmeNm>
																</xsl:if>
																<xsl:if test="ns4:dbPtyOrgIdOtherIssuer">
																	<Issr>
																		<xsl:value-of select="ns4:dbPtyOrgIdOtherIssuer" />
																	</Issr>
																</xsl:if>
															</Othr>
														</xsl:otherwise>
													</xsl:choose>
												</OrgId>
											</xsl:if>
										</Id>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if
											test="ns4:dbPtyBirthDate or ns4:dbPtyProvinceOfBirth or ns4:dbPtyCityOfBirth or ns4:dbPtyCountryOfBirth or ns4:dbPtyPrvIdOtherId or ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp or ns4:dbPtyPrvIdOtherIssuer">
											<Id>
												<PrvtId>
													<xsl:choose>
														<xsl:when
															test="ns4:dbPtyBirthDate or ns4:dbPtyProvinceOfBirth or ns4:dbPtyCityOfBirth or ns4:dbPtyCountryOfBirth">
															<DtAndPlcOfBirth>
																<xsl:if test="ns4:dbPtyBirthDate">
																	<BirthDt>
																		<xsl:value-of
																			select="concat(substring(string(ns4:dbPtyBirthDate), '1', '4'), '-', substring(string(ns4:dbPtyBirthDate), '5', '2'), '-', substring(string(ns4:dbPtyBirthDate), '7', '2'))" />
																	</BirthDt>
																</xsl:if>
																<xsl:if test="ns4:dbPtyProvinceOfBirth">
																	<PrvcOfBirth>
																		<xsl:value-of select="ns4:dbPtyProvinceOfBirth" />
																	</PrvcOfBirth>
																</xsl:if>
																<xsl:if test="ns4:dbPtyCityOfBirth">
																	<CityOfBirth>
																		<xsl:choose>
																			<xsl:when test="starts-with(ns4:dbPtyCityOfBirth, '/')">
																				<xsl:value-of select="substring-after(ns4:dbPtyCityOfBirth, '/')" />
																			</xsl:when>
																			<xsl:otherwise>
																				<xsl:value-of select="ns4:dbPtyCityOfBirth" />
																			</xsl:otherwise>
																		</xsl:choose>
																	</CityOfBirth>
																</xsl:if>
																<xsl:if test="ns4:dbPtyCountryOfBirth">
																	<CtryOfBirth>
																		<xsl:value-of select="ns4:dbPtyCountryOfBirth" />
																	</CtryOfBirth>
																</xsl:if>
															</DtAndPlcOfBirth>
														</xsl:when>
														<xsl:otherwise>
															<xsl:if
																test="ns4:dbPtyPrvIdOtherId or ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp or ns4:dbPtyPrvIdOtherIssuer">
																<Othr>
																	<xsl:if test="ns4:dbPtyPrvIdOtherId">
																		<Id>
																			<xsl:value-of select="ns4:dbPtyPrvIdOtherId" />
																		</Id>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyPrvIdOtherSchCode or ns4:dbPtyPrvIdOtherSchProp">
																		<SchmeNm>
																			<xsl:if test="ns4:dbPtyPrvIdOtherSchCode">
																				<Cd>
																					<xsl:value-of select="ns4:dbPtyPrvIdOtherSchCode" />
																				</Cd>
																			</xsl:if>
																			<xsl:if test="ns4:dbPtyPrvIdOtherSchProp">
																				<Prtry>
																					<xsl:value-of select="ns4:dbPtyPrvIdOtherSchProp" />
																				</Prtry>
																			</xsl:if>
																		</SchmeNm>
																	</xsl:if>
																	<xsl:if test="ns4:dbPtyPrvIdOtherIssuer">
																		<Issr>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns4:dbPtyPrvIdOtherIssuer, '/')">
																					<xsl:value-of select="substring-after(ns4:dbPtyPrvIdOtherIssuer, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns4:dbPtyPrvIdOtherIssuer" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</Issr>
																	</xsl:if>
																</Othr>
															</xsl:if>
														</xsl:otherwise>
													</xsl:choose>
												</PrvtId>
											</Id>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>

							</xsl:for-each>

						</Dbtr>
						<DbtrAcct>
							<Id>
								<xsl:choose>
									<xsl:when
										test="(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:paymentDirection) = 'O' and (ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:singleMultipleIndicator) != 'B' and (not(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions) or (ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions) != 'F'  and(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:orderingPartyTagOptions) != 'K')">
										<xsl:for-each
											select="ns11:doOutwardMappingFlow/ns11:iaccountinfo[ns7:mainOrChargeAccType = 'D' and ns7:relatedIBAN!= '']">
											<IBAN>
												<xsl:value-of select="ns7:relatedIBAN" />
											</IBAN>
										</xsl:for-each>
									</xsl:when>
									<xsl:otherwise>
										<xsl:for-each
											select="ns11:doOutwardMappingFlow/ns11:ipartydebit[ns4:dbPtyRole = 'ORDPTY' and ns4:dbPtyRoleIndicator ='R' and ns4:dbPtyAccountLine!= '']">
											<xsl:variable name="accountLine" select="ns4:dbPtyAccountLine" />
											<IBAN>
												<xsl:choose>
													<xsl:when test="starts-with($accountLine, '/')">
														<xsl:value-of select="substring-after($accountLine, '/')" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$accountLine" />
													</xsl:otherwise>
												</xsl:choose>
											</IBAN>
										</xsl:for-each>
									</xsl:otherwise>
								</xsl:choose>
							</Id>
						</DbtrAcct>
						<DbtrAgt>
							<FinInstnId>
								<BICFI>
									<xsl:choose>
										<xsl:when test="(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:paymentDirection) = 'O'">
											<xsl:if test="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:companyBIC">
												<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:companyBIC" />
											</xsl:if>
										</xsl:when>
										<xsl:when test="(ns11:doOutwardMappingFlow/ns11:iportransaction/ns2:paymentDirection) = 'R'">
											<xsl:if
												test="((ns11:doOutwardMappingFlow/ns11:ipartydebit/ns4:dbPtyRole) = 'ORDINS' or 
                                                         (ns11:doOutwardMappingFlow/ns11:ipartydebit/ns4:dbPtyRole) = 'SENDER') and
                                                         (ns11:doOutwardMappingFlow/ns11:ipartydebit/ns4:dbPtyRoleIndicator) = 'R' and
                                                         (ns11:doOutwardMappingFlow/ns11:ipartydebit/ns4:dbPtyIdentifierCode)">
												<xsl:value-of select="ns11:doOutwardMappingFlow/ns11:ipartydebit/ns4:dbPtyIdentifierCode" />
											</xsl:if>
										</xsl:when>
									</xsl:choose>
								</BICFI>
							</FinInstnId>
						</DbtrAgt>
						<xsl:for-each
							select="ns11:doOutwardMappingFlow/ns11:ipartycredit[ns3:crPtyRole = 'ACWINS' and ns3:crPtyRoleIndicator = 'G']">
							<xsl:if test="ns3:crPtyIdentifierCode">
								<CdtrAgt>
									<FinInstnId>
										<BICFI>
											<xsl:value-of select="ns3:crPtyIdentifierCode" />
										</BICFI>
									</FinInstnId>
								</CdtrAgt>
							</xsl:if>
						</xsl:for-each>
						<Cdtr>
							<xsl:variable name="var296_nested">
								<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit">
									<xsl:variable name="var297_cur" select="." />
									<xsl:for-each select="ns3:crPtyRole">
										<xsl:variable name="var298_cur" select="." />
										<xsl:for-each select="($var297_cur/ns3:crPtyRoleIndicator)[(($var298_cur = 'BENFCY') and (. = 'R'))]">
											<xsl:variable name="var299_filter" select="." />
											<xsl:value-of select="number(boolean($var297_cur/ns3:crPtyName))" />
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="boolean(translate(normalize-space($var296_nested), ' 0', ''))">
									<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit">
										<xsl:variable name="var300_cur" select="." />
										<xsl:for-each select="ns3:crPtyRole">
											<xsl:variable name="var301_cur" select="." />
											<xsl:for-each select="($var300_cur/ns3:crPtyRoleIndicator)[(($var301_cur = 'BENFCY') and (. = 'R'))]">
												<xsl:variable name="var302_filter" select="." />
												<xsl:for-each select="$var300_cur/ns3:crPtyName">
													<xsl:variable name="var303_cur" select="." />
													<xsl:variable name="var304_nested">
														<xsl:choose>
															<xsl:when test="(. != &quot;''&quot;)">
																<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:ipartycredit">
																	<xsl:variable name="var305_cur" select="." />
																	<xsl:for-each select="ns3:crPtyRole">
																		<xsl:variable name="var306_cur" select="." />
																		<xsl:for-each select="($var305_cur/ns3:crPtyRoleIndicator)[(($var306_cur = 'BENFCY') and (. = 'R'))]">
																			<xsl:variable name="var307_filter" select="." />
																			<xsl:for-each select="$var305_cur/ns3:crPtyName">
																				<xsl:variable name="var308_cur" select="." />
																				<xsl:value-of select="'1'" />
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:for-each>
															</xsl:when>
															<xsl:otherwise>
																<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:ipartycredit/ns3:crPtyFreeLine1">
																	<xsl:variable name="var309_cur" select="." />
																	<xsl:value-of select="'1'" />
																</xsl:for-each>
															</xsl:otherwise>
														</xsl:choose>
													</xsl:variable>
													<xsl:if test="boolean(translate(normalize-space($var304_nested), ' 0', ''))">
														<Nm>
															<xsl:choose>
																<xsl:when test="(. != &quot;''&quot;)">
																	<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:ipartycredit">
																		<xsl:variable name="var310_cur" select="." />
																		<xsl:for-each select="ns3:crPtyRole">
																			<xsl:variable name="var311_cur" select="." />
																			<xsl:for-each select="($var310_cur/ns3:crPtyRoleIndicator)[(($var311_cur = 'BENFCY') and (. = 'R'))]">
																				<xsl:variable name="var312_filter" select="." />
																				<xsl:for-each select="$var310_cur/ns3:crPtyName">
																					<xsl:variable name="var313_cur" select="." />
																					<xsl:value-of select="." />
																				</xsl:for-each>
																			</xsl:for-each>
																		</xsl:for-each>
																	</xsl:for-each>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:for-each select="$var1_initial/ns11:doOutwardMappingFlow/ns11:ipartycredit/ns3:crPtyFreeLine1">
																		<xsl:variable name="var314_cur" select="." />
																		<xsl:value-of select="." />
																	</xsl:for-each>
																</xsl:otherwise>
															</xsl:choose>
														</Nm>
													</xsl:if>
												</xsl:for-each>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="var315_nested">
										<xsl:choose>
											<xsl:when test="(&quot;''&quot; != &quot;''&quot;)">
												<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit">
													<xsl:variable name="var316_cur" select="." />
													<xsl:for-each select="ns3:crPtyRole">
														<xsl:variable name="var317_cur" select="." />
														<xsl:for-each select="($var316_cur/ns3:crPtyRoleIndicator)[(($var317_cur = 'BENFCY') and (. = 'R'))]">
															<xsl:variable name="var318_filter" select="." />
															<xsl:for-each select="$var316_cur/ns3:crPtyName">
																<xsl:variable name="var319_cur" select="." />
																<xsl:value-of select="'1'" />
															</xsl:for-each>
														</xsl:for-each>
													</xsl:for-each>
												</xsl:for-each>
											</xsl:when>
											<xsl:otherwise>
												<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit/ns3:crPtyFreeLine1">
													<xsl:variable name="var320_cur" select="." />
													<xsl:value-of select="'1'" />
												</xsl:for-each>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:if test="boolean(translate(normalize-space($var315_nested), ' 0', ''))">
										<Nm>
											<xsl:choose>
												<xsl:when test="(&quot;''&quot; != &quot;''&quot;)">
													<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit">
														<xsl:variable name="var321_cur" select="." />
														<xsl:for-each select="ns3:crPtyRole">
															<xsl:variable name="var322_cur" select="." />
															<xsl:for-each select="($var321_cur/ns3:crPtyRoleIndicator)[(($var322_cur = 'BENFCY') and (. = 'R'))]">
																<xsl:variable name="var323_filter" select="." />
																<xsl:for-each select="$var321_cur/ns3:crPtyName">
																	<xsl:variable name="var324_cur" select="." />
																	<xsl:value-of select="." />
																</xsl:for-each>
															</xsl:for-each>
														</xsl:for-each>
													</xsl:for-each>
												</xsl:when>
												<xsl:otherwise>
													<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit/ns3:crPtyFreeLine1">
														<xsl:variable name="var325_cur" select="." />
														<xsl:value-of select="." />
													</xsl:for-each>
												</xsl:otherwise>
											</xsl:choose>
										</Nm>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:for-each
								select="ns11:doOutwardMappingFlow/ns11:ipartycredit[ns3:crPtyRole = 'BENFCY' and ns3:crPtyRoleIndicator = 'R']">
								<xsl:if test="ns3:crPtyCountry or ns3:crPtyAddressLine1 or ns3:crPtyAddressLine2">
									<PstlAdr>
										<xsl:if test="ns3:crPtyCountry">
											<Ctry>
												<xsl:value-of select="ns3:crPtyCountry" />
											</Ctry>
										</xsl:if>
										<xsl:if test="ns3:crPtyAddressLine1">
											<AdrLine>
												<xsl:choose>
													<xsl:when test="starts-with(ns3:crPtyAddressLine1, '/')">
														<xsl:value-of select="substring-after(ns3:crPtyAddressLine1, '/')" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="ns3:crPtyAddressLine1" />
													</xsl:otherwise>
												</xsl:choose>
											</AdrLine>
										</xsl:if>
										<xsl:if test="ns3:crPtyAddressLine2">
											<AdrLine>
												<xsl:choose>
													<xsl:when test="starts-with(ns3:crPtyAddressLine2, '/')">
														<xsl:value-of select="substring-after(ns3:crPtyAddressLine2, '/')" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="ns3:crPtyAddressLine2" />
													</xsl:otherwise>
												</xsl:choose>
											</AdrLine>
										</xsl:if>
									</PstlAdr>
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each
								select="ns11:doOutwardMappingFlow/ns11:ipartycredit[ns3:crPtyRoleIndicator = 'R' and ns3:crPtyRole = 'BENFCY']">
								<xsl:if
									test="ns3:crPtyIdentifierCode or ns3:crPtyOrgIdOtherId or ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp or ns3:crPtyOrgIdOtherIssuer or ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth or ns3:crPtyPrvIdOtherId or ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp or ns3:crPtyPrvIdOtherIssuer">
									<Id>
										<xsl:choose>
											<xsl:when
												test="ns3:crPtyIdentifierCode or ns3:crPtyOrgIdOtherId or ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp or ns3:crPtyOrgIdOtherIssuer">
												<xsl:if
													test="ns3:crPtyIdentifierCode or ns3:crPtyOrgIdOtherId or ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp or ns3:crPtyOrgIdOtherIssuer">
													<OrgId>
														<xsl:choose>
															<xsl:when test="ns3:crPtyIdentifierCode">
																<AnyBIC>
																	<xsl:value-of select="ns3:crPtyIdentifierCode" />
																</AnyBIC>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if
																	test="ns3:crPtyOrgIdOtherId or ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp or ns3:crPtyOrgIdOtherIssuer">
																	<Othr>
																		<xsl:if test="ns3:crPtyOrgIdOtherId">
																			<Id>
																				<xsl:value-of select="ns3:crPtyOrgIdOtherId" />
																			</Id>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp">
																			<SchmeNm>
																				<xsl:if test="ns3:crPtyOrgIdOtherSchCode">
																					<Cd>
																						<xsl:value-of select="ns3:crPtyOrgIdOtherSchCode" />
																					</Cd>
																				</xsl:if>
																				<xsl:if test="ns3:crPtyOrgIdOtherSchProp">
																					<Prtry>
																						<xsl:choose>
																							<xsl:when test="starts-with(ns3:crPtyOrgIdOtherSchProp, '/')">
																								<xsl:value-of select="substring-after(ns3:crPtyOrgIdOtherSchProp, '/')" />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="ns3:crPtyOrgIdOtherSchProp" />
																							</xsl:otherwise>
																						</xsl:choose>
																					</Prtry>
																				</xsl:if>
																			</SchmeNm>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyOrgIdOtherIssuer">
																			<Issr>
																				<xsl:value-of select="ns3:crPtyOrgIdOtherIssuer" />
																			</Issr>
																		</xsl:if>
																	</Othr>
																</xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</OrgId>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if
													test="ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth or ns3:crPtyPrvIdOtherId or ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp or ns3:crPtyPrvIdOtherIssuer">
													<PrvtId>
														<xsl:choose>
															<xsl:when
																test="ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth">
																<DtAndPlcOfBirth>
																	<xsl:if test="ns3:crPtyBirthDate">
																		<BirthDt>
																			<xsl:value-of
																				select="concat(substring(string(ns3:crPtyBirthDate), '1', '4'), '-', substring(string(ns3:crPtyBirthDate), '5', '2'), '-', substring(string(ns3:crPtyBirthDate), '7', '2'))" />
																		</BirthDt>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyProvinceOfBirth">
																		<PrvcOfBirth>
																			<xsl:value-of select="ns3:crPtyProvinceOfBirth" />
																		</PrvcOfBirth>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyCityOfBirth">
																		<CityOfBirth>
																			<xsl:value-of select="ns3:crPtyCityOfBirth" />
																		</CityOfBirth>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyCountryOfBirth">
																		<CtryOfBirth>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns3:crPtyCountryOfBirth, '/')">
																					<xsl:value-of select="substring-after(ns3:crPtyCountryOfBirth, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns3:crPtyCountryOfBirth" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</CtryOfBirth>
																	</xsl:if>
																</DtAndPlcOfBirth>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if
																	test="(ns3:crPtyOrgIdOtherId) != 'true' or ns3:crPtyPrvIdOtherId or ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp or ns3:crPtyPrvIdOtherIssuer">
																	<Othr>
																		<xsl:if test="ns3:crPtyPrvIdOtherId">
																			<Id>
																				<xsl:value-of select="ns3:crPtyPrvIdOtherId" />
																			</Id>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp">
																			<SchmeNm>
																				<xsl:if test="ns3:crPtyPrvIdOtherSchCode">
																					<Cd>
																						<xsl:value-of select="ns3:crPtyPrvIdOtherSchCode" />
																					</Cd>
																				</xsl:if>
																				<xsl:if test="ns3:crPtyPrvIdOtherSchProp">
																					<Prtry>
																						<xsl:value-of select="ns3:crPtyPrvIdOtherSchProp" />
																					</Prtry>
																				</xsl:if>
																			</SchmeNm>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyPrvIdOtherIssuer">
																			<Issr>
																				<xsl:value-of select="ns3:crPtyPrvIdOtherIssuer" />
																			</Issr>
																		</xsl:if>
																	</Othr>
																</xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</PrvtId>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</Id>
								</xsl:if>
							</xsl:for-each>
						</Cdtr>
						<CdtrAcct>
							<Id>
								<xsl:for-each select="ns11:doOutwardMappingFlow/ns11:ipartycredit">
									<xsl:variable name="var354_cur" select="." />
									<xsl:for-each select="ns3:crPtyRole">
										<xsl:variable name="var355_cur" select="." />
										<xsl:for-each select="($var354_cur/ns3:crPtyRoleIndicator)[(($var355_cur = 'BENFCY') and (. = 'R'))]">
											<xsl:variable name="var356_filter" select="." />
											<xsl:for-each select="$var354_cur/ns3:crPtyAccountLine">
												<xsl:variable name="var357_cur" select="." />
												<IBAN>
													<xsl:choose>
														<xsl:when test="starts-with(., '/')">
															<xsl:value-of select="substring-after(., '/')" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="." />
														</xsl:otherwise>
													</xsl:choose>
												</IBAN>
											</xsl:for-each>
										</xsl:for-each>
									</xsl:for-each>
								</xsl:for-each>
							</Id>
						</CdtrAcct>
						<xsl:for-each
							select="ns11:doOutwardMappingFlow/ns11:ipartycredit[ns3:crPtyRole = 'ULTCDT' and ns3:crPtyRoleIndicator = 'R']">
							<UltmtCdtr>
								<xsl:if test="ns3:crPtyName">
									<Nm>
										<xsl:value-of select="ns3:crPtyName" />
									</Nm>
								</xsl:if>
								<xsl:if
									test="(ns3:crPtyIdentifierCode or ns3:crPtyOrgIdOtherId) or ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth or ns3:crPtyOrgIdOtherId or ns3:crPtyPrvIdOtherId or ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp or ns3:crPtyPrvIdOtherIssuer ">
									<Id>
										<xsl:choose>
											<xsl:when test="(ns3:crPtyIdentifierCode) or (ns3:crPtyOrgIdOtherId)">
												<xsl:if test="ns3:crPtyIdentifierCode or ns3:crPtyOrgIdOtherId">
													<OrgId>
														<xsl:choose>
															<xsl:when test="ns3:crPtyIdentifierCode">

																<AnyBIC>
																	<xsl:value-of select="ns3:crPtyIdentifierCode" />
																</AnyBIC>
															</xsl:when>
															<xsl:otherwise>


																<Othr>
																	<xsl:if test="ns3:crPtyOrgIdOtherId">
																		<Id>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns3:crPtyOrgIdOtherId, '/')">
																					<xsl:value-of select="substring-after(ns3:crPtyOrgIdOtherId, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns3:crPtyOrgIdOtherId" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</Id>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyOrgIdOtherSchCode or ns3:crPtyOrgIdOtherSchProp">
																		<SchmeNm>
																			<xsl:if test="ns3:crPtyOrgIdOtherSchCode">
																				<Cd>
																					<xsl:value-of select="ns3:crPtyOrgIdOtherSchCode" />
																				</Cd>
																			</xsl:if>
																			<xsl:if test="ns3:crPtyOrgIdOtherSchProp">
																				<Prtry>
																					<xsl:value-of select="ns3:crPtyOrgIdOtherSchProp" />
																				</Prtry>
																			</xsl:if>
																		</SchmeNm>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyOrgIdOtherIssuer">
																		<Issr>
																			<xsl:value-of select="ns3:crPtyOrgIdOtherIssuer" />
																		</Issr>
																	</xsl:if>
																</Othr>
															</xsl:otherwise>
														</xsl:choose>
													</OrgId>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if
													test="ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth or ns3:crPtyPrvIdOtherId">
													<PrvtId>
														<xsl:choose>
															<xsl:when
																test="ns3:crPtyBirthDate or ns3:crPtyProvinceOfBirth or ns3:crPtyCityOfBirth or ns3:crPtyCountryOfBirth">
																<DtAndPlcOfBirth>
																	<xsl:if test="ns3:crPtyBirthDate">
																		<BirthDt>
																			<xsl:value-of
																				select="concat(substring(string(ns3:crPtyBirthDate), '1', '4'), '-', substring(string(ns3:crPtyBirthDate), '5', '2'), '-', substring(string(ns3:crPtyBirthDate), '7', '2'))" />
																		</BirthDt>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyProvinceOfBirth">
																		<PrvcOfBirth>
																			<xsl:choose>
																				<xsl:when test="starts-with(ns4:dbPtyProvinceOfBirth, '/')">
																					<xsl:value-of select="substring-after(ns4:dbPtyProvinceOfBirth, '/')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="ns3:crPtyProvinceOfBirth" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</PrvcOfBirth>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyCityOfBirth">
																		<CityOfBirth>
																			<xsl:value-of select="ns3:crPtyCityOfBirth" />
																		</CityOfBirth>
																	</xsl:if>
																	<xsl:if test="ns3:crPtyCountryOfBirth">
																		<CtryOfBirth>
																			<xsl:value-of select="ns3:crPtyCountryOfBirth" />
																		</CtryOfBirth>
																	</xsl:if>
																</DtAndPlcOfBirth>
															</xsl:when>
															<xsl:otherwise>
																<xsl:if
																	test="(ns3:crPtyOrgIdOtherId) != 'true' or (ns3:crPtyPrvIdOtherId or ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp or ns3:crPtyPrvIdOtherIssuer)">
																	<Othr>
																		<xsl:if test="ns3:crPtyPrvIdOtherId">
																			<Id>
																				<xsl:value-of select="ns3:crPtyPrvIdOtherId" />
																			</Id>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyPrvIdOtherSchCode or ns3:crPtyPrvIdOtherSchProp">
																			<SchmeNm>
																				<xsl:if test="ns3:crPtyPrvIdOtherSchCode">
																					<Cd>
																						<xsl:value-of select="ns3:crPtyPrvIdOtherSchCode" />
																					</Cd>
																				</xsl:if>
																				<xsl:if test="ns3:crPtyPrvIdOtherSchProp">
																					<Prtry>
																						<xsl:choose>
																							<xsl:when test="starts-with(ns4:dbPtyPrvIdOtherSchProp, '/')">
																								<xsl:value-of select="substring-after(ns4:dbPtyPrvIdOtherSchProp, '/')" />
																							</xsl:when>
																							<xsl:otherwise>
																								<xsl:value-of select="ns3:crPtyPrvIdOtherSchProp" />
																							</xsl:otherwise>
																						</xsl:choose>
																					</Prtry>
																				</xsl:if>
																			</SchmeNm>
																		</xsl:if>
																		<xsl:if test="ns3:crPtyPrvIdOtherIssuer">
																			<Issr>
																				<xsl:value-of select="ns3:crPtyPrvIdOtherIssuer" />
																			</Issr>
																		</xsl:if>
																	</Othr>
																</xsl:if>
															</xsl:otherwise>
														</xsl:choose>
													</PrvtId>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</Id>
								</xsl:if>
							</UltmtCdtr>
						</xsl:for-each>
					</OrgnlTxRef>
				</TxInf>
			</FIToFIPmtStsReq>
		</Document>
	</xsl:template>
</xsl:stylesheet>