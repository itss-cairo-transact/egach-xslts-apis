* @ValidationCode : MjotNDU5MzQ2NDAxOkNwMTI1MjoxNjM4ODI0NzIyMjQxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 06 Dec 2021 23:05:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*------------------------------------------------------------------------------
$PACKAGE PP.EGACHService
SUBROUTINE InwardMappingFramework.pCSR(iIncomingMessage, oMessageData, oRTResponse)
*-----------------------------------------------------------------------------
    $USING PP.InwardMappingFramework
    GOSUB extractRJ
RETURN
*-----------------------------------------------------------------------------
extractRJ:
*Extract the File header details from the XML message to determine the type of processing
    tagTxn = '<Transaction>'
    pos = 2
    Content = ''
    
    mainContent = FIELD(iIncomingMessage,tagTxn,pos)
    Content = mainContent
    tag = '<StatusIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsId> = tagValue
        
    Content = mainContent
    tag = '<OriginalInstructionIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglInstrId> = tagValue
    
    Content = mainContent
    tag = '<OriginalEndToEndIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglEndToEndId> = tagValue
        
    Content = mainContent
    tag = '<OriginalTransactionIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglTrxId> = tagValue
        
    Content = mainContent
    tag = '<TransactionStatus>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxSts> = tagValue
        
    Content = mainContent
    tag = '<StatusReasonInformationOriginator>'
    GOSUB extract
    Content = tagContent
    GOSUB extract
    tag = '<Name>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfOrgnName> = tagValue
    tag = '<BICOrBEI>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfOrgnIdOrgIdBIC> = tagValue
        
    Content = mainContent
    tag = '<StatusReasonInformationReason>'
    GOSUB extract
    Content = tagContent
    GOSUB extract
    tag = '<Code>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnCd> = tagValue
    tag = '<Proprietary>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfRsnProp> = tagValue
        
* This indicates transaction level additional info
    Content = mainContent
    tag = '<StatusReasonInformationAdditionalInformation>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfAdInf> = tagValue

* If tagvalue not there, have to check bulk level additional info is there or not
    IF tagValue EQ '' THEN
        tagTxn = '<BulkHeader>'
        pos = 2
        Content = ''
    
        mainContentBulk = FIELD(iIncomingMessage,tagTxn,pos)
        Content = mainContentBulk
        tag = '<BulkOriginalClearingStatusReportReasonCodeAddInfo>'
        GOSUB extract
* Presence of tagvalue indicates at bulk level, additonal info is present. So map bulk level additional info to txn level additional info
        IF tagValue THEN
            oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrStsRsnInfAdInf> = tagValue
        END
    END
        
    Content = mainContent
    tag = '<ChargesInformationAmount>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfAm> = tagValue
    
    Content = mainContent
    tag = '<ChargesInformationAmountCurrency>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfCcy> = tagValue
       
    Content = mainContent
    tag = '<PaymentTypeInformationLocalInstrument>'
    GOSUB extract
    Content = tagContent
    GOSUB extract
    tag = '<Code>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrCd> = tagValue
    
    Content = mainContent
    tag = '<PaymentTypeInformationLocalInstrument>'
    GOSUB extract
    Content = tagContent
    GOSUB extract
    tag = '<Proprietary>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrPmtTpInfLclInstrPrtry> = tagValue
    
    Content = mainContent
    tag = '<ChargesInformationPartyFinancialInstitutionIdentification>'
    GOSUB extract
    Content = tagContent
    GOSUB extract
    tag = '<BICFI>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdBIC> = tagValue
    tag = '<ClearingSystemMemberIdentification>'
    GOSUB extract
    tag = '<ClearingSystemIdentification>'
    GOSUB extract
    tag = '<Code>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdCgSysMemIdCd> = tagValue
    tag = '<Proprietary>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdCgSysMemIdProp> = tagValue
    tag = '<MemberIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdMemId> = tagValue
    tag = '<OtherIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrTrxStsChgsInfPartyFinInstIdOthrId> = tagValue
    
        
    Content = mainContent
    tag = '<OriginalInterbankSettlementAmount>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlAm> = tagValue
    
    Content = mainContent
    tag = '<OriginalInterbankSettlementAmountCurrency>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlAmCcy> = tagValue

    Content = mainContent
    tag = '<OriginalInterbankSettlementDate>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglItbkSttlDt> = tagValue
    GOSUB extract
    
       
    Content = mainContent
    tag = '<OriginalDebtorAgent>'
    GOSUB extract
    Content = tagContent
    tag = '<BICFI>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgBICFI> = tagValue
    
    tag = '<ClearingSystemMemberIdentification>'
    GOSUB extract
    tag = '<ClearingSystemIdentification>'
    GOSUB extract
    tag = '<Code>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemIdCgSysIdCd> = tagValue
    tag = '<Proprietary>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemIdCgSysIdProp> = tagValue
    tag = '<MemberIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgCgSysMemId> = tagValue
    tag = '<OtherIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglDbtAgOthId> = tagValue
    
    Content = mainContent
    tag = '<OriginalCreditorAgent>'
    GOSUB extract
    Content = tagContent
    tag = '<BICFI>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgBICFI> = tagValue
    
    tag = '<ClearingSystemMemberIdentification>'
    GOSUB extract
    tag = '<ClearingSystemIdentification>'
    GOSUB extract
    tag = '<Code>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemIdCgSysIdCd> = tagValue
    tag = '<Proprietary>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemIdCgSysIdProp> = tagValue
    tag = '<MemberIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgCgSysMemId> = tagValue
    tag = '<OtherIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrglCrdAgOthId> = tagValue
    
    Content = mainContent
    tag = '<OriginalMessageIdentification>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrOrgMsgId> = tagValue
    
    Content = mainContent
    tag = '<FinalAcceptanceDate>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpFinalAcceptanceDate> = tagValue
    
    Content = mainContent
    tag = '<CSMElapsedTime>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCSMElapsedTime> = tagValue
*
    tag = '<AcceptanceDateTime>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrAcceptanceDate> = tagValue

* Added the new tags as part of admi.007 AckNack Message Type
    Content = mainContent
    tag = '<OriginalBahMessageID>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpOriginalBahMessageID> = tagValue
    
    Content = mainContent
    tag = '<AckNackReason>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackReason> = tagValue
    
    Content = mainContent
    tag = '<AckNackDescription>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackDescription> = tagValue
    
    Content = mainContent
    tag = '<AckNackMsgType>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNackMsgType> = tagValue
    
    Content = mainContent
    tag = '<AckStatus>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckStatus> = tagValue
    
    Content = mainContent
    tag = '<AckNacTimestamp>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpAckNacTimestamp> = tagValue
    
    Content = mainContent
    tag = '<LocalReferences>'
    GOSUB extract
    Content = tagContent
    totalLocRefContent = tagContent
    numberOfLocref= 1       ;   tagValue = 1
    LOOP
    WHILE tagValue NE '' DO
        GOSUB getLocalReferences
        numberOfLocref= numberOfLocref+1
* To extract and check if any more tags are present.
        GOSUB extract
    REPEAT


RETURN
*-----------------------------------------------------------------------------
extract:
    tagContent = ""
    tagValue = ""
    tagContent = FIELD(Content,tag,pos)
    tagValue = FIELD(tagContent,"<",1,1)
RETURN
*-----------------------------------------------------------------------------

getLocalReferences:
    
    tag = '<LocalRefName>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefName,-1> = tagValue
    
    tag = '<LocalRefValue>'
    GOSUB extract
    oMessageData<PP.InwardMappingFramework.PpClearingStatusReport.PpCsrLocalRefValue,-1> = tagValue
    Content = FIELD(totalLocRefContent,'</LocalRefValue>',numberOfLocref+1)

RETURN
*-----------------------------------------------------------------------------

END
